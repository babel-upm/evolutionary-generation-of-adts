module G3
  (
    g3,
    g3InitialStep,
    g3Step,
    g3From,
    g3Stream,

    GSearch (..),
    GStep (..),

    Individual,
    Operation (..),
    Optimization(..),
    Fitness (..),
    Selection,
    maximize,
    minimize,
    fitnessNorm,
    fitnessMean,
    fitnessObjectives,
    fitnessValues,
    norm,

    selectionByElite,
    selectionByEliteOn,
    selectionByEliteSum,
    selectionByEliteFitnessNorm,
    selectionByEliteFitnessMean,
    selectionByParetoElite,

    crossover,
    mutate,
    -- mutateSafe,
    -- paretoFront
  )
where

import G3.EA
