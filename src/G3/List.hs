{-# OPTIONS_GHC -Wno-missing-export-lists #-}
module G3.List
  -- (
  --   list,

  --   prune,
  --   add,
  --   modify,
  --   merge,

  --   sizeAndPrune
  -- )
where

import Test.QuickCheck

data List a = Cons a (List a) | Nil deriving (Show)

list :: Gen (List Int)
list = sized list'
  where
    list' size
      | size <= 0 = return Nil
      | otherwise = do
          a <- arbitrary
          tail' <- list' (pred size)
          return (Cons a tail')

prune :: List a -> Gen (List a)
prune l = sized $ \size -> do
    n <- chooseInt (0, size)
    prune' l n
  where
    prune' :: List a -> Int -> Gen (List a)
    prune' Nil _ = return Nil
    prune' (Cons x xs) size
      | size <= 0 = return (Cons x Nil)
      | otherwise = do
          xs' <- prune' xs (pred size)
          return (Cons x xs')

-- sublist :: List a -> Gen (List a)
-- subList l = do
--   let size = listSize l
--   begin <- chooseInt (0, size)
--   end <- chooseInt (size, end)

--   subList' :: Int -> (Int, Int) -> Gen (List a)
--   subList' _ _ Nil = Nil
--   subList' i (begin, end) (Cons x xs)
--   | i >= end = Nil
--   | begin <= i && i <= end = do
--       xs' <- subList' (i + 1) (begin, end) xs
--       return (Cons x xs')
--   | otherwise = subList' (i + 1) (begin, end) xs

listSize :: List a -> Int
listSize Nil = 0
listSize (Cons _ xs) = 1 + listSize xs

sizeAndPrune :: Gen (Int, Int)
sizeAndPrune = do
  l <- list
  l' <- prune l
  return (listSize l, listSize l')

add :: Arbitrary a => List a -> Gen (List a)
add Nil = do
  a <- arbitrary
  return $ Cons a Nil
add (Cons x xs) = do
  xs' <- add xs
  return (Cons x xs')

modify :: Arbitrary a => List a -> Gen (List a)
modify Nil = return Nil
modify (Cons x xs) = do
  oneof [ do
            xs' <- modify xs
            return (Cons x xs')
        , do
            x' <- arbitrary
            return (Cons x' xs)
        ]


merge :: List a -> List a -> Gen (List a)
merge Nil ys = return ys
merge xs Nil = return xs
merge (Cons x xs) (Cons y ys) =
  oneof
  [
    do
      tail' <- merge xs (Cons y ys)
      return (Cons x tail')
  , do
      tail' <- merge (Cons x xs) ys
      return (Cons y tail')
  ]
