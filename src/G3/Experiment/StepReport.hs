{-# LANGUAGE DeriveGeneric #-}
-- {-# LANGUAGE OverloadedStrings #-}
module G3.Experiment.StepReport
  ( StepReport
  , StepReportAux(..)
  , mkStepReport
  ) where

import G3 hiding (extra)

import GHC.Generics
import Data.Aeson
import qualified Utils

data StepReport i a = StepReport
  { runTitle :: String
  , individualsCount :: Int
  , runTime :: Double
  , totalFitness :: Double
  , generation :: Int
  -- , best :: [Individual i]
  , extra :: a
  } deriving (Show, Generic)

instance (ToJSON a, ToJSON i) => ToJSON (StepReport i a)
instance (FromJSON a, FromJSON i) => FromJSON (StepReport i a)

data StepReportAux i a = StepReportAux
 { auxStepTitle :: String
 , auxStepTime :: Double
 , auxStepMkExtra :: GStep i -> a
 }

mkStepReport :: StepReportAux i a -> GStep i -> StepReport i a
mkStepReport aux step = StepReport
  { individualsCount = 0
  , runTitle = auxStepTitle aux
  , runTime = auxStepTime aux
  , totalFitness = sum (sum . fitnessValues . fst <$> pop step)
  -- , best = [] -- [Utils.maximumOn (sum . fitnessValues . fst) (pop step)]
  , generation = stepCount step
  , extra = auxStepMkExtra aux step
  }
