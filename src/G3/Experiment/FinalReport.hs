{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE NamedFieldPuns #-}
module G3.Experiment.FinalReport
  ( FinalReport
  , FinalReportAux(..)
  , BaseReport(..)
  , mkFinalReport
  ) where

import G3 hiding (extra)

import GHC.Generics
import Data.Aeson

import Data.Set (Set)
import qualified Data.Set as S

data FinalReport final = FinalReport
  { runTitle :: String
  , runTime :: Double
  , generations :: Int
  , totalGeneratedIndividuals :: Int
  , uniqueIndividualsCount :: Int
  , validIndividualsCount :: Int
  , selectedIndividualsCount :: Int
  , extra :: final
  } deriving (Show, Generic)

instance ToJSON a => ToJSON (FinalReport a)
instance FromJSON a => FromJSON (FinalReport a)

data FinalReportAux a b = FinalReportAux
  {
    auxTitle :: String,
    auxTime :: Double,
    auxSelection :: [Individual a] -> [Individual a],
    auxValid :: Individual a -> Bool,
    auxMkExtra :: [GStep a] -> BaseReport a -> b
  }

data BaseReport a = BaseReport
  {
    _generations :: Int,
    _totalGeneratedIndividuals :: Int,
    _uniqueIndividuals :: Set (Individual a),
    _validIndividuals :: Set (Individual a),
    _selectedIndividuals :: Set (Individual a)
  } deriving Show

mkFinalReport :: Ord i => FinalReportAux i final -> [GStep i] -> FinalReport final
mkFinalReport aux steps =
  let base = mkBaseReport aux steps
  in FinalReport {
    runTitle = auxTitle aux,
    runTime = auxTime aux,
    generations = _generations base,
    totalGeneratedIndividuals = _totalGeneratedIndividuals base,
    uniqueIndividualsCount = S.size (_uniqueIndividuals base),
    validIndividualsCount = S.size (_validIndividuals base),
    selectedIndividualsCount = S.size (_selectedIndividuals base),
    extra = auxMkExtra aux steps base
    }

mkBaseReport :: Ord a => FinalReportAux a b -> [GStep a] -> BaseReport a
mkBaseReport = mkBaseReport' emptyReport
  where
    emptyReport = BaseReport {
      _generations = 0,
      _totalGeneratedIndividuals = 0,
      _uniqueIndividuals = S.empty,
      _validIndividuals = S.empty,
      _selectedIndividuals = S.empty
    }

    mkBaseReport' :: Ord a => BaseReport a  -> FinalReportAux a b -> [GStep a] -> BaseReport a
    mkBaseReport' r _ [] = r
    mkBaseReport' r@(BaseReport{
                    _generations,
                    _totalGeneratedIndividuals,
                    _uniqueIndividuals,
                    _validIndividuals,
                    _selectedIndividuals
                    }) aux (step:steps) =
      let pop' = pop step
          s' = _selectedIndividuals `S.union` S.fromList (auxSelection aux pop')
          v' = _validIndividuals `S.union` S.fromList (filter (auxValid aux) pop')
          u' = _uniqueIndividuals `S.union` S.fromList pop'
          r' = r
            {
              _generations = _generations + 1,
              _totalGeneratedIndividuals = _totalGeneratedIndividuals + length pop',
              _uniqueIndividuals = u',
              _validIndividuals = v',
              _selectedIndividuals = s'
            }
       in mkBaseReport' r' aux steps
