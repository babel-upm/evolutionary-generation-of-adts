{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE DeriveGeneric #-}
module G3.Experiment.Options
  (
    MultiRunOptions,
    RunOptions(..),
    RuntimeOpt(..),
    MultipleOptions,
    runOptionsList,
    parseMultipleOptions
  ) where

import Options.Applicative

import GHC.Generics
import Data.Aeson

data RuntimeOpt = GALoop | GPLoop | NSGA2 | CustomLoop deriving (Eq, Generic)

instance Show RuntimeOpt where
  show opt = case opt of
      GALoop -> "ga"
      GPLoop -> "gp"
      NSGA2 -> "nsga2"
      CustomLoop -> "custom"

instance ToJSON RuntimeOpt
instance FromJSON RuntimeOpt

data RunOptions = RunOptions
  { optMutation :: Int
  , optCrossoverP :: Int
  , optPopulation :: Int
  , optGenerations :: Int
  , genEmptyTrees :: Bool
  , optIndividualSize :: Int
  , cases :: [String]
  , exclude :: [String]
  , repeatN :: Int
  , runtimeStep :: RuntimeOpt
  -- Output Configuration
  , optCheckDiversity :: Bool
  , optShowStepReport :: Bool
  , optShowFinalReport :: Bool
  , skipReport :: Bool
  , optPretty :: Bool
  , noExport :: Bool
  , filePrefix :: String
  , outDir :: String
  } deriving Show

data MultiRunOptions = MultiRunOptions
  { _optMutation :: [Int]
  , _optCrossoverP :: [Int]
  , _optPopulation :: [Int]
  , _optGenerations :: [Int]
  , _genEmptyTrees :: Bool
  , _optIndividualSize :: [Int]
  , _cases :: [String]
  , _exclude :: [String]
  , _repeatN :: [Int]
  , _runtimeStep :: [RuntimeOpt]
  -- Output Configuration
  , _optCheckDiversity :: Bool
  , _showStepReport :: Bool
  , _showFinalReport :: Bool
  , _skipReport :: Bool
  , _optPretty :: Bool
  , _noExport :: Bool
  , _filePrefix :: String
  , _outDir :: String
  } deriving Show

data MultipleOptions = MultipleOptions
  { _mut :: Int
  , _cro :: Int
  , _pop :: Int
  , _gen :: Int
  , _siz :: Int
  , _rep :: Int
  , _run :: RuntimeOpt
  } deriving (Show, Generic)

instance ToJSON MultipleOptions
instance FromJSON MultipleOptions

runOptionsList :: MultiRunOptions -> [RunOptions]
runOptionsList opts = mkRunOptions <$> (
  MultipleOptions <$>
    _optMutation opts <*>
    _optCrossoverP opts <*>
    _optPopulation opts <*>
    _optGenerations opts <*>
    _optIndividualSize opts <*>
    _repeatN opts <*>
    _runtimeStep opts
  )
  where mkRunOptions :: MultipleOptions -> RunOptions
        mkRunOptions mOpts = RunOptions
          { optMutation = _mut mOpts
          , optCrossoverP = _cro mOpts
          , optPopulation = _pop mOpts
          , optGenerations = _gen mOpts
          , genEmptyTrees = _genEmptyTrees opts
          , optIndividualSize = _siz mOpts
          , cases = _cases opts
          , exclude = _exclude opts
          , repeatN = _rep mOpts
          , runtimeStep = _run mOpts
          , optCheckDiversity = _optCheckDiversity opts
          , optShowStepReport = _showStepReport opts
          , optShowFinalReport = _showFinalReport opts
          , skipReport = _skipReport opts
          , optPretty = _optPretty opts
          , noExport = _noExport opts
          , filePrefix = _filePrefix opts
          , outDir = _outDir opts
          }

manyDefault :: a -> Parser [a] -> Parser [a]
manyDefault d = fmap (\xs -> if null xs then [d] else xs)

runOptions :: Parser MultiRunOptions
runOptions = MultiRunOptions
  <$> manyDefault 30 (many (option (auto @Int)
      (  long "mutation"
      <> short 'm'
      <> metavar "M"
      <> help "Probability (between 0 and 100) to mutate an individual. (Default: 30)"
      )))
  <*> manyDefault 80 (many (option (auto @Int)
      (  long "mutation"
      <> short 'x'
      <> metavar "X"
      <> help "Probability (between 0 and 100) to crossover two individuals. (Default: 80)"
      )))
  <*> manyDefault 100 (many (option (auto @Int)
      (  long "population"
      <> short 'p'
      <> metavar "P"
      <> help "Population size. (Default 100)"
      )))
  <*> manyDefault 1000 (many (option (auto @Int)
      (  long "max-generations"
      <> short 'g'
      <> metavar "G"
      <> help "Max generations. Stop evolution when generation STEPS is reached. (Default: 1000)"
      )))
  <*> switch
      (  long "gen-empty-trees"
      <> help "Activate random generation of empty trees."
      )
  <*> manyDefault 30 (many (option (auto @Int)
      (  long "target-size"
      <> short 's'
      <> metavar "S"
      <> help "Target size of individuals. Optimize size fitness towards this value. (Default: 30)"
      )))
  <*> many
      ( strOption
        (  long "case"
        <> metavar "SUBSTRING"
        <> help "Execute only evaluation cases with the provided string in their title. Can be repeated. By default, executes all cases."
        )
      )
  <*> many
      ( strOption
        (  long "exclude"
        <> metavar "SUBSTRING"
        <> help "Execute only evaluation cases WITHOUT the provided string in their title. Can be repeated. By default, executes all cases."
        )
      )
  <*> manyDefault 1 (many (option (auto @Int)
      (  long "repeat"
      <> short 'r'
      <> metavar "N"
      <> help "Run multiple times the experiments. Prefix in file will include the number repetition. (Default: 1)"
      )))
  <*> manyDefault GALoop (many
      (
        flag' GALoop (long "ga" <> help "Use the GA loop")
        <|> flag' GPLoop (long "gp" <> help "Use the GP loop")
        <|> flag' NSGA2 (long "nsga2" <> help "Use a NSGA2 algorithm (on multi-objective fitness cases)")
        <|> flag' CustomLoop (long "custom" <> help "Use a custom loop")
      ))
  -- Output Configuration
  <*> switch
      (  long "diversity"
      <> help "When enabled, step report is printed"
      )
  <*> switch
      (  long "step-report"
      <> help "When enabled, step report is printed"
      )
  <*> (not <$> switch
      (  long "no-final-report"
      <> help "When enabled, final report is not printed"
      ))
  <*> switch
      (  long "skip-report"
      <> help "When enabled, execution report is not printed"
      )
  <*> switch
      (  long "pretty"
      <> help "When enabled, the JSON report is formatted with a prettifier."
      )
  <*> switch
      (  long "no-export"
      <> help "When enabled, no csv is not written."
      )
  <*> strOption
      (  long "prefix"
      <> help "Filename prefix. Output files will have this prefix. (Default: \"\")"
      <> value ""
      )
  <*> strOption
      (  long "out-dir"
      <> short 'o'
      <> help "Output directory. Output files will be written in this directory. (Default: ./experiments)"
      <> value "./experiments"
      )

optionsInfo :: ParserInfo MultiRunOptions
optionsInfo = info (runOptions <**> helper)
      (  fullDesc
      <> progDesc "Experiment with random red-black tree generation using g3."
      <> header "g3-rbtree - Generate random red-black trees using g3"
      )

parseMultipleOptions :: IO MultiRunOptions
parseMultipleOptions = execParser optionsInfo
