module G3.EA.Individual (Individual) where

import G3.EA.Fitness (Fitness)

type Individual a = (Fitness, a)
