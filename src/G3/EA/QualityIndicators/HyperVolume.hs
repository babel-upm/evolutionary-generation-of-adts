module G3.EA.QualityIndicators.HyperVolume (wfg, pyHV) where

import Data.List (foldl')

import System.Process
import GHC.IO.Handle (hGetContents)

type Point = [Double]

-- data ProblemType = Min | Max

newtype RefPoint = RefPoint { getPoint :: Point } deriving (Show, Eq)


pyHV :: RefPoint -> [Point] -> IO Double
pyHV (RefPoint ref) points =
  let
    cmd = concat [
      "import numpy as np;",
      "from pymoo.indicators.hv import HV;",
      "ind=HV(ref_point=np.array(" <> show ref <> "));",
      "hv=ind(np.array(" <> show points <> "));",
      "print(hv);"
      ]
  in do
    (_, Just hout, _, _) <- createProcess (proc "python" ["-c", cmd]){ std_out = CreatePipe }
    read <$> hGetContents hout

wfg :: RefPoint -> [Point] -> Double
wfg refPoint points =
  let nPoints = length points
   in sum (exclhv points refPoint <$> [0 .. (nPoints - 1) ])

exclhv :: [Point] -> RefPoint -> Int -> Double
exclhv points refPoint k = inclhv pK refPoint - hvS'
  where
    pK = points !! k
    hvS' = wfg refPoint (nds (limitSet points k))

inclhv :: Point -> RefPoint -> Double
inclhv p (RefPoint refP) = product $ zipWith (\a b -> abs (a - b)) p refP

limitSet :: [Point] -> Int -> [Point]
-- limitSet points k = [ zipWith worse pK (points !! (k + i)) | i <- 0 .. (length points) - k - 1]
limitSet points k = zipWith worse pK <$> drop (k+1) points
  where
    pK = points !! k
    -- TODO: Include ProblemType
    worse :: Double -> Double -> Double
    worse = max

nds :: [Point] -> [Point]
nds = foldl' (\acc p -> if any (dominates p) acc then acc else p : filter (not . (`dominates` p)) acc) []
  where
    dominates p1 p2 = all (uncurry (<=)) (zip p1 p2) && any (uncurry (<)) (zip p1 p2)
