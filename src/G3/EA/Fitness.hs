{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DeriveGeneric #-}
module G3.EA.Fitness
  (
    Optimization(..),
    Fitness (..),
    maximize,
    minimize,
    fitnessNorm,
    fitnessMean,
    fitnessObjectives,
    fitnessValues,
    norm
  )

where

import Data.Ord (Down(..))

import qualified Statistics.Sample as Statistics
import qualified Data.Vector as V

import GHC.Generics (Generic)
import Data.Aeson
import Control.DeepSeq (NFData)

import Test.QuickCheck (Arbitrary, arbitrary, vectorOf, elements, choose, sized)

data Optimization
  = Maximize String Double
  | Minimize String Double deriving (Show, Eq, Generic)

instance ToJSON Optimization
instance FromJSON Optimization
instance NFData Optimization

instance Arbitrary Optimization where
  arbitrary = do
    value <- choose(0.0, 1.0)
    obj <- elements [ Maximize, Minimize ]
    return $ obj "" value

instance Ord Optimization where
  -- (Maximize _ a) <= (Maximize _ b) = Down a <= Down b
  -- (Minimize _ a) <= (Minimize _ b) = a <= b
  -- _ <= _ = True

  -- compare (Maximize _ a) (Maximize _ b) = compare (Down a) (Down b)
  -- compare (Minimize _ a) (Minimize _ b) = compare a b
  compare (Maximize _ a) (Maximize _ b) = compare a b
  compare (Minimize _ a) (Minimize _ b) = compare (Down a) (Down b)
  compare _ _ = EQ

data Fitness
  = SingleObjective Optimization
  | MultiObjective [Optimization] deriving (Show, Eq, Generic)

instance Arbitrary Fitness where
  arbitrary = sized $ \n -> do
    let nObjectives :: Int = ceiling (log (fromIntegral (n+1)) :: Double)
    if nObjectives <= 1
      then SingleObjective <$> arbitrary
      else MultiObjective <$> vectorOf nObjectives arbitrary

instance Ord Fitness where
  (SingleObjective a) <= (SingleObjective b) = a <= b
  (MultiObjective a) <= (MultiObjective b) = a <= b
  _ <= _ = True

instance ToJSON Fitness
instance FromJSON Fitness
instance NFData Fitness

-- newtype Fitness = Fitness {objectives :: [Optimization Double]} deriving (Show, Eq, Ord, Generic)

{- TODO: re-work fitness to maximize/minizme

  data Optimization a = Maximize a | Minimize a deriving (Show, Eq, Ord, Generic)
  data Fitness a = Fitness { objectives :: [Optimization a] } deriving (Show, Eq, Ord, Generic)

  maximize :: a -> Fitness [Optimization a]
  maximize x = Fitness [Maximize x]

  minimize :: a -> Fitness [Optimization a]
  minimize x = Fitness [minimize x]

  TODO: rewrite selection algorithms to work on this optimizations
-}

instance Semigroup Fitness where
  (SingleObjective n) <> (SingleObjective m) = MultiObjective [n, m]
  (SingleObjective n) <> (MultiObjective m) = MultiObjective (n:m)
  (MultiObjective n) <> (SingleObjective m) = MultiObjective (n ++ [m])
  (MultiObjective n) <> (MultiObjective m) = MultiObjective (n ++ m)

maximize :: String -> Double -> Fitness
maximize name n = SingleObjective $ Maximize name n

minimize :: String -> Double -> Fitness
minimize name n = SingleObjective $ Minimize name n

-- Not only the Fitness values can be optimized, but these values can be
-- bounded. These bound can be used to calculate a better unified fitness for
-- multiple objectives. For example, on red-black trees, the standar deviation
-- of the black-depth of the nodes can be bounded as non-negative. More
-- precisely, there is a maximum deviation and therefore and upper bound
-- depending on the max-depth of the tree.
fitnessNorm :: Fitness -> Double
fitnessNorm (SingleObjective (Maximize _ n)) = n
fitnessNorm (SingleObjective (Minimize _ n)) = n
fitnessNorm mo = norm $ fitnessValues mo

fitnessMean :: Fitness -> Double
fitnessMean = mean . fitnessValues

fitnessObjectives :: Fitness -> [Optimization]
fitnessObjectives (SingleObjective o) = [o]
fitnessObjectives (MultiObjective n) = n

fitnessValues :: Fitness -> [Double]
fitnessValues (SingleObjective (Maximize _ n)) = [n]
fitnessValues (SingleObjective (Minimize _ n)) = [n]
fitnessValues (MultiObjective n) = concatMap (fitnessValues . SingleObjective) n

norm :: [Double] -> Double
norm = sqrt . sum .  fmap (**2)

mean :: [Double] -> Double
mean = Statistics.mean . V.fromList
