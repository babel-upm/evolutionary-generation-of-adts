{-# LANGUAGE ScopedTypeVariables #-}
module G3.EA.Operation
  (
    Operation (..),
    OperationOpts (..),

    crossover,
    crossoverPair,
    mutate
  )
where

import Test.QuickCheck (Gen, elements, frequency, vectorOf)
import Data.Maybe (mapMaybe)
import Data.Ratio (numerator, denominator)

data Operation a
  = CrossoverOp (a -> a -> Gen [a])
  | MutationOp (a -> Gen a)

data OperationOpts a = OperationOpts
  {
    size :: Int,
    operations :: [ Operation a ],
    mutationR :: Rational
  }

crossoverOps :: [Operation a] -> [a -> a -> Gen [a]]
crossoverOps = mapMaybe getCrossoverOp
  where
    getCrossoverOp :: Operation a -> Maybe (a -> a -> Gen [a])
    getCrossoverOp (CrossoverOp op) = Just op
    getCrossoverOp _ = Nothing

mutationOps :: [Operation a ] -> [a -> Gen a]
mutationOps = mapMaybe getMutationOp
  where
    getMutationOp :: Operation a -> Maybe (a -> Gen a)
    getMutationOp (MutationOp op) = Just op
    getMutationOp _ = Nothing

-- |
--
-- == Crossover
--

crossover :: Int -> [ Operation a ] -> [a] -> Gen [a]
crossover s ops selectedPop = do
  case crossoverOps ops of
    [] -> return selectedPop
    xOps -> do
      vectorOf (s - length selectedPop) $ do
        a <- elements selectedPop
        b <- elements selectedPop
        op <- elements xOps
        x' <- op a b
        elements x'

crossoverPair :: Rational -> [ Operation a ] -> (a, a) -> Gen [a]
crossoverPair pC ops (x1, x2)
  | null (crossoverOps ops) = return [x1, x2]
  | otherwise =
    let cW = fromInteger (numerator pC)
        iW = fromInteger (denominator pC) - cW
     in frequency [
      (iW, return [x1, x2]),
      (cW, do
          op <- elements $ crossoverOps ops
          op x1 x2
      )]

-- |
--
--  == Mutation
--
mutate :: Rational -> [Operation a] -> a -> Gen a
mutate pM ops i =
  let
    mW :: Int = fromInteger (numerator pM)         -- Mutation weight
    iW :: Int = fromInteger (denominator pM) - mW  -- Identiy weight
  in case mutationOps ops of
    [] -> return i
    mOps -> frequency [
      (iW, return i),
      (mW, do
          op' <- elements mOps
          op' i
      )]



-- mutateSafe :: OperationOpts a -> a -> Gen a
-- mutateSafe options x =
--   let
--     m  = operations options
--     f = fit options                         -- Fitness
--   in case mutationOps m of
--     [] -> return x
--     mOps -> do
--       op' <- elements mOps
--       x' <- op' x
--       return $ if f x' > f x then x' else x
