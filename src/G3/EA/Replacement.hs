module G3.EA.Replacement
  (
    roundRobinTournament,
    roundRobinDiversityTournament
  ) where

import G3.EA.Individual

import Control.Monad (forM)
import qualified Data.Map as M
import Test.QuickCheck (Gen, vectorOf, elements)
import Data.List (sortOn)
import Data.Ord
import Data.Foldable.Levenshtein (levenshtein)

import Debug.Trace

-- | Round-robin tournament
--
-- This mechanism was introduced within Evolutionary Programming, where it is
-- applied to choose μ survivors. However, in principle, it can also be used to
-- select λ parents from a given population of μ.
--
-- The method works by holding pairwise tournament competitions in round-robin
-- format, where each individual is evaluated against q others randomly chosen
-- from the merged parent and offspring populations. For each comparison, a
-- “win” is assigned if the individual is better than its opponent. After
-- finishing all tournaments, the μ individuals with the greatest number of wins
-- are selected. Typically, q = 10 is recommended in Evolutionary
-- Programming. It is worth noting that this stochastic variant of selection
-- allows for less-fit solutions to be selected if they had a lucky draw of
-- opponents. As the value of q increases this chance becomes more and unlikely,
-- until in the limit it becomes deterministic μ + μ.

-- Function to perform a single round of comparison between two individuals
compareIndividuals :: Individual a -> Individual a -> Ordering
compareIndividuals (fit1, _) (fit2, _) = compare fit1 fit2

-- Function to count wins in a round-robin tournament
countWins :: Ord a => [Individual a] -> Int -> Gen (M.Map (Individual a) Int)
countWins population nMatches = do
  results <- forM population $ \individual -> do
    opponents <- vectorOf nMatches (elements population)
    return (individual, length $ filter id $ map (individual >) opponents)
  return $ M.fromList results

-- Function to count diversity  in a round-robin tournament, that is,
countDiversity :: (Ord a) => [Individual a] -> Int -> Gen (M.Map (Individual a) Int)
countDiversity population nMatches = do
  results <- forM population $ \individual -> do
    opponents <- vectorOf nMatches (elements population)
    return (individual, sum $ map (fst . levenshtein individual) opponents)
  return $ M.fromList results

-- Round-robin tournament selection
roundRobinTournament :: Ord a => Int -> Int -> [Individual a] -> Gen [Individual a]
roundRobinTournament size matches population = do
  winsMap <- countWins population matches
  let sortedIndividuals = take size $ sortOn (Down . (winsMap M.!)) population
  return sortedIndividuals

-- roundRobinTournament :: (Show a, Ord a) => Int -> Int -> [Individual a] -> Gen [Individual a]
-- roundRobinTournament size matches population = do
--   winsMap <- traceShowId <$> countWins population matches
--   traceM $ "roundRobinTournament:WinsMap: " ++ show winsMap
--   let sortedIndividuals = take size $ sortOn (Down . (winsMap M.!)) population
--   traceM $ "roundRobinTournament:Sorted Individuals: " ++ show sortedIndividuals
--   return sortedIndividuals

roundRobinDiversityTournament :: Ord a => Int -> Int -> [Individual a] -> Gen [Individual a]
roundRobinDiversityTournament size matches population = do
  winsMap <- countDiversity population matches
  let sortedIndividuals = take size $ sortOn (Down . (winsMap M.!)) population
  return sortedIndividuals

-- roundRobinDiversityTournament :: (Show a, Ord a) => Int -> Int -> [Individual a] -> Gen [Individual a]
-- roundRobinDiversityTournament size matches population = do
--   winsMap <- countDiversity (trace ("roundRobinDiversityTournament:population: " ++ show population) population) matches
--   let sortedIndividuals = take size $ sortOn (Down . (winsMap M.!)) population
--   return sortedIndividuals
