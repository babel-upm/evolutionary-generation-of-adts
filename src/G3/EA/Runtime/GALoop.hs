{-# LANGUAGE ScopedTypeVariables #-}
module G3.EA.Runtime.GALoop ( gaLoop ) where

import G3.EA.Operation
import qualified G3.EA.Replacement as Replacement
import qualified G3.EA.Selection as Selection
import G3.EA.Runtime.Base

import Test.QuickCheck (Gen)

-- import Data.Ratio

gaLoop :: (Show a, Ord a) => GSearch a -> GStep a -> Gen (Maybe (GStep a))
gaLoop search step
  | stepCount step >= maxSteps search = return Nothing
  | otherwise = do
      -- Selection + Crossover
      crossoverOffspring :: [a] <- genOffspringByCrossover search step

      -- Mutate
      let mutate' = mutate (mutationRatio search) (ops search)
      offspring <- mapM mutate' crossoverOffspring
      let newPop = fmap (\x -> (fit search x, x)) offspring

      -- TODO: parametrize amount of matches, currently, q = 10
      nextPop <- Replacement.roundRobinTournament (popSize search) 10 (pop step ++ newPop)

      let step' = GStep { stepCount = succ (stepCount step), pop = nextPop }

      return (Just step')

genOffspringByCrossover :: Ord a => GSearch a -> GStep a -> Gen [a]
genOffspringByCrossover search step =
  let pC = crossoverRatio search
      variate = do
        -- xs <- sus 2 (pop step)
        (_fx, x) <- Selection.tournamentSelection 5 (pop step)
        (_fy, y) <- Selection.tournamentSelection 5 (pop step)
        crossoverPair pC (ops search) (x, y)
  in do
    take (popSize search) . concat <$> sequence (repeat variate)
