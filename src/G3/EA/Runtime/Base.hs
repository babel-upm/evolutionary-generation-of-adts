{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DeriveGeneric #-}
module G3.EA.Runtime.Base
  (
    GSearch (..),
    GStep (..),
    RuntimeStep,
  )
where

import G3.EA.Fitness
import G3.EA.Selection
import G3.EA.Operation
import G3.EA.Individual

import Test.QuickCheck (Gen)

import GHC.Generics (Generic)
import Data.Aeson
import Control.DeepSeq (NFData)

data GSearch a = GSearch
  { -- | Generator of a term
    gen :: Gen a,
    -- | Fitness function for a term
    fit :: a -> Fitness,
    -- | Count of terms considered for each step
    popSize :: Int,
    -- | Maximum number of iterations.
    maxSteps :: Int,  -- TODO: Replace maxSteps with a general stop condition
    -- | Step condition for a given search and step.
    -- stopAt :: GSearch a -> GStep a -> Bool,
    runStep :: GSearch a -> GStep a -> Gen (Maybe (GStep a)),
    -- | Selection type
    selectionType :: Selection a,
    -- | List of possible crossover operations. See @CrossoverOp@.
    ops :: [Operation a],
    -- | Mutation probability
    mutationRatio :: Rational,
    -- | Mutation probability
    crossoverRatio :: Rational,
    -- | Probability Mutation
    probabilityMutation :: Rational,
    -- | Probability Crossover
    probabilityCrossover :: Rational,
    -- | Extra operation performed at the end of each step.
    extra :: [a] -> [a]
  }

-- TODO: Replace basic type with an Individual that can carry additional information (first: self fitness)
-- newtype Individual a = Individual { getIndividual :: (a, Fitness)} deriving (Show, Eq)

data GStep a = GStep
  { -- | Number of the generation
    stepCount :: Int,
    -- | Population of terms for a given
    pop :: [Individual a]
  }
  deriving (Show, Eq, Generic)

type RuntimeStep a = GSearch a -> GStep a -> Gen (Maybe (GStep a))

instance ToJSON a => ToJSON (GStep a)
instance FromJSON a => FromJSON (GStep a)
instance NFData a => NFData (GStep a)
