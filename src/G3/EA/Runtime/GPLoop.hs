module G3.EA.Runtime.GPLoop ( gpLoop ) where

import G3.EA.Operation
import G3.EA.Individual
import G3.EA.Replacement
import G3.EA.Runtime.Base

import Test.QuickCheck (Gen, elements, frequency)

import Debug.Trace

gpLoop :: (Show a, Ord a) => GSearch a -> GStep a -> Gen (Maybe (GStep a))
gpLoop search step
  | stepCount step >= maxSteps search = return Nothing
  | otherwise = do
      -- Selection + Crossover/Mutation
      offspring <- genOffspring search step

      let newPop = fmap (\x -> (fit search x, x)) offspring

      -- TODO: parametrize amount of matches, currently, q = 10
      nextPop <- roundRobinTournament (popSize search) 10 (pop step ++ newPop)
      traceM $ "nextPop: " ++ show nextPop

      return $ Just GStep { stepCount = succ (stepCount step), pop = nextPop }

genOffspring :: GSearch a -> GStep a -> Gen [a]
genOffspring search step =
  let pM = mutationRatio search
      pC = 1 - pM
      variate = mutateOrCrossover pM pC (ops search) (pop step)
  in  take (popSize search) . concat <$> sequence (repeat variate)

mutateOrCrossover :: Rational -> Rational -> [ Operation a ] -> [ Individual a ] -> Gen [a]
mutateOrCrossover pM pC ops' population =
  let precision = 1000
  in frequency [
    (round (pM * precision), do
        i <- elements (snd <$> population)
        i' <- mutate 1 ops' i
        return [i']
        ),
    (round (pC * precision), do
        i1 <- elements (snd <$> population)
        i2 <- elements (snd <$> population)
        crossoverPair 1 ops' (i1, i2)
        )
    ]
