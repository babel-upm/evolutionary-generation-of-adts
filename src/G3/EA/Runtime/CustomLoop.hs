module G3.EA.Runtime.CustomLoop (customLoop) where

import G3.EA.Operation
import G3.EA.Runtime.Base

import qualified Data.List as List

import Test.QuickCheck (Gen)

customLoop :: Ord a => GSearch a -> GStep a -> Gen (Maybe (GStep a))
customLoop search step
  | stepCount step >= maxSteps search = return Nothing
  | otherwise = do
      -- Selection
      selectedPop <- selectionType search (pop step)

      -- Crossover
      crossoverPop <- crossover (popSize search) (ops search) $ snd <$> selectedPop

      -- Mutation
      let pM = mutationRatio search
      mutantsPop <- mapM (mutate pM (ops search)) ((snd <$> selectedPop) ++ crossoverPop)

      -- Ensure extra is apply to new members
      -- Extra could be a filter or transformation on new members
      let mutantsWithExtra = extra search (mutantsPop ++ crossoverPop)

      let mutantsPopWithFitness = (\x -> (fit search x, x)) <$> mutantsWithExtra

      -- let popSizeAdjusted = allPop ++ adjustedPop
      let nextPop = take (popSize search) . List.nub $ List.sortOn fst mutantsPopWithFitness

      -- Call next generation step
      return $ Just GStep { stepCount = succ (stepCount step), pop = nextPop }
