module G3.EA.Runtime.NSGA2 (nsga2Step) where

import G3.EA.Individual
import G3.EA.Fitness
import G3.EA.Selection (paretoFront)
import G3.EA.Runtime.Base
import G3.EA.Operation
import qualified G3.EA.Selection as Selection

import Test.QuickCheck

import Data.Set (Set)
import qualified Data.Set as Set

import Control.Monad (forM_)
import Data.Array (array, (!), elems, )
import Data.Array.ST (runSTArray, newArray, readArray, writeArray)
import Data.Function (on)
import Data.List (sortBy)

nsga2Step :: Ord a => GSearch a -> GStep a -> Gen (Maybe (GStep a))
nsga2Step search step
  | stepCount step >= maxSteps search = return Nothing
  | otherwise = do
      -- Selection + Crossover
      crossoverOffspring <- genOffspringByCrossover search step

      -- Mutate
      let mutate' = mutate (mutationRatio search) (ops search)
      offspring <- mapM mutate' crossoverOffspring
      -- let newPop = fmap (\x -> (fit search x, x)) offspring

      -- TODO: parametrize amount of matches, currently, q = 10
      let nextPop = nsga2replacement search ((snd <$> pop step) ++ offspring)

      let step' = GStep { stepCount = succ (stepCount step), pop = nextPop }

      return (Just step')


-- TODO: Duplicated code from GA loop. Should unify offspring strategies, tuned with parameters
genOffspringByCrossover :: Ord a => GSearch a -> GStep a -> Gen [a]
genOffspringByCrossover search step =
  let pC = crossoverRatio search
      variate = do
        -- xs <- sus 2 (pop step)
        (_fx, x) <- Selection.tournamentSelection 5 (pop step)
        (_fy, y) <- Selection.tournamentSelection 5 (pop step)
        crossoverPair pC (ops search) (x, y)
  in do
    take (popSize search) . concat <$> sequence (repeat variate)

{-

  FROM HERE, CODE HAS BEEN COPIED AND ADAPTED FROM
    - https://hackage.haskell.org/package/moo
    - https://github.com/astanin/moo/blob/master/Moo/GeneticAlgorithm/Multiobjective/NSGA2.hs

  Copyright (c)2011-2013, Sergey Astanin
  Copyright (c)2011, Erlend Hamberg

  And with their license at: https://github.com/astanin/moo/blob/master/LICENSE

-}

-- | Take a pool of phenotypes of size 2N, ordered by the crowded
-- comparison operator, and select N best.
nsga2replacement
    :: Ord a
    => GSearch a
    -> [a]                          -- ^ @pool@ of genomes to select from
    -> [Individual a]               -- ^ @n@ best phenotypes
nsga2replacement search xs =
    -- nsga2Ranking returns genomes properly sorted already
    let n = popSize search
        rankedgenomes = nsga2Ranking search n xs
        selected = fst <$> take n rankedgenomes
    in  selected


nondominatedSort :: Ord a => [Individual a] -> [[Individual a]]
nondominatedSort = go . Set.fromList
  where
    go :: Ord a => Set (Individual a) -> [[Individual a]]
    go p | Set.null p = []
         | otherwise =
           let
             front = paretoFront (Set.toList p)
             p' = Set.difference p (Set.fromList front)
           in front : go p'

crowdingDistances :: [Individual a] -> [Double]
crowdingDistances = crowdingDistances' . fmap (fitnessValues . fst)
  where
    crowdingDistances' :: [[Double]] -> [Double]
    crowdingDistances' [] = []
    crowdingDistances' xs@(objvals:_) =
        let m = length objvals  -- number of objectives
            n = length xs      -- number of genomes
            inf = 1.0/0.0 :: Double
            -- (genome-idx, objective-idx) -> objective value
            ovTable = array ((0,0), (n-1, m-1))
                      [ ((i, objid), (xs !! i) !! objid)
                      | i <- [0..(n-1)], objid <- [0..(m-1)] ]
            -- calculate crowding distances
            distances = runSTArray $ do
              ss <- newArray (0, n-1) 0.0  -- initialize distances
              forM_ [0..(m-1)] $ \objid -> do    -- for every objective
                let ixs = sortByObjective objid xs
                  -- for all inner points
                forM_ (zip3 ixs (drop 1 ixs) (drop 2 ixs)) $ \(iprev, i, inext) -> do
                  sum_of_si <- readArray ss i
                  let si = (ovTable ! (inext, objid)) - (ovTable ! (iprev, objid))
                  writeArray ss i (sum_of_si + si)
                writeArray ss (head ixs) inf   -- boundary points have infinite cuboids
                writeArray ss (last ixs) inf
              return ss
        in elems distances

    sortByObjective :: Int -> [[Double]] -> [Int]
    sortByObjective i = sortIndicesBy (compare `on` (!! i))

    sortIndicesBy :: (a -> a -> Ordering) -> [a] -> [Int]
    sortIndicesBy cmp xs = map snd $ sortBy (cmp `on` fst) (zip xs (iterate (+1) 0))


-- | Solution and its non-dominated rank and local crowding distance.
data RankedSolution a = RankedSolution {
      rs'phenotype :: Individual a
    , rs'nondominationRank :: Int  -- ^ @0@ is the best
    , rs'localCrowdingDistnace :: Double  -- ^ @Infinity@ for less-crowded boundary points
    } deriving (Show, Eq)


-- | Given there is non-domination rank @rank_i@, and local crowding
-- distance @distance_i@ assigned to every individual @i@, the partial
-- order between individuals @i@ and @q@ is defined by relation
--
-- @i ~ j@ if @rank_i < rank_j@ or (@rank_i = rank_j@ and @distance_i@
-- @>@ @distance_j@).
--
crowdedCompare :: RankedSolution a -> RankedSolution a -> Ordering
crowdedCompare (RankedSolution _ ranki disti) (RankedSolution _ rankj distj) =
    case (ranki < rankj, ranki == rankj, disti > distj) of
      (True, _, _) -> LT
      (_, True, True) -> LT
      (_, True, False) -> if disti == distj
                          then EQ
                          else GT
      _  -> GT

-- | Assign non-domination rank and crowding distances to all solutions.
-- Return a list of non-domination fronts.
rankAllSolutions :: Ord a => [Individual a] -> [[RankedSolution a]]
rankAllSolutions  genomes =
    let -- non-dominated fronts
        fronts = nondominatedSort genomes
        -- for every non-dominated front
        frontsDists = map crowdingDistances fronts
        ranks = iterate (+1) 1
    in  map rankedSolutions1 (zip3 fronts ranks frontsDists)
  where
    rankedSolutions1 :: ([Individual a], Int, [Double]) -> [RankedSolution a]
    rankedSolutions1 (front, rank, dists) =
        zipWith (\g d -> RankedSolution g rank d) front dists

-- -- | To every genome in the population, assign a single objective
-- -- value according to its non-domination rank. This ranking is
-- -- supposed to be used once in the beginning of the NSGA-II algorithm.
-- --
-- -- Note: 'nondominatedRanking' reorders the genomes.
-- nondominatedRanking
--     :: Ord a
--     => GSearch a
--     -> [a]                   -- ^ a population of raw @genomes@
--     -> [(a, Double)]
-- nondominatedRanking search xs =
--     let evaluated = zip (fit search <$> xs) xs
--         fronts = nondominatedSort evaluated
--         ranks = concatMap assignRanks (zip fronts (iterate (+1) 1))
--     in ranks
--   where
--     assignRanks :: ([Individual a], Int) -> [(a, Double)]
--     assignRanks (gs, r) = map (\(eg, rank) -> (snd eg, fromIntegral rank)) $ zip gs (repeat r)

-- | To every genome in the population, assign a single objective value
-- equal to its non-domination rank, and sort genomes by the decreasing
-- local crowding distance within every rank
-- (i.e. sort the population with NSGA-II crowded comparision
-- operator)
nsga2Ranking :: Ord a
    => GSearch a                -- ^ a list of @objective@ functions
    -> Int                      -- ^ @n@, number of top-ranked genomes to select
    -> [a]                      -- ^ a population of raw @genomes@
    -> [(Individual a, Double)] -- ^ selected genomes with their non-domination ranks
nsga2Ranking search n xs =
    let evaledGenomes = zip (fit search <$> xs) xs
        fronts = rankAllSolutions evaledGenomes
        frontSizes = map length fronts
        nFullFronts = length . takeWhile (< n) $ scanl1 (+) frontSizes
        partialSize = n - sum (take nFullFronts frontSizes)
        (frontsFull, frontsPartial) = splitAt nFullFronts fronts
        fromFullFronts = concatMap (map assignRank) frontsFull
        fromPartialFront = concatMap (map assignRank
                                      . take partialSize
                                      . sortBy crowdedCompare) $
                           take 1 frontsPartial
    in  fromFullFronts ++ fromPartialFront
  where
    assignRank eg =
        let r = fromIntegral $ rs'nondominationRank eg
            phenotype = rs'phenotype eg
        in  (phenotype, r)
