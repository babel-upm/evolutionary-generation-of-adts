{-# LANGUAGE ScopedTypeVariables #-}
module G3.EA.Selection
  (
    Selection,
    selectionByElite,
    selectionByEliteOn,
    selectionByRelativeEliteOn,
    selectionByRelativeElite,
    selectionByEliteSum,
    selectionByEliteFitnessNorm,
    selectionByEliteFitnessMean,
    selectionByParetoElite,

    sus,
    susWith,
    selectAt,

    tournamentSelection,

    paretoFront,
  )
where

import G3.EA.Individual
import G3.EA.Fitness

import Test.QuickCheck (Gen, shuffle, choose)

import qualified Data.List as List

import qualified Data.Map.Strict as M
import qualified Utils

-- |
--
-- == Selection
--

type Selection a = [Individual a] -> Gen [Individual a]

selectionByElite :: Int -> [Individual a] -> [Individual a]
selectionByElite n = take n . List.sortOn fst

selectionByRelativeElite :: Rational -> [Individual a] -> [Individual a]
selectionByRelativeElite r pop = selectionByElite n pop
  where n = ceiling $ r * fromIntegral (length pop)

selectionByEliteOn :: Ord b => (Fitness -> b) -> Int -> [Individual a] -> [Individual a]
selectionByEliteOn f n = take n . List.sortOn (f . fst)

selectionByRelativeEliteOn :: Ord b => (Fitness -> b) -> Rational -> [Individual a] -> [Individual a]
selectionByRelativeEliteOn f r pop = selectionByEliteOn f n pop
  where n = ceiling $ r * fromIntegral (length pop)

selectionByEliteSum :: Int -> [Individual a] -> [Individual a]
selectionByEliteSum = selectionByEliteOn (sum . fitnessValues)

selectionByEliteFitnessNorm :: Int -> [Individual a] -> [Individual a]
selectionByEliteFitnessNorm = selectionByEliteOn fitnessNorm

selectionByEliteFitnessMean :: Int -> [Individual a] -> [Individual a]
selectionByEliteFitnessMean = selectionByEliteOn fitnessMean

selectionByParetoElite :: Int -> [Individual a] -> Gen [Individual a]
selectionByParetoElite n population = do
  let pop' = paretoDominance population
  if length pop' > n then take n <$> shuffle pop' else return pop'

-- selectionByTournament :: Int -> GSearch a -> [Individual a] -> Gen [Individual a]
-- selectionByTournament = undefined

-- | The 'paretoDominates' function checks if one individual dominates another.
-- An individual A dominates individual B if A is better or equal in all objectives
-- and strictly better in at least one objective.
paretoDominates :: Individual a -> Individual a -> Bool
paretoDominates (f1, _) (f2, _) = paretoDominates' f1 f2
  where
    paretoDominates' :: Fitness -> Fitness -> Bool
    paretoDominates' (SingleObjective n) (SingleObjective m) = optimizationDominates n m
    paretoDominates' (MultiObjective n) (MultiObjective m) =
      and (zipWith optimizationEqDominates n m) && -- All values of n are greater or equal and
       or (zipWith optimizationDominates n m)      -- at least one is greater
    paretoDominates' _ _ = False

    optimizationDominates :: Optimization -> Optimization -> Bool
    optimizationDominates (Maximize _ n) (Maximize _ m) = n > m
    optimizationDominates (Minimize _ n) (Minimize _ m) = n < m
    optimizationDominates _ _ = False

    optimizationEqDominates :: Optimization -> Optimization -> Bool
    optimizationEqDominates (Maximize _ n) (Maximize _ m) = n >= m
    optimizationEqDominates (Minimize _ n) (Minimize _ m) = n <= m
    optimizationEqDominates _ _ = False

-- | The 'paretoDominance' function filters out dominated solutions from the given population
-- to obtain the non-dominated (Pareto-optimal) solutions.
paretoDominance :: [Individual a] -> [Individual a]
paretoDominance population = filter isNonDominated population
  where
    isNonDominated ind = not $ any (`paretoDominates` ind) population

paretoFront :: [Individual a] -> [Individual a]
paretoFront = paretoDominance

susWith :: (Fitness -> Double) -> Int -> [Individual a] -> Gen [a]
susWith f n pop =
  let scanFitness :: [Double] = scanl1 (+) (map (f . fst) pop)
      totalFitness = last scanFitness
      upperBound = totalFitness / fromIntegral n
   in do
    start <- choose (0.0, upperBound)
    let selectionPoints = [ start + (fromIntegral x * upperBound) | x <- [0..(n-1)] ]
    -- return $ traceShow ("selectAt", selectionPoints, zip (map (f . fst) pop) scanFitness) $ selectAt [] selectionPoints scanFitness (snd <$> pop)
    return $ selectAt [] selectionPoints scanFitness (snd <$> pop)
      -- [0.4,0.8]
      -- [0.17,0.23,0.56,0.81,0.90,1]
      --
selectAt :: [a]      -- Accumulated Selected Poulation
         -> [Double] -- Stop points
         -> [Double] -- Cumulative fitness
         -> [a]      -- Population
         -> [a]      -- SelectedResult
selectAt selected [] _ _ = selected
selectAt _selected ps@(_:_) [] _ = error $ "SUS/SelectAt: Run out of cumulative fitness whileselection points remaining\n" <> show ps
selectAt _selected _ _  [] = error "SUS/SelectAt: Not enough population to perform SUS"
selectAt selected (p:ps) (accF:fs) (x:xs)
  | p <= accF = selectAt (x:selected) ps fs xs
  | otherwise = selectAt selected (p:ps) fs xs
-- selectAt ps fs xs = error $ concat ["Unexpected SUS selection. ", "ps: ", show ps, "fs: ", show fs]

sus :: Int -> [Individual a] -> Gen [a]
sus = susWith (sum . fitnessValues)

tournamentSelection :: Ord a => Int -> [Individual a] -> Gen (Individual a)
tournamentSelection k population = do
  participants <- take k <$> shuffle population
  let wins = Utils.count [ winner x y | x <- participants, y <- participants]
  return $ fst (Utils.maximumOn snd (M.toList wins))
    where
      winner :: Individual a -> Individual a -> Individual a
      winner x@(fx, _) y@(fy, _)
        | fx > fy = x
        | otherwise = y
