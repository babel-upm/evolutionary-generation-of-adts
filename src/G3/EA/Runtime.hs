{-# LANGUAGE ScopedTypeVariables #-}
module G3.EA.Runtime
  (
    g3,
    g3InitialStep,
    g3Step,
    g3From,
    g3Stream,

    gaLoop,
    gpLoop,
    nsga2Step,
    customLoop,

    GSearch (..),
    GStep (..),
    RuntimeStep,
  )
where

-- import G3.EA.Selection
-- import G3.EA.Operation
import G3.EA.Individual
import G3.EA.Runtime.Base
import G3.EA.Runtime.GALoop
import G3.EA.Runtime.GPLoop
import G3.EA.Runtime.CustomLoop
import G3.EA.Runtime.NSGA2

import Test.QuickCheck (Gen, vectorOf)


-- debugStep :: a -> b -> b
-- debugStep = const id

-- debugStep :: (Show a, Show b) => GStep a -> b -> b
-- debugStep step b = trace (header) b
--   where
--     header = "======= STEP " ++ show (stepCount step)++ " ======="
--     -- debugPop = "\n pop: " ++ concatMap (\t -> "\n\t\t" ++ show t) (zip [1..] (pop step))

-- |
--
-- = Genetic Algorithm
--
-- In the genetic algorithm we iterate through the stages of:
--
--  1. Selection
--  2. Crossover
--  3. Mutation
g3 :: GSearch a -> Gen [GStep a]
g3 gsearch = do
  step0 <- g3InitialStep gsearch
  g3From gsearch step0

g3InitialStep :: GSearch a -> Gen (GStep a)
g3InitialStep search = do
  initialPop <- vectorOf (popSize search) (gen search)
  let initialFitness = fit search <$> initialPop
  return $ GStep {stepCount = 0, pop = zip initialFitness initialPop}

g3Step :: GSearch a -> GStep a -> Gen (Maybe (GStep a))
g3Step search = runStep search search

g3From :: GSearch a -> GStep a -> Gen [GStep a]
g3From search step
  | stepCount step >= maxSteps search = return []
  | otherwise = do
      mStep' <- runStep search search step
      case mStep' of
        Nothing -> return []
        Just step' -> do
          steps' <- g3From search step'
          return (step':steps')

g3Stream :: (Individual a -> Bool) -> GSearch a -> Gen [Individual a]
g3Stream isValid search = do
  steps <- g3 search
  return (concatMap (filter isValid . pop) steps)
