{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
module G3.Experiment (
  Experiment(..),
  runExperiment,
  runExperiment_,

  Report,
  BaseReport(..)
) where

import G3
import G3.Experiment.StepReport (StepReportAux(..), mkStepReport)
import G3.Experiment.FinalReport (FinalReportAux(..), mkFinalReport, BaseReport(..))
import G3.EA.Runtime (RuntimeStep)

import Test.QuickCheck

import Control.Monad (when)

import GHC.Generics
import Control.DeepSeq (NFData, deepseq)
import System.CPUTime
import System.IO
import qualified Data.ByteString.Lazy as BL
import Data.Aeson
import Data.Ratio

-- import Data.Set (Set)
-- import qualified Data.Set as Set

data Experiment a step final = E
  { title :: String
  -- Evolutionary Operations
  , searchGenerator :: Gen a
  , searchFitness :: a -> Fitness
  , searchSelection :: Selection a
  , searchOperators :: [Operation a]
  , searchStep :: RuntimeStep a
  -- Evolutionary Params
  , searchGenerations :: Int
  , searchPopulation :: Int
  , searchMutation :: Int
  , searchCrossover :: Int
  , targetSize :: Int
  -- Report
  , checkDiversity :: Bool
  , showStepReport :: Bool
  , stepReportExtra :: GStep a -> step
  , showFinalReport :: Bool
  , validReport :: Individual a -> Bool
  , selectionReport :: [Individual a] -> [Individual a]
  , extraReportFinal :: [GStep a] -> BaseReport a -> final
  }

data ReportType = Step | Final deriving (Show, Generic)

instance ToJSON ReportType
instance FromJSON ReportType

data Report a = Report
  { reportType :: ReportType
  , report :: a
  } deriving (Show, Generic)

instance ToJSON a => ToJSON (Report a)
instance FromJSON a => FromJSON (Report a)

runExperimentStep :: (ToJSON a, NFData a, ToJSON s) => Experiment a s f -> GSearch a -> GStep a -> IO (Maybe (GStep a))
runExperimentStep experiment search lastStep = do
  t1 <- getCPUTime
  step <- generate (g3Step search lastStep)
  t2 <- step `deepseq` getCPUTime
  let time = fromIntegral (t2-t1) * 1e-12 -- Time in seconds

  case (step, showStepReport experiment) of
    (Just s, True) -> do
      outReport (Report Step (mkStepReport (mkStepReportAux experiment time) s))
      return step
    _ -> return step

runExperiment_ :: (ToJSON a, Ord a, NFData a, ToJSON s, ToJSON f) => Experiment a s f -> IO ()
runExperiment_ e = runExperiment e >> return ()

runExperiment :: (ToJSON a, Ord a, NFData a, ToJSON s, ToJSON f) => Experiment a s f -> IO (GStep a)
runExperiment experiment =  do
  t1 <- getCPUTime

  let search' = GSearch
        { gen = searchGenerator experiment,
          fit = searchFitness experiment,
          popSize = searchPopulation experiment,
          maxSteps = searchGenerations experiment,
          selectionType = searchSelection experiment,
          runStep = searchStep experiment,
          probabilityMutation = min 100 (fromIntegral (searchMutation experiment)) % 100,
          probabilityCrossover = min 100 (fromIntegral (searchCrossover experiment)) % 100,
          ops = searchOperators experiment,
          mutationRatio = min 100 (fromIntegral (searchMutation experiment)) % 100,
          crossoverRatio = min 100 (fromIntegral (searchCrossover experiment)) % 100,
          extra = id
        }

  initialStep <- generate (g3InitialStep search')
  when (showStepReport experiment) $ do
    t2 <- initialStep `deepseq` getCPUTime
    let time = fromIntegral (t2-t1) * 1e-12 -- Time in seconds
    outReport (Report Step (mkStepReport (mkStepReportAux experiment time) initialStep))

  steps <- loop (runExperimentStep experiment search') (Just initialStep)

  t2 <- steps `deepseq` getCPUTime
  let time = fromIntegral (t2-t1) * 1e-12 -- Time in seconds

  when (showFinalReport experiment) $
    let final = Report Final (mkFinalReport (mkFinalReportAux experiment time) steps)
    in outReport final

  return $ last steps

  where
    loop :: (GStep a -> IO (Maybe (GStep a))) -> Maybe (GStep a) -> IO [GStep a]
    loop _ Nothing = pure []
    loop advanceStep (Just s) = do
      nextStep <- advanceStep s
      followingSteps <- loop advanceStep nextStep
      return (s:followingSteps)

data DiversityReport = DiversityReport {
  drTitle :: String,
  drValid :: Int,
  drValue :: Double
  } deriving (Show, Generic)

instance ToJSON DiversityReport
instance FromJSON DiversityReport

mkStepReportAux :: Experiment a s f -> Double -> StepReportAux a s
mkStepReportAux experiment time =
  StepReportAux
    { auxStepTitle = title experiment
    , auxStepTime = time
    , auxStepMkExtra = stepReportExtra experiment
    }

mkFinalReportAux :: Experiment a s f -> Double -> FinalReportAux a f
mkFinalReportAux experiment time =
  FinalReportAux
    { auxTitle = title experiment
    , auxTime = time
    , auxSelection = selectionReport experiment
    , auxValid = validReport experiment
    , auxMkExtra = extraReportFinal experiment
    }

outReport :: ToJSON a => Report a -> IO ()
outReport r = do
  (BL.hPutStr stdout . (<> "\n") . encode) r
  hFlush stdout
