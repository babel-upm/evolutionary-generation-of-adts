module Utils
  (
    maximumOn,
    count,
    countBy,
    paretoDominates,
    fitnessToDouble,
    fitnessToDoubleWithBounds,
    dateTimePrefix,
    replicateMWithIndex_,
    calcDiversity,
    calcDiversityBySize,
    partitionBy
  ) where

import Data.List (maximumBy, tails)
import Data.Ord (comparing)

import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map

import Data.Set (Set)
import qualified Data.Set as Set

import Data.Time.Clock
import Data.Time.Format

import Data.Foldable.Levenshtein

-- Necessary imports for parallelism
import Control.Parallel (par, pseq)
import Control.Parallel.Strategies (parMap, rpar)

maximumOn :: Ord b => (a -> b) -> [a] -> a
maximumOn f = snd . maximumBy (comparing fst) . map (\x -> let y = f x in y `seq` (y, x))

countBy :: (Foldable f, Ord b) => (a -> b) -> f a -> Map b Int
countBy f = foldr (countBy' f) mempty
  where
    countBy' :: Ord b => (a -> b) -> a -> Map b Int -> Map b Int
    countBy' f' x = Map.insertWith (const (+ 1)) (f' x) 1

count :: (Foldable f, Ord a) => f a -> Map a Int
count = countBy id

paretoDominates :: Ord a => [a] -> [a] -> Bool
paretoDominates a b = and (zipWith (>=) a b) && or (zipWith (>) a b)

fitnessToDouble :: [Double] -> [Double] -> Double
fitnessToDouble upperBounds values = fitnessToDoubleWithBounds upperBounds values / fromIntegral (length values)

fitnessToDoubleWithBounds :: [Double] -> [Double] -> Double
fitnessToDoubleWithBounds upperBounds values = sum (zipWith (/) values upperBounds)

dateTimePrefix :: IO String
dateTimePrefix = formatTime defaultTimeLocale "%Y%m%d%H%M%S" <$> getCurrentTime

replicateMWithIndex_                 :: (Applicative m) => Int -> (Int -> m a) -> m ()
{-# INLINABLE replicateMWithIndex_ #-}
{-# SPECIALISE replicateMWithIndex_ :: Int -> (Int -> IO a) -> IO () #-}
{-# SPECIALISE replicateMWithIndex_ :: Int -> (Int -> Maybe a) -> Maybe () #-}
replicateMWithIndex_ cnt0 f =
    loop cnt0
  where
    loop cnt
        | cnt <= 0  = pure ()
        | otherwise = f cnt *> loop (cnt - 1)

-- Calculate average diversity given a list flattened terms
-- calcDiversity :: Eq a => [[a]] -> Double
-- calcDiversity l = sum ld / fromIntegral (length ld)
--   where
--     ld = [ fst (levenshtein a b) | a <- l, b <- l, a /= b ]

calcDiversity :: Eq a => [[a]] -> Double
calcDiversity l = totalDistance / totalPairs
  where
    uniquePairs = [(a, b) | (a:bs) <- tails l, b <- bs]
    ld = parMap rpar (\(a, b) -> fst (levenshtein a b)) uniquePairs
    totalDistance = fromIntegral (sum ld)
    totalPairs = fromIntegral (length ld)

calcDiversityBySize :: Ord a => Set [a] -> Map Int Double
calcDiversityBySize xs = Map.map (calcDiversity . Set.toList) mBySize
  where
    mBySize = foldr insertByLength Map.empty xs

    insertByLength :: Ord a => [a] -> Map Int (Set [a]) -> Map Int (Set [a])
    insertByLength v = Map.insertWith Set.union (length v) (Set.singleton v)

partitionBy :: (Ord k, Ord a) => (a -> k) -> Set a -> Map k (Set a)
partitionBy f = Set.foldr (insertBy f) Map.empty
  where
    insertBy f' xs = Map.insertWith Set.union (f' xs) (Set.singleton xs)
