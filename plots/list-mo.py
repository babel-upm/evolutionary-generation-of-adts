import json
import numpy as np
import matplotlib.pyplot as plt
from collections import defaultdict, Counter
import statistics
import os
import sys
import seaborn as sns
import pandas as pd
from pathlib import Path
import pickle # searialize data
import random

from pymoo.indicators.gd import GD
from pymoo.indicators.gd_plus import GDPlus
from pymoo.indicators.igd import IGD
from pymoo.indicators.igd_plus import IGDPlus
from pymoo.indicators.hv import HV
from pymoo.util.nds.non_dominated_sorting import NonDominatedSorting

import logging

# Configure the logger (Best Practice)
logging.basicConfig(level=logging.ERROR, format='%(asctime)s - %(levelname)s - %(message)s')

line_styles = ['-', '--', '-.', ':']
markers = ['o', 's', '^', 'd']  # Circle, Square, Triangle, Diamond

def styles(runTitle):
    if runTitle in ['A']:
        return { 'title': runTitle, 'line': 'solid',  'marker': 'o', 'color': 'blue' }
    if runTitle in ['B', 'C', 'D']:
        return { 'title': runTitle, 'line': 'dashed', 'marker': 's', 'color': 'green' }
    if runTitle in ['D', 'E', 'F']:
        return { 'title': runTitle, 'line': 'dashdot', 'marker': '^', 'color': 'orange' }
    if runTitle == ['G', 'H', 'I']:
        return { 'label': runTitle, 'line': 'dotted', 'marker': 'o', 'color': 'purple' }
    return { 'title': runTitle, 'line': ':',  'marker': 'o', 'color': 'red' }

def metadata(title):
    splits = title.split("_")
    ts = splits[0]
    rep = splits[1].split('.')[0]
    title = "_".join(splits[1:-6]).split('.')[1]
    [g, p, m, x, s, a] = splits[-6:]
    return {
        'ts': int(ts),
        'rep': int(rep),
        'title': title,
        'g': int(g[:-1]),
        'p': int(p[:-1]),
        'm': int(m[:-1]),
        'x': int(x[:-1]),
        's': int(s[:-1]),
        'a': a
    }

def pareto_fronts(key, max_size, min_size=0, target_size=30):
    return {
    'MOO_B': [ [0, size] for size in range(min_size, max_size) ],
    'MOO_C': [ [0, size] for size in range(min_size, max_size) ],
    #
    'MOO_D': [ [0, size] for size in range(min_size, 120) ],
    'MOO_E': [ [0, size] for size in range(min_size, 120) ],
    'MOO_F': [ [0, size] for size in range(min_size, 85) ],
    #
    'MOO_G': [ [size * size / target_size, size] for size in range(min_size, 30) ],
    'MOO_H': [ [size * size / target_size, size] for size in range(min_size, 30) ],
    'MOO_I': [ [   1 * size / target_size, size] for size in range(min_size, 30) ],
    }[key]

def transformation(key):
    return {
        'MOO_B': (1,-1),
        'MOO_C': (1,-1),
        #
        'MOO_D': (1,-1),
        'MOO_E': (1,-1),
        'MOO_F': (-1,-1),
        #
        'MOO_G': (1,-1),
        'MOO_H': (1,-1),
        'MOO_I': (1,-1),
    }[key]

def reference_point(key, max_x=1):
    return {
        'MOO_B': (20, 0),
        'MOO_C': (400, 0),
        #
        'MOO_D': (1, 0),
        'MOO_E': (1, 0),
        'MOO_F': (0, 0),
        #
        'MOO_G': (50, 0),
        'MOO_H': (50, 0),
        'MOO_I': (1, 0),
    }[key]

# GS reflects the extent of spread for the solutions in PFc.Let e = (e_1, ..., e_m)
# be the extreme solution of the PFref ,where e_i is the maximum value of objective
# function f_i . Also, we define d(e_i,PFc) as the minimum Euclidean distance from
# e_i to PFc. Given a solution s, let id(s, PFc) = d(s,PFc \ {s}) be the minimum
#   distance of the solution s from all the other solutions in PFc, and \overline{id} be the
#   mean value of id(s, PFc) across all the solutions of PFc. GS is defined as:

# \[
# GS\left(P F^c\right)=\frac{\sum_{i=1}^m d\left(e_i, P F^c\right)+\sum_{s_j \in P F^c}\left|i d\left(s_j, P F^c\right)-\overline{i d}\right|}{\sum_{i=1}^m d\left(e_i, P F^c\right)+\left|P F^c\right| * \overline{i d}}
# \]
def generational_spread(pfc, e_i):
    def euclidean_distance(point1, point2):
        return np.sqrt(np.sum((np.array(point1) - np.array(point2))**2))

    # Step 1: Calculate d(e_i, PFc)
    def d(e_i, pfc):
        return min(euclidean_distance(e_i, s) for s in pfc)

    d_e_i_pfc = [d(e_i, pfc) for e_i in e]

    # Step 2: Calculate id(s_j, PFc) for each s_j in PFc
    def id(s, pfc):
        return min(euclidean_distance(s, other) for other in pfc if not np.array_equal(s, other))

    id_sj_pfc = [id(s, pfc) for s in pfc]

    # Step 3: Calculate mean id
    mean_id = np.mean(id_sj_pfc)

    # Step 4: Calculate numerator and denominator for GS formula
    numerator = sum(d_e_i_pfc) + sum(abs(id_val - mean_id) for id_val in id_sj_pfc)
    denominator = sum(d_e_i_pfc) + len(pfc) * mean_id

    # Step 5: Calculate GS
    GS = numerator / denominator

    return GS

def sort_by_intkey(d):
    return {str(k): d[str(k)] for k in sorted(int(x) for x in d.keys())}

def add_report(data, groups):
    global MAX_SIZE
    meta = metadata(data["report"]["runTitle"])
    # key = f"{meta['title']}_{meta['a']}"
    key = meta['title']

    # Data
    time = data["report"]["runTime"]
    valid =  data["report"]["validIndividualsCount"]
    unique =  data["report"]["uniqueIndividualsCount"]
    max_size =  data["report"]["extra"]["maxSize"]
    unique_fitness = data["report"]["extra"]["uniqueFitnessSummary"]

    # Add report
    groups[key]['time'].append(time)
    groups[key]['valid'].append(valid)
    groups[key]['unique'].append(unique)
    groups[key]['max_size'].append(max_size)
    groups[key]['hv'].append(unique_fitness)
    groups[key]['gd'].append(unique_fitness)
    groups[key]['gdp'].append(unique_fitness)
    groups[key]['igd'].append(unique_fitness)
    groups[key]['igdp'].append(unique_fitness)
    return

def apply_stats(data):
    total = len([0 for title in data.keys() for k in data[title].keys() ])
    current = 0
    for title, tData in data.items():
        for k, measurements in tData.items():
            current += 1
            print(f"Calculating stats... {current:5} / {total:5}", end="\r")
            if k in ['time', 'valid', 'unique', 'max_size']:
                stats = single_stats(measurements)
                stats['raw'] = measurements
                data[title][k] = stats
            if k == 'hv':
                hv_stats = hv(measurements, title)
                stats = single_stats(hv_stats)
                stats['raw'] = measurements
                data[title][k] = stats
            if k == 'gd':
                distance_stats = distance_pi(measurements, key=title, pi=GD)
                stats = single_stats(distance_stats)
                stats['raw'] = measurements
                data[title][k] = stats
            if k == 'gdp':
                distance_stats = distance_pi(measurements, key=title, pi=GDPlus)
                stats = single_stats(distance_stats)
                stats['raw'] = measurements
                data[title][k] = stats
            if k == 'igd':
                distance_stats = distance_pi(measurements, key=title, pi=IGD)
                stats = single_stats(distance_stats)
                stats['raw'] = measurements
                data[title][k] = stats
            if k == 'igdp':
                distance_stats = distance_pi(measurements, key=title, pi=IGDPlus)
                stats = single_stats(distance_stats)
                stats['raw'] = measurements
                data[title][k] = stats
    print("[DONE]")

def single_stats(measurements): # list of values
    stats = {}
    stats['mean'] = statistics.mean(measurements)
    stats['median'] = statistics.median(measurements)
    stats['sum'] = sum(measurements)
    if len(measurements) > 2:
        stats['variance'] = statistics.variance(measurements)
        stats['stdev'] = statistics.stdev(measurements)
    if len(measurements) > 0:
        stats['min'] = min(measurements)
        stats['max'] = max(measurements)
    return stats

def hv(multi, key=None):
    result = []
    # Ref point is defined as follows
    # (x=1) means not sorted at all for a -1 elem fitness
    # (y=1) is for a -1 max size, because fitness values are reverse (* (-1)) to make it a minimization representation.
    (x, y) = transformation(key)
    [a,b] = reference_point(key)
    ref_point = np.array([x * a, y * b])
    for fitness in multi:
        ind = HV(ref_point=ref_point)
        fs = np.array([ np.array([ x * f1, y * f2]) for [f1,f2] in fitness ])
        hv = ind(fs)
        result.append(hv)
    return result

def distance_pi(multi, key=None, title=None, pi=None, max_size=30):
    result = []
    (x,y) = transformation(key)
    pf = np.array([ [x*p1, y*p2] for [p1, p2] in pareto_fronts(key, max_size) ])
    for fitness in multi:
        ind = pi(pf)
        fs = np.array([ np.array([x*f1, y*f2]) for [f1,f2] in fitness ])
        gd = ind(fs)
        result.append(gd)
    return result

def tabular(data):
    width = 8
    print(" & ".join([ x.ljust(width) for x in ["ID", "Time (s)", "", "Valid", "", "", "Length", "", ""] ]), "\\\\")
    print(" & ".join([ x.ljust(width) for x in ["", "mean", "stdev", "total", "mean", "stdev", "max", "mean", "stdev"] ]), "\\\\ \\midrule")
    for title in sorted(data.keys()):
        fields = [
            title,
            f"{data[title]['time']['mean']:.2f}",
            f"±{data[title]['time']['stdev']:.2f}",
            f"{data[title]['valid']['sum']}",
            f"{data[title]['valid']['mean']:.2f}",
            f"±{data[title]['valid']['stdev']:.2f}",
            f"{data[title]['max_size']['max']}",
            f"{data[title]['max_size']['mean']:.2f}",
            f"±{data[title]['max_size']['stdev']:.2f}"
        ]

        fields = " & ".join([ x.ljust(width) for x in fields ])
        print(fields, '\\\\')

def tabular_pi(data, pi=None):
    if pi == None:
        logging.error("No performance indicator in tabular_pi")
        return

    print("=========", pi, "=========")
    width = 10
    new_data = {}
    print(" & ".join([x.ljust(width) for x in ['title', 'max', 'min', 'mean', 'stdev']]))
    for title in sorted(data.keys()):
        fields = [
            title,
            f"{data[title][pi]['max']:.2f}",
            f"{data[title][pi]['min']:.2f}",
            f"{data[title][pi]['mean']:.2f}",
            f"±{data[title][pi]['stdev']:.2f}",
        ]
        print(" & ".join([f"{x}".ljust(width) for x in fields]))

TMP_FILE = 'tmp_data.pkl'
def load_data_from_tmp():
    with open(TMP_FILE, 'rb') as file:
        data = pickle.load(file)
    return data

def save_data_to_tmp(data):
    with open(TMP_FILE, 'wb') as file:
        pickle.dump(data, file)

def default_groups():
    return {
        'time': list(),
        'unique': list(),
        'valid': list(),
        'max_size': list(),
        'hv': list(),
        'gd': list(),
        'gdp': list(),
        'igd': list(),
        'igdp': list(),
    }

def all_pareto_fronts(data, x, y, chunk_size=6000):
    all = [ [x * f1, y * f2] for run in data for [f1,f2] in run ]
    random.shuffle(all)
    chunks = [np.array(all[i:i + chunk_size]) for i in range(0, len(all), chunk_size)]

    while len(chunks) > 1:
        print(f'Currently with {len(chunks)} chunks...', end='\r', flush=True)
        acc_cpf = list()
        for chunk in chunks:
            nds = NonDominatedSorting().do(chunk, only_non_dominated_front=True)
            cpf = chunk[nds]
            acc_cpf.append(cpf)
        acc_cpf = [ ndp for cpf in acc_cpf for ndp in cpf ]
        random.shuffle(acc_cpf)
        chunks = [ np.array(acc_cpf[i:i + chunk_size]) for i in range(0, len(acc_cpf), chunk_size) ]
    print(f'Final chunk ({len(chunks)}) of {len(chunks[0])} elems')

    nds = NonDominatedSorting().do(chunks[0], only_non_dominated_front=True)
    cpf = chunks[0][nds]
    cpf = cpf[cpf[:, 0].argsort()]

    return cpf


def plot_moo_subgrid(data, sub_f=10000, x=1, y=1, pf=None, ax=None, x2=lambda a: a, y2=lambda b: b):
    fitness = data['hv']['raw']
    fitness_sub = fitness
    fitness_sub = [[a*x, b*y] for run in fitness for [a, b] in run]
    # fitness_sub = random.sample(fitness_sub, sub_f)
    fitness_sub = np.array(fitness_sub)

    final_f_x = [ x2(value) for value in fitness_sub[:, 0]]
    final_f_y = [ y2(value) for value in fitness_sub[:, 1]]
    ax.scatter(final_f_x, final_f_y, label='Fitness values', color='#87CEEB', alpha=0.9, s=5)

    pf = [[x2(a*x), y2(b*y)] for [a, b] in pf]
    pf_x, pf_y = zip(*pf)
    ax.plot(pf_x, pf_y, color='red', label='Reference pareto front')

    # all_cpf = all_pareto_fronts(data['hv']['raw'], x, y)
    all_cpf = all_pareto_fronts([fitness_sub], 1, 1)
    all_cpf = all_cpf[all_cpf[:, 0].argsort()]
    all_cpf_x = [x2(value) for value in all_cpf[:, 0]]
    all_cpf_y = [y2(value) for value in all_cpf[:, 1]]
    ax.plot(all_cpf_x, all_cpf_y, color='blue', label='Computed pareto front', marker='o', markersize=6)
    ax.scatter(all_cpf_x, all_cpf_y, color='blue', s=5, alpha=0.3)
    # ax.legend()

def plot_moo_grid(groups):
    fig, axs = plt.subplots(3, 3, figsize=(15, 15))

    axs[0, 0].set_title('MO_B')
    axs[0, 0].set_ylim(0, 120)
    plot_moo_subgrid(groups['MOO_B'], x=1, y=-1, y2=lambda b: -b, pf=[[0, size] for size in range(0, 200)], ax=axs[0, 0])
    (x,y) = transformation('MOO_B')
    (rx,ry) = reference_point('MOO_B')
    axs[0, 0].plot(rx * x, ry * y, color='orange', marker='D', linestyle='None', label='HV Reference Point')

    axs[0, 1].set_title('MO_C')
    axs[0, 1].set_ylim(0, 500)
    # axs[0, 1].set_xscale('log')
    # axs[0, 1].set_yscale('log')
    plot_moo_subgrid(groups['MOO_C'], x=1, y=-1, y2=lambda b: -b, pf=[[0, size] for size in range(0, 500)], ax=axs[0, 1])
    (x,y) = transformation('MOO_C')
    (rx,ry) = reference_point('MOO_C')
    axs[0, 1].plot(rx * x, ry * y, color='orange', marker='D', linestyle='None', label='HV Reference Point')

    # Leave axs[0, 2] blank
    axs[0, 2].axis('off')

    axs[1, 0].set_title('MO_D')
    plot_moo_subgrid(groups['MOO_D'], x=1, y=-1,  y2=lambda b: -b, pf=[[0, size] for size in range(0, 150)], ax=axs[1, 0])
    (x,y) = transformation('MOO_D')
    (rx,ry) = reference_point('MOO_D')
    axs[1, 0].plot(rx, ry, color='orange', marker='D', linestyle='None')
    axs[1, 0].set_ylim(0, 135)

    axs[1, 1].set_title('MO_E')
    plot_moo_subgrid(groups['MOO_E'], x=1, y=-1,  y2=lambda b: -b, pf=[[0, size] for size in range(0, 150)], ax=axs[1, 1])
    (x,y) = transformation('MOO_E')
    (rx,ry) = reference_point('MOO_E')
    axs[1, 1].plot(rx, ry, color='orange', marker='D', linestyle='None')
    axs[1, 1].set_ylim(0, 135)

    axs[1, 2].set_title('MO_F')
    plot_moo_subgrid(groups['MOO_F'], x=-1, y=-1, x2=lambda a: a + 1, y2=lambda b: -b, pf=[[1, size] for size in range(0, 150)], ax=axs[1, 2])
    (x,y) = transformation('MOO_F')
    (rx,ry) = reference_point('MOO_F')
    axs[1, 2].plot(rx, ry, color='orange', marker='D', linestyle='None')
    axs[1, 2].set_ylim(0, 135)

    axs[2, 0].set_title('MO_G')
    axs[2, 0].set_ylim(0,100)
    plot_moo_subgrid(groups['MOO_G'], x=-1, y=1, x2=lambda a: -a, pf=[[size * size / 30, size] for size in range(1, 30)], ax=axs[2, 0])
    (x,y) = transformation('MOO_G')
    (rx,ry) = reference_point('MOO_G')
    axs[2, 0].plot(rx, ry, color='orange', marker='D', linestyle='None')

    axs[2, 1].set_title('MO_H')
    axs[2, 1].set_ylim(0,100)
    plot_moo_subgrid(groups['MOO_H'], x=-1, y=1,  x2=lambda a: -a, pf=[[size * size / 30, size] for size in range(1, 30)], ax=axs[2, 1])
    (x,y) = transformation('MOO_H')
    (rx,ry) = reference_point('MOO_H')
    axs[2, 1].plot(rx, ry, color='orange', marker='D', linestyle='None')

    axs[2, 2].set_title('MO_I')
    axs[2, 2].set_ylim(0,100)
    plot_moo_subgrid(groups['MOO_I'], x=-1, y=1,  x2=lambda a: -a, pf=[[(size / 30), size] for size in range(1, 30)], ax=axs[2, 2])
    (x,y) = transformation('MOO_I')
    (rx,ry) = reference_point('MOO_I')
    axs[2, 2].plot(rx, ry, color='orange', marker='D', linestyle='None')

    # Creating legend in axs[0, 2]
    handles, labels = axs[0, 0].get_legend_handles_labels()
    fig.legend(handles, labels)

    plt.tight_layout()
    plt.savefig('sorted-list-mo-grid', format='jpg', dpi=600)



def summary(source):
    # Our base aggregation is #title_Mprob => stats{ valid => [], time: [], ... }
    groups = defaultdict(default_groups)

    if ("--no-cache" not in sys.argv) and os.path.exists(TMP_FILE):
        print("[LOG] Loading data from tmp file", TMP_FILE)
        groups = load_data_from_tmp()
    else:
        print("[LOG] Cached data not found at tmp file", TMP_FILE)
        for line in source:
            data = json.loads(line)
            add_report(data, groups)
        apply_stats(groups)
        save_data_to_tmp(groups)

    tabular(groups)
    tabular_pi(groups, pi='hv')
    tabular_pi(groups, pi='gd')
    tabular_pi(groups, pi='gdp')
    tabular_pi(groups, pi='igd')
    tabular_pi(groups, pi='igdp')

    plot_moo_grid(groups)

def main():
    source = None
    print(len(sys.argv), sys.argv)
    if len(sys.argv) >= 2:
        input_file = sys.argv[1]
        print("="*30, input_file, "="*30)
        source = open(input_file, 'r')
        summary(source)
        source.close()
    else:
        summary(sys.stdin)

if __name__ == '__main__':
    main()
