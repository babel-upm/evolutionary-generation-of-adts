# First, run with:
#
# parallel -j 9 'stack run list -- --gp --case {} -s 20 -m 5 -x 95 -p 100 -g 1000 -r 10' ::: A B C D E F G H I > test.json
# parallel -j 15 'stack run list -- --gp --case {1} -s {2} -m 5 -x 95 -p 100 -g 1000 -r {3}' ::: G H I ::: {5,10,15,20,25,30,35,40,45} ::: 5 5 > test-target-size.json
#
#
# Then:
#
# python rbtree.py rbtree.json

from collections import defaultdict, Counter
from pathlib import Path
import json
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import pickle # searialize data
import random
import seaborn as sns
import statistics
import sys

from pymoo.indicators.gd import GD
from pymoo.indicators.gd_plus import GDPlus
from pymoo.indicators.igd import IGD
from pymoo.indicators.igd_plus import IGDPlus
from pymoo.indicators.hv import HV
from pymoo.util.nds.non_dominated_sorting import NonDominatedSorting

import logging

# Configure the logger (Best Practice)
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

line_styles = ['-', '--', '-.', ':']
markers = ['o', 's', '^', 'd']  # Circle, Square, Triangle, Diamond

def styles(runTitle):
    if runTitle in ['A']:
        return { 'title': runTitle, 'line': 'solid',  'marker': 'o', 'color': 'blue' }
    if runTitle in ['B', 'C', 'D']:
        return { 'title': runTitle, 'line': 'dashed', 'marker': 's', 'color': 'green' }
    if runTitle in ['D', 'E', 'F']:
        return { 'title': runTitle, 'line': 'dashdot', 'marker': '^', 'color': 'orange' }
    if runTitle == ['G', 'H', 'I']:
        return { 'label': runTitle, 'line': 'dotted', 'marker': 'o', 'color': 'purple' }
    return { 'title': runTitle, 'line': ':',  'marker': 'o', 'color': 'red' }

full_title = {
    "A" : "Rejection Sampling",
    "B" : "MIN $F_a/\text{size}$",
    "C" : "MIN $F_a$",
    "D" : "MIN validityFitnessMinRelative",
    "E" : "MIN validityFitnessMinRelative",
    "F" : "MAX validityFitnessMaxRelative",
    "G" : "MAX target S validityFitnessMax",
    "H" : "MAX validityFitnessMaxRelative",
    "I" : "MAX target S validityFitnessMax",
    "J" : "MIN-MAX (relative props) x size",
    "K" : "MIN-MAX rbTreeFitnessMin x size",
    "L" : "MIN-MAX validityFitnessMin x size",
    "M" : "MAX-MAX (relative props) x size",
    "N" : "MAX-MAX validityFitnessMaxRelative x size",
    "O" : "MIN-MAX validityFitnessMinRelative x size"
}


moo_config = {
    'J': {
        'problem_type': ['MIN', 'MIN', 'MIN', 'MAX'],
        'reference_pf': [ [0, 0, 0, n] for n in range(0, 45)],
        'adjust': [1,1,1,-1],
        'ref_point': [50, 50, 50, 0]
    },
    'K': {
        'problem_type': ['MIN', 'MAX'],
        'reference_pf': [ [1, n] for n in range(0, 50)],
        'adjust': [1, -1],
        'x': 1,
        'y': -1,
        'x2': lambda x:  x,
        'y2': lambda x: -x,
        'ref_point': [30, 0],
    },
    'L': {
        'problem_type': ['MIN', 'MAX'],
        'reference_pf': [ [0, n] for n in range(1, 350)],
        'adjust': [1, -1],
        'x': 1,
        'y': -1,
        'x2': lambda x:  x,
        'y2': lambda x: -x,
        'ref_point': [30,0],
    },
    'M': {
        'problem_type': ['MAX', 'MAX', 'MAX', 'MAX'],
        'reference_pf': [ [0, 0, 0, n] for n in range(0, 45)],
        'adjust': [-1,-1,-1,-1],
        'ref_point': [0, 0, 0, 0]
    },
    'N': {
        'problem_type': ['MIN', 'MAX'],
        'reference_pf': [ [1, n] for n in range(1, 250)],
        'adjust': [-1, -1],
        'x': 1,
        'y': 1,
        'x2': lambda x: 1+x,
        'y2': lambda x: -x,
        'ref_point': [0,0],
    },
    'O': {
        'problem_type': ['MIN', 'MAX'],
        'reference_pf': [ [0, n] for n in range(1, 250)],
        'adjust': [1, -1],
        'x': 1,
        'y': -1,
        'x2': lambda x: x,
        'y2': lambda x: -x,
        'ref_point': [1,0],
    }
}

def metadata(title):
    splits = title.split("_")
    ts = splits[0]
    rep = splits[1].split('.')[0]
    title = "_".join(splits[1:-6]).split('.')[1]
    [g, p, m, x, s, a] = splits[-6:]
    return {
        'ts': int(ts),
        'rep': int(rep),
        'title': title,
        'g': int(g[:-1]),
        'p': int(p[:-1]),
        'm': int(m[:-1]),
        'x': int(x[:-1]),
        's': int(s[:-1]),
        'a': a
    }

def add_report(data, groups):
    meta = metadata(data["report"]["runTitle"])
    title = meta['title']

    time = data["report"]["runTime"]
    valid =  data["report"]["validIndividualsCount"]
    max_size = data["report"]["extra"]["maxSize"]
    unique_sizes = data["report"]["extra"]["uniqueSizes"]
    valid_unique_sizes = data["report"]["extra"]["uniqueValidSizes"]
    diversity = data["report"]["extra"]["diversity"]
    unique_fitness = data["report"]["extra"]["uniqueFitnessSummary"]

    groups['time'][title].append(time)
    groups['valid'][title].append(valid)
    groups['max_size'][title].append(max_size)
    groups['sizes_all'][title].update(unique_sizes)
    groups['valid_sizes_all'][title].update(valid_unique_sizes)
    groups['fitness'][title].append(unique_fitness)
    groups['diversity'][title].append(diversity)
    groups['hv'][title].append(unique_fitness)
    groups['gd'][title].append(unique_fitness)
    return

def plot_sizes_all(data,relative=False, key='sizes_all', pre=''):
    plt.figure(figsize=(10,5))

    for title, tData in data.items():
        style = styles(title)

        line_style = style['line']
        marker = style['marker']
        color = style['color']

        total = tData[key].total()

        sorted_sizes_all = {str(k): tData[key][str(k)] for k in sorted(int(x) for x in sorted(tData[key].keys()))}
        sizes = [int(x) for x in sorted_sizes_all.keys()]
        counts = list(sorted_sizes_all.values())
        if relative: counts = [x / total for x in counts]

        plt.plot(sizes, counts, label=title)

        # plt.title('Size Distribution by Case')
    plt.xlabel('Nodes in red-black tree')
    # plt.xscale('log')
    plt.xlim(left=1)

    plt.ylabel('Amount of valid red-black trees')
    if relative:
        plt.ylabel('Proportion valid red-black trees')
    plt.yscale('log')

    plt.legend()

    export_name = f'rbtree_py_{pre}_{key}.jpg' if not relative else f'rbtree_py_{pre}_{key}_relative.jpg'
    logging.info(f">>>> Exporting to: {export_name}")
    plt.savefig(export_name, format='jpg', dpi=600)

    plt.clf()
    plt.cla()
    plt.close()

def sort_by_intkey(d):
    return {str(k): d[str(k)] for k in sorted(int(x) for x in d.keys())}

def transpose(groups):
    tgroups = defaultdict(dict)

    for measure,kvTitle in groups.items():
        for title,v in kvTitle.items():
            tgroups[title][measure] = v
    return tgroups


def apply_stats(data):
    for title, tData in sorted(data.items()):
        logging.info(f"Stats for {title}")
        for k, measurements in tData.items():
            if k in ['time', 'valid', 'max_size', 'diversity']:
                stats = single_stats(measurements)
                stats['raw'] = measurements
                data[title][k] = stats
            if title in moo_config.keys():
                if k == 'hv':
                    hv_stats = hv(measurements, title)
                    stats = single_stats(hv_stats)
                    stats['raw'] = measurements
                    data[title][k] = stats
                if k == 'gd':
                    distance_stats = distance_pi(measurements, key=title, pi=GD)
                    stats = single_stats(distance_stats)
                    stats['raw'] = measurements
                    data[title][k] = stats

def single_stats(measurements): # list of values
    stats = {}
    stats['mean'] = statistics.mean(measurements)
    stats['median'] = statistics.median(measurements)
    stats['variance'] = statistics.variance(measurements)
    stats['stdev'] = statistics.stdev(measurements)
    stats['sum'] = sum(measurements)
    if len(measurements) > 0:
        stats['min'] = min(measurements)
        stats['max'] = max(measurements)
    return stats

def hv(multi, key=None):
    result = []
    adjust = np.array(moo_config[key]['adjust'])
    ref_point = adjust * np.array(moo_config[key]['ref_point'])
    print(key, ref_point)

    dbg_multi_fitness = len(multi)
    for idx, fitness in enumerate(multi):
        ind = HV(ref_point=ref_point)

        logging.info(f"HV calculation for {key} ({idx + 1}/{dbg_multi_fitness}) having {len(fitness)} feature points.")
        fs = adjust * np.array([f for f in fitness if None not in f])

        hv = ind(fs)
        result.append(hv)

    return result

def distance_pi(multi, key=None,  pi=None):
    result = []
    pf = np.array(moo_config[key]['reference_pf'])
    for fitness in multi:
        ind = pi(pf)
        fs = np.array([f for f in fitness if None not in f])
        gd = ind(fs)
        result.append(gd)
    return result

def tabular(data):
    width = 8
    print(" & ".join([ x.ljust(width) for x in ["ID", "Fitness".ljust(47), "Time (s)", "", "Valid", "", "", "Length", "", "", "diversity", ""] ]), "\\\\")
    print(" & ".join([ x.ljust(width) for x in ["", "".ljust(47), "mean", "stdev", "total", "mean", "stdev", "max", "mean", "stdev", "mean", "stdev"] ]), "\\\\ \\midrule")
    for title in sorted(data.keys()):
        fields = [
            title,
            full_title[title].ljust(47),
            f"{data[title]['time']['mean']:.2f}",
            f"±{data[title]['time']['stdev']:.2f}",
            f"{data[title]['valid']['sum']}",
            f"{data[title]['valid']['mean']:.2f}",
            f"±{data[title]['valid']['stdev']:.2f}",
            f"{data[title]['max_size']['max']}",
            f"{data[title]['max_size']['mean']:.2f}",
            f"±{data[title]['max_size']['stdev']:.2f}",
            f"{data[title]['diversity']['mean']:.2f}",
            f"±{data[title]['diversity']['stdev']:.2f}"
        ]

        fields = " & ".join([ x.ljust(width) for x in fields ])
        print(fields, '\\\\')

def tabular_pi(data, pi=None):
    if pi == None:
        logging.error("No performance indicator in tabular_pi")
        return

    print("=========", pi, "=========")
    width = 10
    new_data = {}
    print(" & ".join([x.ljust(width) for x in ['title', 'max', 'min', 'mean', 'stdev']]))
    for title in sorted(moo_config.keys()):
        fields = [
            title,
            f"{data[title][pi]['max']:.2f}",
            f"{data[title][pi]['min']:.2f}",
            f"{data[title][pi]['mean']:.2f}",
            f"±{data[title][pi]['stdev']:.2f}",
        ]
        print(" & ".join([f"{x}".ljust(width) for x in fields]))

def default_groups():
    return {
        'time': defaultdict(lambda: list()),
        'valid': defaultdict(lambda: list()),
        'max_size': defaultdict(lambda: list()),
        'sizes_all': defaultdict(lambda: Counter()),
        'valid_sizes_all': defaultdict(lambda: Counter()),
        'fitness': defaultdict(lambda: list()),
        'diversity': defaultdict(lambda: list()),
        'hv': defaultdict(lambda: list()),
        'gd': defaultdict(lambda: list()),
    }

def summary(source):
    grouped_diversity_data = defaultdict(lambda: defaultdict(list))

    groups = default_groups()

    for line in source:
        data = json.loads(line)
        add_report(data, groups)

    tGroups = transpose(groups)

    stats = apply_stats(tGroups)

    return tGroups

def all_pareto_fronts(data, x, y, chunk_size=6000):
    all = [ [x * f1, y * f2] for run in data for [f1,f2] in run ]
    random.shuffle(all)
    chunks = [np.array(all[i:i + chunk_size]) for i in range(0, len(all), chunk_size)]

    while len(chunks) > 1:
        print(f'Currently with {len(chunks)} chunks...', end='\r', flush=True)
        acc_cpf = list()
        for chunk in chunks:
            nds = NonDominatedSorting().do(chunk, only_non_dominated_front=True)
            cpf = chunk[nds]
            acc_cpf.append(cpf)
        acc_cpf = [ ndp for cpf in acc_cpf for ndp in cpf ]
        random.shuffle(acc_cpf)
        chunks = [ np.array(acc_cpf[i:i + chunk_size]) for i in range(0, len(acc_cpf), chunk_size) ]
    logging.info(f'Final chunk ({len(chunks)}) of {len(chunks[0])} elems')

    nds = NonDominatedSorting().do(chunks[0], only_non_dominated_front=True)
    cpf = chunks[0][nds]
    cpf = cpf[cpf[:, 0].argsort()]

    return cpf

def plot_moo_subgrid(data, sub_f=10000, x=1, y=1, pf=None, ax=None, x2=lambda a: a, y2=lambda b: b):
    fitness = data
    fitness_sub = [[a*x, b*y] for run in fitness for [a, b] in run]

    fitness_sub = np.array(fitness_sub)

    final_f_x = [ x2(value) for value in fitness_sub[:, 0]]
    final_f_y = [ y2(value) for value in fitness_sub[:, 1]]
    ax.scatter(final_f_x, final_f_y, label='Fitness values', color='#87CEEB', alpha=0.9, s=5)

    pf = [[x2(a*x), y2(b*y)] for [a, b] in pf]
    pf_x, pf_y = zip(*pf)
    ax.plot(pf_x, pf_y, color='red', label='Reference pareto front')

    # all_cpf = all_pareto_fronts(data['hv']['raw'], x, y)
    all_cpf = all_pareto_fronts([fitness_sub], 1, 1)
    all_cpf = all_cpf[all_cpf[:, 0].argsort()]
    all_cpf_x = [x2(value) for value in all_cpf[:, 0]]
    all_cpf_y = [y2(value) for value in all_cpf[:, 1]]
    ax.plot(all_cpf_x, all_cpf_y, color='blue', label='Computed pareto front', marker='o', markersize=6)
    ax.scatter(all_cpf_x, all_cpf_y, color='blue', s=5, alpha=0.3)
    # ax.legend()

# First file argument:  summary
# Second file argument: plots sizes
def main():
    source = None
    tGroups = None
    input_file = None
    print(len(sys.argv), sys.argv)
    if len(sys.argv) >= 2:
        input_file = sys.argv[1]
        print("="*30, input_file, "="*30)

        CACHE_FILE = input_file + ".pkl"
        if os.path.exists(CACHE_FILE):
            logging.info(f"Loading summary from cache file: {CACHE_FILE}")
            with open(CACHE_FILE, 'rb') as file:
                tGroups = pickle.load(file)
        else:
            logging.info(f"Cache file not found. Calculating summary.")
            source = open(input_file, 'r')
            tGroups = summary(source)
            source.close()
            logging.info(f"Saving summary to cache file: {CACHE_FILE}")
            with open(CACHE_FILE, 'wb') as file:
                pickle.dump(tGroups, file)
    else:
        tGroups = summary(sys.stdin)

    tabular(tGroups)
    tabular_pi(tGroups, pi='hv')
    tabular_pi(tGroups, pi='gd')

    # pre = Path(input_file).stem
    # plot_sizes_all(tGroups, pre=pre)
    # plot_sizes_all(tGroups, relative=True, pre=pre)
    # plot_sizes_all(tGroups, key='valid_sizes_all', pre=pre)
    # plot_sizes_all(tGroups, relative=True, key='valid_sizes_all', pre=pre)
    # source.close()

    # # Only MO
    # tGroupsMO = { k: tGroups[k] for k in ['J', 'K', 'L', 'M', 'N', 'O'] }
    # pre_title = Path(input_file).stem + '_MO'
    # plot_sizes_all(tGroupsMO, relative=True, pre=pre_title)
    # plot_sizes_all(tGroupsMO, relative=True, key='valid_sizes_all', pre=pre_title)

    # tGroupsSO = { k: tGroups[k] for k in [ 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'] }
    # pre_title = Path(input_file).stem + '_SO'
    # plot_sizes_all(tGroupsSO, relative=True, pre=pre_title)
    # plot_sizes_all(tGroupsSO, relative=True, key='valid_sizes_all', pre=pre_title)

    keys = list(moo_config.keys())
    keys.remove('J')
    keys.remove('M')
    fig, axs = plt.subplots(2, 2, figsize=(15, 15))
    for idx, key in enumerate(sorted(keys)):
        key_axs = axs[idx // 2, idx % 2]
        key_axs.set_title(key)
        plot_moo_subgrid(tGroups[key]['fitness'],
                         x=moo_config[key]['adjust'][0],
                         y=moo_config[key]['adjust'][1],
                         pf=moo_config[key]['reference_pf'],
                         ax=key_axs,
                         x2=moo_config[key]['x2'],
                         y2=moo_config[key]['y2'])
        [x, y] = list(np.array(moo_config[key]['adjust']) * np.array(moo_config[key]['ref_point']))
        x2=moo_config[key]['x2']
        y2=moo_config[key]['y2']
        key_axs.plot(x2(x), y2(y), color='orange', marker='D', linestyle='None', label='HV Reference Point')



    plt.legend()
    export_name = f'rbtree_py_MO_fitness.jpg'
    print(f">>>> Exporting to: {export_name}")
    plt.savefig(export_name, format='jpg', dpi=600)

    plt.clf()
    plt.cla()
    plt.close()

    return

if __name__ == '__main__':
    main()
