import json
import numpy as np
import matplotlib.pyplot as plt
from collections import defaultdict, Counter
import statistics
import os
import sys
import seaborn as sns
import pandas as pd
from pathlib import Path

line_styles = ['-', '--', '-.', ':']
markers = ['o', 's', '^', 'd']  # Circle, Square, Triangle, Diamond

def styles(runTitle):
    if runTitle in ['A']:
        return { 'title': runTitle, 'line': 'solid',  'marker': 'o', 'color': 'blue' }
    if runTitle in ['B', 'C', 'D']:
        return { 'title': runTitle, 'line': 'dashed', 'marker': 's', 'color': 'green' }
    if runTitle in ['D', 'E', 'F']:
        return { 'title': runTitle, 'line': 'dashdot', 'marker': '^', 'color': 'orange' }
    if runTitle == ['G', 'H', 'I']:
        return { 'label': runTitle, 'line': 'dotted', 'marker': 'o', 'color': 'purple' }
    return { 'title': runTitle, 'line': ':',  'marker': 'o', 'color': 'red' }

full_title = {
    "A" : "Rejection Sampling",
    "B" : "$\\text{\\lstinline{fitnessSorted}}$",
    "C" : "$\\text{\\lstinline{fitnessSorted'}}$",
    "D" : "$\\text{\\lstinline{sized fitnessSorted}}$",
    "E" : "$\\text{\\lstinline{sized fitnessSorted'}}$",
    "F" : "$\\text{\\lstinline{fitnessSorted''}}$",
    "G" : "$\\text{\\lstinline{target 10 fitnessSorted}}$",
    "H" : "$\\text{\\lstinline{target 10 fitnessSorted'}}$",
    "I" : "$\\text{\\lstinline{target 10 fitnessSorted''}}$",
    "MOO_B" : "$\\text{\\lstinline{fitnessSorted}}$",
    "MOO_C" : "$\\text{\\lstinline{fitnessSorted'}}$",
    "MOO_D" : "$\\text{\\lstinline{sized fitnessSorted}}$",
    "MOO_E" : "$\\text{\\lstinline{sized fitnessSorted'}}$",
    "MOO_F" : "$\\text{\\lstinline{fitnessSorted''}}$",
    "MOO_G" : "$\\text{\\lstinline{target 10 fitnessSorted}}$",
    "MOO_H" : "$\\text{\\lstinline{target 10 fitnessSorted'}}$",
    "MOO_I" : "$\\text{\\lstinline{target 10 fitnessSorted''}}$",
}

def metadata(title):
    splits = title.split("_")
    ts = splits[0]
    rep = splits[1].split('.')[0]
    title = "_".join(splits[1:-6]).split('.')[1]
    [g, p, m, x, s, a] = splits[-6:]
    return {
        'ts': int(ts),
        'rep': int(rep),
        'title': title,
        'g': int(g[:-1]),
        'p': int(p[:-1]),
        'm': int(m[:-1]),
        'x': int(x[:-1]),
        's': int(s[:-1]),
        'a': a
    }

def add_report(data, groups):
    meta = metadata(data["report"]["runTitle"])
    title = meta['title']

    time = data["report"]["runTime"]
    valid =  data["report"]["validIndividualsCount"]
    max_size = data["report"]["extra"]["maxSize"]
    unique_sizes = data["report"]["extra"]["uniqueSizes"]
    valid_unique_sizes = data["report"]["extra"]["uniqueValidSizes"]
    diversity = data["report"]["extra"]["diversity"]

    groups['time'][title].append(time)
    groups['valid'][title].append(valid)
    groups['max_size'][title].append(max_size)
    groups['sizes_all'][title].update(unique_sizes)
    groups['valid_sizes_all'][title].update(valid_unique_sizes)
    groups['diversity'][title].append(diversity)
    return

def plot_sizes_all(data,relative=False, key='sizes_all', pre=''):
    plt.figure(figsize=(10,5))

    for title, tData in data.items():
        style = styles(title)

        line_style = style['line']
        marker = style['marker']
        color = style['color']

        total = tData[key].total()

        sorted_sizes_all = {str(k): tData[key][str(k)] for k in sorted(int(x) for x in tData[key].keys())}
        sizes = [int(x) for x in sorted_sizes_all.keys()]
        counts = list(sorted_sizes_all.values())
        if relative: counts = [x / total for x in counts]

        plt.plot(sizes, counts, label=title)

    # plt.title('Size Distribution by Case')
    plt.xlabel('List length')

    plt.ylabel('Amount of sorted lists')
    if relative:
        plt.ylabel('Proportion sorted lists')
    plt.legend()

    export_name = f'list_py_{pre}_{key}.jpg' if not relative else f'list_py_{pre}_{key}_relative.jpg'
    print(f">>>> Exporting to: {export_name}")
    plt.savefig(export_name, format='jpg', dpi=600)

    plt.clf()
    plt.cla()
    plt.close()

def sort_by_intkey(d):
    return {str(k): d[str(k)] for k in sorted(int(x) for x in d.keys())}

def transpose(groups):
    tgroups = defaultdict(lambda: {})

    for measure,kvTitle in groups.items():
        for title,v in kvTitle.items():
            tgroups[title][measure] = v
    return tgroups


def apply_stats(data):
    for title, tData in data.items():
        for k, measurements in tData.items():
            if k in ['time', 'valid', 'max_size', 'diversity']:
                stats = single_stats(measurements)
                stats['raw'] = measurements
                data[title][k] = stats

def single_stats(measurements): # list of values
    stats = {}
    measurements = [ m for m in measurements if m != None ]
    stats['mean'] = statistics.mean(measurements)
    stats['median'] = statistics.median(measurements)
    stats['variance'] = statistics.variance(measurements)
    stats['stdev'] = statistics.stdev(measurements)
    stats['sum'] = sum(measurements)
    if len(measurements) > 0:
        stats['min'] = min(measurements)
        stats['max'] = max(measurements)
    return stats

def tabular(data):
    width = 8
    print(" & ".join([ x.ljust(width) for x in ["ID", "Fitness".ljust(47), "Time (s)", "", "Valid", "", "", "Length", "", "", "Diversity", ""] ]), "\\\\")
    print(" & ".join([ x.ljust(width) for x in ["", "".ljust(47), "mean", "stdev", "total", "mean", "stdev", "max", "mean", "stdev", "mean", "stdev"] ]), "\\\\ \\midrule")
    for title in sorted(data.keys()):
        fields = [
            title,
            full_title[title].ljust(47),
            f"{data[title]['time']['mean']:.2f}",
            f"±{data[title]['time']['stdev']:.2f}",
            f"{data[title]['valid']['sum']}",
            f"{data[title]['valid']['mean']:.2f}",
            f"±{data[title]['valid']['stdev']:.2f}",
            f"{data[title]['max_size']['max']}",
            f"{data[title]['max_size']['mean']:.2f}",
            f"±{data[title]['max_size']['stdev']:.2f}",
            f"{data[title]['diversity']['mean']:.2f}",
            f"±{data[title]['diversity']['stdev']:.2f}",
        ]

        fields = " & ".join([ x.ljust(width) for x in fields ])
        print(fields, '\\\\')

def summary(source):
    grouped_diversity_data = defaultdict(lambda: defaultdict(list))

    groups = {
        'time': defaultdict(lambda: list()),
        'valid': defaultdict(lambda: list()),
        'max_size': defaultdict(lambda: list()),
        'sizes_all': defaultdict(lambda: Counter()),
        'valid_sizes_all': defaultdict(lambda: Counter()),
        'diversity': defaultdict(lambda: list()),
    }

    for line in source:
        data = json.loads(line)
        add_report(data, groups)

    tGroups = transpose(groups)

    stats = apply_stats(tGroups)

    tabular(tGroups)
    return tGroups

def main():
    source = None
    print(len(sys.argv), sys.argv)
    if len(sys.argv) >= 2:
        input_file = sys.argv[1]
        print("="*30, input_file, "="*30)
        source = open(input_file, 'r')
        summary(source)
        source.close()
    else:
        summary(sys.stdin)

    if len(sys.argv) < 3: sys.exit(0)
    input_file = sys.argv[2]
    print("="*30, input_file, "="*30)
    source = open(input_file, 'r')
    tGroups = summary(source)
    pre = Path(input_file).stem
    plot_sizes_all(tGroups, pre=pre)
    plot_sizes_all(tGroups, relative=True, pre=pre)
    plot_sizes_all(tGroups, key='valid_sizes_all', pre=pre)
    plot_sizes_all(tGroups, relative=True, key='valid_sizes_all', pre=pre)
    source.close()

    if len(sys.argv) < 4: sys.exit(0)
    input_file = sys.argv[3]
    print("="*30, input_file, "="*30)
    source = open(input_file, 'r')
    tGroups = summary(source)
    pre = Path(input_file).stem
    plot_sizes_all(tGroups, pre=pre)
    plot_sizes_all(tGroups, relative=True, pre=pre)
    plot_sizes_all(tGroups, key='valid_sizes_all', pre=pre)
    plot_sizes_all(tGroups, relative=True, key='valid_sizes_all', pre=pre)
    source.close()

if __name__ == '__main__':
    main()


# First, run with:

# parallel -j 9 'stack run list -- --gp --case {} -s 20 -m 5 -x 95 -p 100 -g 1000 -r 10' ::: A B C D E F G H I > test.json
# parallel -j 15 'stack run list -- --gp --case {1} -s {2} -m 5 -x 95 -p 100 -g 1000 -r {3}' ::: G H I ::: {5,10,15,20,25,30,35,40,45} ::: 5 5 > test-target-size.json
