import json
import numpy as np
import matplotlib.pyplot as plt
from collections import defaultdict, Counter
import statistics
import os
import sys
import seaborn as sns
import pandas as pd
from pathlib import Path

line_styles = ['-', '--', '-.', ':']
markers = ['o', 's', '^', 'd']  # Circle, Square, Triangle, Diamond

def styles(runTitle):
    if runTitle in ['A']:
        return { 'title': runTitle, 'line': 'solid',  'marker': 'o', 'color': 'blue' }
    if runTitle in ['B', 'C', 'D']:
        return { 'title': runTitle, 'line': 'dashed', 'marker': 's', 'color': 'green' }
    if runTitle in ['D', 'E', 'F']:
        return { 'title': runTitle, 'line': 'dashdot', 'marker': '^', 'color': 'orange' }
    if runTitle == ['G', 'H', 'I']:
        return { 'label': runTitle, 'line': 'dotted', 'marker': 'o', 'color': 'purple' }
    return { 'title': runTitle, 'line': ':',  'marker': 'o', 'color': 'red' }

full_title = {
    "A" : "Rejection Sampling",
    "B" : "$\\text{\\lstinline{fitnessSorted}}$",
    "C" : "$\\text{\\lstinline{fitnessSorted'}}$",
    "D" : "$\\text{\\lstinline{sized fitnessSorted}}$",
    "E" : "$\\text{\\lstinline{sized fitnessSorted'}}$",
    "F" : "$\\text{\\lstinline{fitnessSorted''}}$",
    "G" : "$\\text{\\lstinline{target 10 fitnessSorted}}$",
    "H" : "$\\text{\\lstinline{target 10 fitnessSorted'}}$",
    "I" : "$\\text{\\lstinline{target 10 fitnessSorted''}}$",
    "MOO_B" : "$\\text{\\lstinline{fitnessSorted}}$",
    "MOO_C" : "$\\text{\\lstinline{fitnessSorted'}}$",
    "MOO_D" : "$\\text{\\lstinline{sized fitnessSorted}}$",
    "MOO_E" : "$\\text{\\lstinline{sized fitnessSorted'}}$",
    "MOO_F" : "$\\text{\\lstinline{fitnessSorted''}}$",
    "MOO_G" : "$\\text{\\lstinline{target 10 fitnessSorted}}$",
    "MOO_H" : "$\\text{\\lstinline{target 10 fitnessSorted'}}$",
    "MOO_I" : "$\\text{\\lstinline{target 10 fitnessSorted''}}$",
}

def metadata(title):
    splits = title.split("_")
    ts = splits[0]
    rep = splits[1].split('.')[0]
    title = "_".join(splits[1:-6]).split('.')[1]
    [g, p, m, x, s, a] = splits[-6:]
    return {
        'ts': int(ts),
        'rep': int(rep),
        'title': title,
        'g': int(g[:-1]),
        'p': int(p[:-1]),
        'm': int(m[:-1]),
        'x': int(x[:-1]),
        's': int(s[:-1]),
        'a': a
    }

def sort_by_intkey(d):
    return {str(k): d[str(k)] for k in sorted(int(x) for x in d.keys())}

def add_report(data, groups):
    meta = metadata(data["report"]["runTitle"])
    key = f"{meta['title']}_{meta['a']}"

    # Data
    time = data["report"]["runTime"]
    valid =  data["report"]["validIndividualsCount"]
    unique =  data["report"]["uniqueIndividualsCount"]
    max_size =  data["report"]["extra"]["maxSize"]

    # Add report
    groups[key][meta['m']]['time'].append(time)
    groups[key][meta['m']]['valid'].append(valid)
    groups[key][meta['m']]['unique'].append(valid)
    groups[key][meta['m']]['max_size'].append(valid)
    return

def apply_stats(data):
    for title, tData in data.items():
        for m, mData in tData.items():
            for k, measurements in tData[m].items():
                if k in ['time', 'valid', 'unique', 'max_size']:
                    stats = single_stats(measurements)
                    stats['raw'] = measurements
                    data[title][m][k] = stats

def single_stats(measurements): # list of values
    stats = {}
    stats['mean'] = statistics.mean(measurements)
    stats['median'] = statistics.median(measurements)
    stats['sum'] = sum(measurements)
    if len(measurements) > 2:
        stats['variance'] = statistics.variance(measurements)
        stats['stdev'] = statistics.stdev(measurements)
    if len(measurements) > 0:
        stats['min'] = min(measurements)
        stats['max'] = max(measurements)
    return stats

def tabular(data):
    width = 8
    new_data = {}
    for idx, title in enumerate(sorted(data.keys())):
        if idx == 0:
            # Only on the first, print this before
            print(" & ".join([" ".rjust(width)] + [f"{m}".rjust(width) for m in sorted(data[title].keys())]))

        new_values = [ data[title][m]['valid']['mean'] for m in sorted(data[title].keys()) ]
        fields = [title] + [ f"{x}" for x in new_values ]
        fields = " & ".join([ x.rjust(width) for x in fields ])
        new_data[title] = new_values
        print(fields, '\\\\')
    return new_data

def plot_mutation_data(data, format='jpg'):
    # Init
    plt.figure(figsize=(10,5))

    # Data
    x_ticks = [x / 100 for x in range(5,100,5)]
    for title,values in data.items():
        [label, algorithm] = title.split('_')
        if algorithm == 'gp':
            plt.plot(x_ticks, values, label=label)


    plt.xticks(x_ticks)
    plt.yscale('log')
    plt.xlabel('Mutation probability')
    plt.ylabel('Amount of unique sorted lists')
    plt.legend()

    # Export
    export_name = f'plot-list-mutation'
    print(f">>>> Exporting to: {export_name}.{format}")
    plt.savefig(f'{export_name}.{format}', format=format, dpi=600)

    plt.clf()
    plt.cla()
    plt.close()

def summary(source):
    # Our base aggregation is #title_Mprob => stats{ valid => [], time: [], ... }
    groups = defaultdict(lambda: defaultdict(lambda: {
        'time': list(),
        'unique': list(),
        'valid': list(),
        'max_size': list(),
    }))

    for line in source:
        data = json.loads(line)
        add_report(data, groups)

    apply_stats(groups)
    mutation_data = tabular(groups)
    plot_mutation_data(mutation_data)

def main():
    source = None
    print(len(sys.argv), sys.argv)
    if len(sys.argv) >= 2:
        input_file = sys.argv[1]
        print("="*30, input_file, "="*30)
        source = open(input_file, 'r')
        summary(source)
        source.close()
    else:
        summary(sys.stdin)

if __name__ == '__main__':
    main()
