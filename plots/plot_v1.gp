set print "-" # Print to STDOUT

# if (!exists("directory")) { directory=sprintf("bench/%s", system("ls -t bench/ | head -n 1")) }
# if (!exists("filename")) { filename="max_fitness.csv" }
# if (!exists("output_directory")) { output_directory="plots_output" }
# if (!exists("N_COLS")) { N_COLS=3; }

directory=ARG1
basename=ARG2

all_input_filename=sprintf("%s/%s.all.csv", directory, basename)
mean_input_filename=sprintf("%s/%s.mean.csv", directory, basename)
bestmean_input_filename=sprintf("%s/%s.bestmean.csv", directory, basename)
# bestf1_input_filename=sprintf("%s/%s.bestf1.csv", directory, basename)
# bestf2_input_filename=sprintf("%s/%s.bestf2.csv", directory, basename)
# bestf3_input_filename=sprintf("%s/%s.bestf3.csv", directory, basename)
unique_input_filename=sprintf("%s/%s.unique.csv", directory, basename)

output_filename=sprintf("%s/%s.png", directory, basename)

print sprintf("working directory:   %s", directory)
print sprintf("all_input_filename:  %s", all_input_filename)
print sprintf("mean_input_filename: %s", mean_input_filename)
print sprintf("output_filename:     %s", output_filename)

#######################
# auxiliary functions #
#######################

max(x, y) = (x > y ? x : y)
min(x, y) = (x > y ? y : x)

#########
# Style #
#########
set style line 1 lc rgb '#000000' lw 2   # Black, thicker line
set style line 2 lc rgb '#E69F00' lw 2   # Yellow, thicker line
set style line 3 lc rgb '#56B4E9' lw 2   # Sky blue, thicker line
set style line 4 lc rgb '#009E73' lw 2   # Green, thicker line
set style line 5 lc rgb '#800000' lw 2   # Darker red, thicker line

set style line 1 pt 7 lc rgb '#000000' ps 1.5   # Black, solid circle with size 1.5
set style line 2 pt 5 lc rgb '#E69F00' ps 1.5   # Yellow, solid triangle with size 1.5
set style line 3 pt 13 lc rgb '#56B4E9' ps 1.5  # Sky blue, solid square with size 1.5

########
# Plot #
########

# scale 1063.0/420.0.5
# set terminal pngcairo size 400*scale,400*scale fontscale scale linewidth scale pointscale scale

# set terminal svg size 800,800;
# set size ratio 1;
set terminal png size 1300,1000
# set terminal pdf size 33cm,20cm
# set terminal pdf size 6cm,6cm
set output output_filename;

set datafile separator ","
stats all_input_filename skip 1
max_col = STATS_columns
do for [COL=1:max_col] { stats all_input_filename using COL name columnheader nooutput}

# layout:         rows,columns
set multiplot layout 3,3

##################
# Global fitness #
##################

set title "" tc rgb 'black';
set yrange [0:]
set ylabel 'Fitness'
set xrange [0:generation_max]
set xlabel 'Generation'
set datafile separator ","
plot all_input_filename skip 1 using 1:2 notitle with dots linestyle 3,\
     mean_input_filename skip 1 using 1:2 notitle with lines linestyle 2,\
     bestmean_input_filename skip 1 using 1:2 notitle with lines linestyle 1;

set title "" tc rgb 'black';
set yrange [0:]
set ylabel 'Unique valid terms'
set y2range [0:]
set y2label 'Accumulated unique valid terms'
set y2tics nomirror autofreq
set xrange [0:generation_max]
set xlabel 'Generation'
set datafile separator ","
set style data histogram
set style histogram cluster gap 1
set style fill solid border -1
set boxwidth 0.9
plot unique_input_filename skip 1 using 1 bins=25 notitle with boxes linestyle 3,\
     unique_input_filename skip 1 using 1:(1) axes x1y2 notitle smooth cumulative linestyle 2,\
     unique_input_filename skip 1 using 1:(.9) notitle with impulse lc "black";
unset y2label;
unset y2tics;

set title "" tc rgb 'black';
unset yrange;
unset xrange;
set yrange [0:]
set ylabel 'Terms'
set xrange [0:]
set xlabel 'Nodes'
set datafile separator ","
set style data histogram
set style histogram cluster gap 1
set style fill solid border -1
set boxwidth 0.9
plot unique_input_filename skip 1 using 8 smooth frequency notitle with boxes linestyle 3;
     # unique_input_filename skip 1 using 1:(1) axes x1y2 notitle smooth cumulative linestyle 2,\
     # unique_input_filename skip 1 using 1:(.9) notitle with impulse lc "black";
# unset y2label;
# unset y2tics;


# set multiplot next; # 4
# set multiplot next; # 5

#########################################################
# Fitness - Standard deviation of black depth of leaves #
#########################################################

set title "" tc rgb 'black';
set yrange [0:]
set ylabel 'Standard deviation of black-depth of leaves'
set xrange [0:]
set xlabel 'Generation'
set datafile separator ","
plot all_input_filename skip 1 using 1:2 notitle with dots linestyle 3,\
     mean_input_filename skip 1 using 1:2 notitle with lines linestyle 2;
     # bestf1_input_filename skip 1 using 1:3 notitle with lines linestyle 1;

set title "Depth (rb) of a leaf" tc rgb 'black';
set yrange [0:]
set ylabel 'Depth of a leaf'
set xrange [0:]
set xlabel 'Generation'
set datafile separator ","
plot all_input_filename skip 1 using 1:4 notitle with dots linestyle 3,\
     mean_input_filename skip 1 using 1:4 notitle with lines linestyle 3,\
     all_input_filename skip 1 using 1:5 notitle with dots linestyle 2,\
     mean_input_filename skip 1 using 1:5 notitle with lines linestyle 2;

# set title "Maximum depth (rb) of a leaf" tc rgb 'black';
# set yrange [0:]
# set ylabel 'Maximum depth of a leaf'
# set xrange [0:]
# set xlabel 'Generation'
# set datafile separator ","
# plot all_input_filename skip 1 using 1:5 notitle with dots linestyle 3,\
#      mean_input_filename skip 1 using 1:5 notitle with lines linestyle 2;

set title "Black-Depth of a leaf" tc rgb 'black';
set yrange [0:]
set ylabel 'Black-Depth of a leaf'
set xrange [0:]
set xlabel 'Generation'
set datafile separator ","
plot all_input_filename skip 1 using 1:6 notitle with dots linestyle 3,\
     mean_input_filename skip 1 using 1:6 notitle with lines linestyle 3,\
     all_input_filename skip 1 using 1:7 notitle with dots linestyle 2,\
     mean_input_filename skip 1 using 1:7 notitle with lines linestyle 2;


# set title "Maximum Black-Depth of a leaf" tc rgb 'black';
# set yrange [0:]
# set ylabel 'Maximum Depth'
# set xrange [0:]
# set xlabel 'Generation'
# set datafile separator ","
# plot all_input_filename skip 1 using 1:7 notitle with dots linestyle 3,\
#      mean_input_filename skip 1 using 1:7 notitle with lines linestyle 2;

# set multiplot next; # 4
# set multiplot next; # 5

##############################
# Fitness - No red-red nodes #
##############################

set title "Number of red-red nodes" tc rgb 'black';
# set yrange []
set ylabel 'Number of red-red nodes'
# set xrange [0:]
set xlabel 'Generation'
set datafile separator ","
plot all_input_filename skip 1 using 1:11 notitle with dots linestyle 3,\
     mean_input_filename skip 1 using 1:11 notitle with lines linestyle 2;
     # bestf2_input_filename skip 1 using 1:11 notitle with lines linestyle 1;

set title "Total nodes in a tree" tc rgb 'black';
set yrange [*:totalNodes_max]
set ylabel 'Number of nodes'
# set xrange [0:]
set xlabel 'Generation'
set datafile separator ","
plot all_input_filename skip 1 using 1:8 notitle with dots linestyle 3,\
     mean_input_filename skip 1 using 1:8 notitle with lines linestyle 2,\
     all_input_filename skip 1 using 1:9 notitle with dots linestyle 1,\
     mean_input_filename skip 1 using 1:9 notitle with lines linestyle 1,\
     all_input_filename skip 1 using 1:10 notitle with dots linestyle 5,\
     mean_input_filename skip 1 using 1:10 notitle with lines linestyle 5;

# set title "Number of black nodes" tc rgb 'black';
# set yrange [0:totalNodes_max]
# set ylabel 'Number of black nodes'
# set xrange [0:]
# set xlabel 'Generation'
# set datafile separator ","
# plot all_input_filename skip 1 using 1:9 notitle with dots linestyle 3,\
#      mean_input_filename skip 1 using 1:9 notitle with lines linestyle 2;

# set title "Number of red nodes" tc rgb 'black';
# set yrange [0:totalNodes_max]
# set ylabel 'Number of red nodes'
# set xrange [0:]
# set xlabel 'Generation'
# set datafile separator ","
# plot all_input_filename skip 1 using 1:10 notitle with dots linestyle 3,\
#      mean_input_filename skip 1 using 1:10 notitle with lines linestyle 2;

set multiplot next; # 3
# set multiplot next; # 4
# set multiplot next; # 5

###########################
# Fitness - Sorted values #
###########################

unset multiplot;
