# Generation of Algebraic Data Types Values Using Evolutionary Algorithms

This repository contains the library and experiments to use evolutionary
algorithms in the generation of random values of Algebraic Data Types. The
contents of this repository are organized as follows:

- `G3.EA`: Evolutionary Algorithm (EA) runtime.
- `G3.TH`: Template Haskell utilities for fitness derivation.
- `G3.Experiment`: Main utilities for running experiments.
- `G3.Example`: Initial examples, using this library. In particular: sorted lists
  and red-black trees.

## How to run the experiments

```sh
stack run
```

There are options available to run the program, for example, to adjust the
evolutionary algorithm parameters, measuring diversity and logging:

To list all avaible options, run `stack run -- --help`.

Optionally, you can install `g3` using `stack install`. Then, run with
`g3 --help`.

### Requirements

Please, make sure you have the following packages installed:
1. `ghc`
2. `stack`

## Examples

If you want to create your own example generator, we recommend following the
existent examples as a reference ([sorted lists](#sorted-lists) and [red-black
trees](#red-black-trees)). You may also look at the [generaral usage
guidelines](#guidelines-on-custom-examples) to implement a custom experiment.

### Sorted lists

Documentation peding. Meanwhile, see module `G3.Example.List`.

### Red-black trees

Automatic data generation is a key component of automated software testing.
Random generation of test input data can uncover some bugs in software, but
its effectiveness decreases when those inputs must satisfy complex properties
in order to be meaningful.  In this work we study an evolutionary approach to
generate values that can be encoded as algebraic data types plus additional
properties.  More specifically, we have conducted an experiment on the
automated generation of red-black trees using evolutionary algorithms.
Although relatively simple, this example will allow us to introduce the main
principles of evolutionary algorithms and how these principles can be applied
to obtain valid, nontrivial samples of a given data structure. While the
preliminary results show some potential, further experimentation is needed.

<!-- ![Header-tree](./doc/src/header-tree.svg) -->

#### Crossover

A crossover operation is an exchange **between two trees**, *A* and *B*, where a
subtree of A replaces a subtree of *B* and the subtree from *B* replaces the one
from *A*, resulting in two new trees.

![Crossover](./doc/src/crossover.svg)


#### Mutation

- **Unitary Insertion**: replaces a leaf with a unitary tree.
- **Trimming**: replaces a unitary sub-tree with a leaf.
- **Mutate Color**: changes the color of some node.
- **Mutate Value**: changes the value of some node.

![Mutation](./doc/src/mutation.svg)

#### Fitness

The fitness is used to **select** the **best individuals** with respect to a
goal. Our fitness functions try to approximate the notion of how "close" a tree
is to being a valid red-black tree.

![Fitness](./doc/src/fitness.svg)

Red-black trees are **black-balanced**, that is, all leaves have the same
black-depth. A measure representing this property is the **standard deviation**
of the black-depth of the leaves.

The previous tree annotates the black-depth for each leaf, having `0.53` of
standard deviation.

![Fitness Bar](./doc/src/fitness-bar.svg)

We run an experiment with the **goal** of **generating black-balanced trees**,
regardless of the other validity requirements. In the next plot, each dot is a
generated tree, representing the value of the fitness function. The evolutionary
algorithms runs for 100 generations using the set of mutation operations.

![Plot](./doc/src/plot.svg)

### Guidelines on custom examples



## Acknowledgements

This work has been partially supported by PROCODE Project
(PID2019-108528RB-C21/MCIN/AEI/10.13039/501100011033). This work has been
partially supported by PRODIGY Project (TED2021-132464B-I00) funded by
MCIN/AEI/10.13039/501100011033/ and the European Union NextGenerationEU/ PRTR.
Funded by the European Union. Views and opinions expressed are however those of
the author(s) only and do not necessarily reflect those of the European
Union. The European Union can not be held responsible for them (GA
No. 101061343).

<img alt="EU NextGeneration Logo" src="./doc/img/eu-ng-1.jpg" width="10%">
<img alt="PRTR Logo" src="./doc/img/logo-prtr.jpg" width="10%">
