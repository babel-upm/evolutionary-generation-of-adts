module Operations
  (
    singleCrossing,

    insert,
    remove,
    prune,
    colour,
    value,

    swapValue,

    regenerate
  ) where

import Base (RBTree (..), Color(..))
import qualified RBTreeUtils as Utils

import Test.QuickCheck (Gen, Arbitrary(..), elements)

singleCrossing :: RBTree a -> RBTree a -> Gen [RBTree a]
singleCrossing a b = do
  cutA <- elements $ Utils.npi a
  cutB <- elements $ Utils.npi b
  let subA = Utils.atNpi cutA a
      subB = Utils.atNpi cutB b
  return [ Utils.replaceTreeAtNpi cutA subB a,
           Utils.replaceTreeAtNpi cutB subA b
         ]

-- |
--
-- == Mutation
--

-- |
--
-- Prunning makes a RBTree have the same number of nodes from root to end.
--
-- It takes the minimum depth of a branch and removes all node with greater depth.
-- This operation is not complete, as we could not express all sub-trees of the given tree.
prune :: RBTree a -> RBTree a
prune t = prune' (Utils.minDepthB t) t
  where
    prune' :: Int -> RBTree a -> RBTree a
    prune' n (T c l v r)
      | n <= 0 = E
      | n > 0 =
          let n' = n - 1
           in T c (prune' n' l) v (prune' n' r)
    prune' _ _ = E

remove :: RBTree a -> Gen (RBTree a)
remove E = return E
remove t = do
  i <- elements (Utils.npiLeafs t)
  return (Utils.removeNpi i t)

insert :: Arbitrary a => RBTree a -> Gen (RBTree a)
insert t = do
  leafs <- elements (Utils.npiLeafs t) -- Pick one a leaf of the tree
  i <- arbitrary -- Choose the NPI insertion, left of right
  color <- elements [R, B] -- Choose a color for the node
  val <- arbitrary -- Generate a value for the node
  return $ Utils.replaceNodeAtNpi (leafs ++ [i]) color val t

colour :: RBTree a -> Gen (RBTree a)
colour t = do
  i <- elements (Utils.npi t)
  return $ case Utils.atNpi i t of
    (T R _ v _) -> Utils.replaceNodeAtNpi i B v t
    (T B _ v _) -> Utils.replaceNodeAtNpi i R v t
    _ -> t

value :: Arbitrary a => RBTree a -> Gen (RBTree a)
value t = do
  i <- elements $ Utils.npi t
  v' <- arbitrary
  return $ case Utils.atNpi i t of
       (T c l _ r) -> Utils.replaceTreeAtNpi i (T c l v' r) t
       _ -> t

swapValue :: RBTree a -> Gen (RBTree a)
swapValue tree = do
  i <- elements $ Utils.npi tree
  j <- elements $ Utils.npi tree
  let ni = Utils.atNpi i tree
      nj = Utils.atNpi i tree
  case (ni, nj ) of
    (T ci _ vi _, T cj _ vj _) -> return $
      Utils.replaceNodeAtNpi i ci vj (Utils.replaceNodeAtNpi j cj vi tree)
    (_, _) -> return tree

-- swapValue' :: RBTree a -> Gen (RBTree a)
-- swapValue' tree = do
--   i <- elements $ Utils.npi tree
--   let ni = Utils.atNpi i tree
--   case ni of
--     T ci (T lci ll lvi lr) vi (T rci rl rvi rr) -> return $
--       Utils.replaceNodeAtNpi i ci vj (Utils.replaceNodeAtNpi j cj vi tree)
--     _ -> return tree

regenerate :: Gen (RBTree a) -> RBTree a -> Gen (RBTree a)
regenerate genTree t = do
  i <- elements (Utils.npi t)
  subT <- genTree
  return $ Utils.replaceTreeAtNpi i subT t
