{-# LANGUAGE DeriveGeneric #-}
module Main (main) where

import G3.EA
import G3.EA.Runtime (gaLoop, gpLoop, customLoop, nsga2Step)

import G3.Experiment (Experiment(..), runExperiment)
import G3.Experiment.Options (RunOptions(..), RuntimeOpt(..), parseMultipleOptions, runOptionsList)
import qualified G3.Experiment as BaseReport (BaseReport(..))

import Base (RBTree)
import Fitness
import GenRBTree (boundedTree)
import Operations
import Properties (prop_RBValid)
import qualified RBTreeUtils as RBTree
import qualified Base as RBTree (RBTree(..), toAscList, toAscListC)

import Control.Monad (forM_)
import Data.Aeson
import Data.List (isInfixOf)
import Data.Map.Strict (Map)
import Data.Ord
import GHC.Generics
import Test.QuickCheck (Arbitrary, suchThat, arbitrary, chooseInt)

import qualified Data.List as List
import qualified Data.Map.Strict as Map
import Data.Set (Set)
import qualified Data.Set as Set
import qualified Data.Vector as V
import qualified Statistics.Sample as Statistics
import qualified Utils

import Control.DeepSeq (NFData)

newtype Int' = Int' { getInt' :: Int } deriving (Show, Eq, Ord, Generic)

instance ToJSON Int'
instance FromJSON Int'
instance NFData Int'

instance Arbitrary Int' where
  arbitrary = do
    let r = 10000
    n <- chooseInt (-r, r)
    return (Int' n)

data ExtraStepReport' a = ExtraStepReport
  { popSummary :: [([Double], Int)] -- Represents the fitness values and the size, ordered by fitness
  , stepSizes :: Map Int Int
  , uniqueStepSizes :: Map Int Int
  , validStepSizes :: Map Int Int
  , best :: ([Double], a)
  , meanFitness :: Double
  , meanSize :: Double
  , stepDiversity :: Double
  } deriving (Show, Eq, Ord, Generic)

instance ToJSON a => ToJSON (ExtraStepReport' a)
instance FromJSON a => FromJSON (ExtraStepReport' a)

data ExtraFinalReport' a = ExtraFinalReport
  { maxSize :: Int
  , uniqueSizes :: Map Int Int
  , uniqueValidSizes :: Map Int Int
  , maxTree :: Individual a
  , diversity :: Double
  , diversityBySize :: Map Int Double
  , totalUniqueValid :: Int
  , uniqueValidFitnessSummary :: [([Double], Int)]
  , uniqueFitnessSummary :: [[Double]]
  } deriving (Show, Generic)

instance ToJSON a => ToJSON (ExtraFinalReport' a)
instance FromJSON a => FromJSON (ExtraFinalReport' a)

type Experiment' = Experiment (RBTree Int') (ExtraStepReport' (RBTree Int)) (ExtraFinalReport' (RBTree Int'))

mySort :: Ord a => a -> a -> Bool
mySort = (<)

main :: IO ()
main = do
  multiOpts <- parseMultipleOptions

  -- Multipe options can be received. They are combined into a list of single
  -- options For example, mutliple generation options (-g 100 -g 1000) will
  -- execute two experiments, the first one with 100 generations and the second
  -- one with 1000
  forM_ (runOptionsList multiOpts) $ \opts -> do
    timePrefix <- Utils.dateTimePrefix
    let r = repeatN opts

    -- If the experiment is said to be repeated N time, we use that index to identify the iteration.
    Utils.replicateMWithIndex_ r $ \repeatIndex -> do
      let repeatPrefix = if r > 0 then show repeatIndex else ""
      let prefix = filePrefix opts <> (if  "" == filePrefix opts then "" else "_") <> timePrefix <> "_" <> repeatPrefix
      let suffix = "_" <>
            show (optGenerations opts) <> "g_" <>
            show (optPopulation opts) <> "p_" <>
            show (optMutation opts) <> "m_" <>
            show (optCrossoverP opts) <> "x_" <>
            show (optIndividualSize opts) <> "s_" <>
            show (runtimeStep opts)

      mapM_ runExperiment (
        filterExperiments (cases opts) (exclude opts).
        fmap (\ex -> ex { title = prefix <> "." <> title ex <> suffix}) $
        experimentList opts)

filterExperiments :: [String] -> [String] -> [Experiment'] -> [Experiment']
filterExperiments [] [] ecs = ecs
filterExperiments [] exclusions ecs =
  filter (\ec -> not $ any (\p -> p `isInfixOf` title ec) exclusions) ecs
filterExperiments patterns exclusions ecs =
  let included = filter (\ec -> any (\p -> p `isInfixOf` title ec) patterns) ecs
  in filterExperiments [] exclusions included

experimentList :: RunOptions -> [Experiment']
experimentList options =
  let
    experiment = baseExperiment {
        searchPopulation = optPopulation options,
        searchMutation = optMutation options,
        searchCrossover = optCrossoverP options,
        searchGenerations = optGenerations options,
        targetSize = optIndividualSize options,
        searchStep = case runtimeStep options of
            GALoop -> gaLoop
            GPLoop -> gpLoop
            NSGA2 -> nsga2Step
            CustomLoop -> customLoop,
        checkDiversity = optCheckDiversity options,
        showStepReport = optShowStepReport options,
        showFinalReport = optShowFinalReport options
      }
  in
    [
      experiment { title = "A",
                   searchFitness = maximize "zero" . const 0,
                   searchOperators = [ MutationOp (const (searchGenerator baseExperiment)) ],
                   searchSelection = return,
                   searchMutation = 100
                 },
      -- Single Objetive
      -- Minimization
      experiment {
        -- "so_rbtree_min_props"
        title = "B",
        searchFitness = minimize "rbTreeFitnessMin mySort" . rbTreeFitnessMin mySort,
        searchStep = gpLoop
        },
      experiment {
        -- "so_rbtree_min_validity"
        title = "C",
        searchFitness = minimize "validityFitnessMin" . validityFitnessMin,
        searchStep = gpLoop
        },
      experiment {
        -- "so_rbtree_min_relative_validity"
        title = "D",
        searchFitness = minimize "validityFitnessMinRelative" . validityFitnessMinRelative,
        searchStep = gpLoop
        },
      -- NOTE: risk of no convergence
      -- experiment {
      --   -- "so_rbtree_min_relative_props"
      --   title = "E",
      --   searchFitness = minimize "validity" . validityFitnessMinRelative,
      --   searchStep = gpLoop
      --   },
      -- Maximization
      experiment {
        -- "so_rbtree_max_relative_validity"
        title = "F",
        searchFitness = maximize "validity" . validityFitnessMaxRelative,
        searchStep = gpLoop
        },
      experiment {
        -- "so_rbtree_max_penalty_validity"
        title = "G",
        searchFitness = maximize "validity" . penalty (targetSize experiment) RBTree.countNodes validityFitnessMax,
        searchStep = gpLoop
        },
      experiment {
        -- "so_rbtree_max_props"
        title = "H",
        searchFitness = maximize "props" . rbTreeFitnessMax mySort,
        searchStep = gpLoop
        },
      experiment {
        -- "so_rbtree_max_penalty_props"
        title = "I",
        searchFitness = maximize "props" . penalty (targetSize experiment) RBTree.countNodes (rbTreeFitnessMax mySort),
        searchStep = gpLoop
        },
      -- Multi-objetive

      -- Min-max
      experiment {
        -- "moo_rbtree_min_max_props"
        title = "J",
        searchFitness = minimize "sorted" . sortedFitnessMinRelative mySort <>
                        minimize "redred" . relativeRedRedMin <>
                        minimize "depth"  . relativeDepthMin <>
                        maximize "size" . countNodesFitness,
        searchStep = nsga2Step
        },
      experiment {
        -- "moo_rbtree_min_max_single_props"
        title = "K",
        searchFitness = minimize "rbtree" . rbTreeFitnessMin mySort <>
                        maximize "size" . countNodesFitness,
        searchStep = nsga2Step
        },
      experiment {
        -- "moo_rbtree_minmax_validity"
        title = "L",
        searchFitness = minimize "validity" . validityFitnessMin <>
                        maximize "size" . countNodesFitness,
        searchStep = nsga2Step
        },
      -- Max-max
      experiment {
        -- "moo_rbtree_props"
        title = "M",
        searchFitness = maximize "sorted" . sortedPairsFitnessMax mySort . RBTree.toAscList <>
                        maximize "redred" . relativeRedRedMax <>
                        maximize "depth"  . relativeDepthMax <>
                        maximize "size" . countNodesFitness,
        searchStep = nsga2Step
        },
      experiment {
        -- "moo_rbtree_validity"
        title = "N",
        searchFitness = maximize "validity" . validityFitnessMaxRelative <>
                        maximize "length" . countNodesFitness,
        searchStep = nsga2Step
        },
      experiment {
        -- "moo_rbtree_min_validity"
        title = "O",
        searchFitness = minimize "validity" . validityFitnessMinRelative <>
                        maximize "length" . countNodesFitness,
        searchStep = nsga2Step
        }
      -- experiment {
      --   title = "g3_syntax_sized",
      --   searchFitness = minimize "rbTreeFitness" . rbTreeFitness <>
      --                   minimize "length" . rbTreeFitnessSizedWith RBTree.countNodes,
      --   searchSelection = return . selectionByEliteOn fitnessMean 20
      --   -- searchSelection = selectionByParetoElite 10
      --   },
      -- experiment {
      --   title = "g3_moo_properties_sized",
      --   searchFitness = minimize "sorted" . sortedFitness <>
      --                   minimize "balanced" . balanceFitnessStdDev <>
      --                   minimize "noRedRed" . redRedFitness <>
      --                   minimize "size" . rbTreeFitnessSizedWith RBTree.countNodes,
      --   searchSelection = return . selectionByEliteOn fitnessMean 20
      --   -- searchSelection = selectionByParetoElite 10
      --   },
      -- experiment {
      --   title = "g3_new",
      --   searchFitness = maximize "rbtree" . fitness' (<=) 42
      --   -- searchSelection = selectionByParetoElite 10
      --   },
    ]

-- Contains default values for options not configurable by CLI options
baseExperiment :: Experiment'
baseExperiment = E
  { title = ""
  -- Evolutionary
  , searchOperators = (MutationOp <$> [insert, remove, colour, value, swapValue]) ++ (CrossoverOp <$> [singleCrossing])
  , searchSelection = return . selectionByEliteOn (Down . fitnessMean) 20
  , searchGenerator = boundedTree `suchThat` (/= RBTree.E) -- Generate non-empty trees
  -- Fitness
  , searchFitness = error "undefined experiment fitness"
  -- Option params
  , searchGenerations = error "undefined experiment generators"
  , searchPopulation = error "undefined experiment population"
  , searchMutation = error "undefined experiment mutation ratio"
  , searchCrossover = error "undefined experiment crossover ratio"
  , searchStep = error "undefined experiment runtime step"
  , targetSize = error "undefined target size"
  -- Reports
  , checkDiversity = False
  , showStepReport = False
  , stepReportExtra = \step -> -- (\i -> (RBTree.countNodes (snd i), fitnessValues (fst i), getInt' <$> snd i)) . Utils.maximumOn (sum . fitnessValues . fst) . pop -- const [] -- fmap (fitnessValues . fst) . pop
      let
        stepUnique :: Set (Individual (RBTree Int'))
        stepUnique = Set.fromList (pop step)

        stepUValid :: Set (Individual (RBTree Int'))
        stepUValid = Set.filter (prop_RBValid . snd) stepUnique

        countBySize :: Foldable f => f (Individual (RBTree Int')) -> Map Int Int
        countBySize = Utils.countBy (\(_,x) -> RBTree.countNodes x)

        -- TODO: Make it dependent of the optimization type of problem
        bestI = head $ List.sortOn (Down . sum . fitnessValues . fst) $ pop step
        best' = (fitnessValues (fst bestI), getInt' <$> snd bestI)
        meanFitness' = Statistics.mean $ V.fromList $ (sum . fitnessValues . fst) <$> pop step
        meanSize' = Statistics.mean $ V.fromList $ (fromIntegral . RBTree.countNodes . snd) <$> pop step
        -- stepDiversity' = Utils.calcDiversity (snd <$> (pop step))
        stepDiversity' = 0
       in ExtraStepReport
          { popSummary = (\(fi, x) -> (fitnessValues fi, RBTree.countNodes x)) <$> pop step -- [([Double], Int)] -- Represents the fitness values and the size, ordered by fitness
          , stepSizes = countBySize (pop step)
          , uniqueStepSizes = countBySize stepUnique
          , validStepSizes = countBySize stepUValid
          , best = best'
          , meanFitness = meanFitness'
          , meanSize = meanSize'
          , stepDiversity = stepDiversity'
          }
  , showFinalReport = True
  , validReport = prop_RBValid . snd
  , selectionReport = \stepPopulation ->
      let valid' = filter (prop_RBValid . snd) stepPopulation
      in if valid' == [] then [] else [Utils.maximumOn (RBTree.countNodes . snd) valid']
  , extraReportFinal = \_steps baseReport ->
    let
        unique = BaseReport._uniqueIndividuals baseReport
        uniqueValid' = BaseReport._validIndividuals baseReport
        uniqueValidTrees' = Set.map snd (BaseReport._validIndividuals baseReport)
        uniqueSizes' = Utils.countBy (RBTree.countNodes . snd) unique
        uniqueValidSizes' = Utils.countBy (RBTree.countNodes . snd) uniqueValid'
        maxSize' = if null (Map.keys uniqueValidSizes') then -1 else maximum (Map.keys uniqueValidSizes')
        selected = Set.toList $ BaseReport._selectedIndividuals baseReport
        maxTree' = if null selected then (SingleObjective (Maximize "not selected" 0), RBTree.E) else Utils.maximumOn (RBTree.countNodes . snd) selected
        -- totalUnique' = Set.size unique
        totalUniqueValid' = Set.size uniqueValid'
        -- uValid' = (Set.map snd (uniqueValid'))
        diversity' = Utils.calcDiversity $ fmap RBTree.toAscListC (Set.toList uniqueValidTrees')
        -- diversity' = 0
        diversityBySize' = Utils.calcDiversityBySize (Set.map RBTree.toAscListC uniqueValidTrees')
        -- diversityBySize' = Map.empty
        uniqueValidFitnessSummary' = (\(fi, x) -> (fitnessValues fi, RBTree.countNodes x)) <$> Set.toList uniqueValid' -- [([Double], Int)] -- Represents the fitness values and the size, ordered by fitness
        uniqueFitnessSummary' = fitnessValues . fst <$> Set.toList unique
     in ExtraFinalReport
        { maxSize = maxSize'
        , uniqueSizes = uniqueSizes'
        , uniqueValidSizes = uniqueValidSizes'
        -- , totalUnique = totalUnique'
        , totalUniqueValid = totalUniqueValid'
        , maxTree = maxTree'
        , diversity = diversity'
        , diversityBySize = diversityBySize'
        -- , uValid = uValid'
        , uniqueValidFitnessSummary = uniqueValidFitnessSummary'
        , uniqueFitnessSummary = uniqueFitnessSummary'
        }
     --  let uniqueSizes' = Utils.countBy (RBTree.countNodes . snd) (BaseReport._uniqueIndividuals baseReport)
     --    uniqueValidSizes' = Utils.countBy (RBTree.countNodes . snd) (BaseReport._validIndividuals baseReport)
     --    maxSize' = if null (Map.keys uniqueValidSizes') then -1 else maximum (Map.keys uniqueValidSizes')
     --    maxTree' = Utils.maximumOn (RBTree.countNodes . snd) (S.toList $ BaseReport._validIndividuals baseReport)
     --    -- uValid' = S.map (RBTree.toAscListC . snd) (BaseReport._validIndividuals baseReport)
     --    -- diversity' = Utils.calcDiversity uValid'
     --    diversity' = 0
     --    -- diversityBySize' = Utils.calcDiversityBySize uValid'
     --    diversityBySize' = Map.empty
     -- in ExtraFinalReport
     --    { maxSize = maxSize'
     --    , uniqueSizes = uniqueSizes'
     --    , uniqueValidSizes = uniqueValidSizes'
     --    , maxTree = maxTree'
     --    , diversity = diversity'
     --    , diversityBySize = diversityBySize'
     --    }
  }
