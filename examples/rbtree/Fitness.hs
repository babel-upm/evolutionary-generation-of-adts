module Fitness
  (
    rbTreeFitness,
    rbTreeFitnessMin,
    rbTreeFitnessMax,

    validFitness,
    maxDepthFitness,
    minDepthFitness,
    countNodesFitness,

    balanceFitnessMin,
    balanceFitnessMax,
    -- balanceFitnessStdDev,
    validityFitness,
    validityFitness',
    validityFitness2,

    validityFitnessMax,
    validityFitnessMin,
    validityFitnessMaxRelative,
    validityFitnessMinRelative,

    redRedFitness,
    redRedFitnessMin,
    redRedFitnessMax,
    relativeRedRed,
    relativeRedRedMax,
    relativeRedRedMin,

    sortedFitnessMin,
    sortedFitnessMax,
    sortedPairsFitnessMin,
    sortedPairsFitnessMax,
    sortedFitnessMinRelative,
    sortedFitnessMaxRelative,

    relativeDepthMax,
    relativeDepthMin,

    penalty,
    adjusted,
    towards,
  ) where

import Base (RBTree(..), toAscList)
import Properties (prop_RBValid)
-- import qualified RBTree
import qualified RBTreeUtils as Utils

import qualified Data.Vector as V
import Statistics.Sample (stdDev)

rbTreeFitness :: Ord a => (a -> a -> Bool) -> RBTree a -> Double
rbTreeFitness = rbTreeFitnessMin

rbTreeFitnessMin :: Ord a => (a -> a -> Bool) -> RBTree a -> Double
rbTreeFitnessMin f t = balanceFitnessMin t + redRedFitnessMin t + sortedFitnessMin f t

rbTreeFitnessMax :: Ord a => (a -> a -> Bool) -> RBTree a -> Double
rbTreeFitnessMax f t = balanceFitnessMax t + redRedFitnessMax t + sortedFitnessMax f t

rbTreeFitnessMaxRelative :: Ord a => (a -> a -> Bool) -> RBTree a -> Double
rbTreeFitnessMaxRelative f t = (balanceFitnessMaxRelative t + redRedFitnessMaxRelative t + sortedFitnessMaxRelative f t) / 3

-- Balance

balanceFitnessStdDevMin, balanceFitnessStdDevMax :: RBTree a -> Double
balanceFitnessStdDevMin = stdDev . V.fromList . fmap fromIntegral . Utils.balanceBLeafs
balanceFitnessStdDevMax t = maxDepthBFitness t - balanceFitnessStdDevMin t

balanceFitnessMin :: RBTree a -> Double
balanceFitnessMin t = abs (maxDepthBFitness t - minDepthBFitness t)

balanceFitnessMax :: RBTree a -> Double
balanceFitnessMax t = maxDepthBFitness t - balanceFitnessMin t

balanceFitnessMaxRelative :: RBTree a -> Double
balanceFitnessMaxRelative t = balanceFitnessMax t / maxDepthBFitness t

-- Coloring

redRedFitness :: RBTree a -> Double
redRedFitness = redRedFitnessMin

redRedFitnessMin :: RBTree a -> Double
redRedFitnessMin = fromIntegral . Utils.countRR

redRedFitnessMax :: RBTree a -> Double
redRedFitnessMax t = fromIntegral (Utils.countNodes t) - redRedFitnessMin t

redRedFitnessMaxRelative :: RBTree a -> Double
redRedFitnessMaxRelative t = redRedFitnessMax t / fromIntegral (Utils.countNodes t)

-- Sorting

sortedFitnessMin, sortedFitnessMax :: Ord a => (a -> a -> Bool) -> RBTree a -> Double
sortedFitnessMin f = sortedFitnessMin' f . toAscList
sortedFitnessMax f = sortedFitnessMax' f . toAscList

sortedFitnessMinRelative, sortedFitnessMaxRelative :: Ord a => (a -> a -> Bool) -> RBTree a -> Double
sortedFitnessMinRelative f t = sortedFitnessMin f t / fromIntegral (Utils.countNodes t)
sortedFitnessMaxRelative f t = sortedFitnessMax f t / fromIntegral (Utils.countNodes t)

sortedFitnessMin' :: (a -> a -> Bool) -> [a] -> Double
sortedFitnessMin' _ [] = 0
sortedFitnessMin' _ [_] = 0
sortedFitnessMin' f (x:y:ys)
  | x `f` y = sortedFitnessMin' f (y:ys)
  | otherwise = 1 + sortedFitnessMin' f (y:ys)

sortedFitnessMax' :: (a -> a -> Bool) -> [a] -> Double
sortedFitnessMax' _ [] = 0
sortedFitnessMax' _ [_] = 0
sortedFitnessMax' f (x:y:ys)
  | x `f` y = 1 + sortedFitnessMax' f (y:ys)
  | otherwise = sortedFitnessMax' f (y:ys)

sortedPairsFitnessMax :: (a -> a -> Bool) -> [a] -> Double
sortedPairsFitnessMax _ [] = 0
sortedPairsFitnessMax _ [_] = 1
sortedPairsFitnessMax f xs =
  fromIntegral (length correctPairs) / fromIntegral (length totalPairs)
    where
      xs' = zip [(1::Int)..] xs
      totalPairs = [ (x,y) | (i, x) <- xs', (j, y) <- xs', i < j ]
      correctPairs = filter (uncurry f) totalPairs

sortedPairsFitnessMin :: (a -> a -> Bool) -> [a] -> Double
sortedPairsFitnessMin _ [] = 0
sortedPairsFitnessMin _ [_] = 1
sortedPairsFitnessMin f xs =
  fromIntegral (length incorrectPairs) / fromIntegral (length totalPairs)
    where
      xs' = zip [(1::Int)..] xs
      totalPairs = [ (x,y) | (i, x) <- xs', (j, y) <- xs', i < j ]
      incorrectPairs = filter (not . uncurry f) totalPairs

--

adjusted :: Double -> Double
adjusted n = 1 / (1 + n)

towards :: Double -> (RBTree a -> Double) -> RBTree a -> Double
towards n f t = abs $ n - f t

penalty :: Int -> (a -> Int) -> (a -> Double) -> a -> Double
penalty t sizeOf f x =
  let
    baseFitness = f x
    size = fromIntegral (sizeOf x)
    target = fromIntegral t
    sizeModifier = min 1 (size / target)
    sizePenalty = if size <= target then 1 else max 0 (1 - (((size - target) / target)))**2
  in
    baseFitness * sizeModifier * sizePenalty

validFitness :: Ord a => RBTree a -> Double
validFitness = (\b -> if b then 1 else 0) . prop_RBValid

countNodesFitness :: RBTree a -> Double
countNodesFitness = fromIntegral . Utils.countNodes

maxDepthFitness :: RBTree a -> Double
maxDepthFitness = fromIntegral . Utils.maxDepthRB

maxDepthBFitness :: RBTree a -> Double
maxDepthBFitness = fromIntegral . Utils.maxDepthB

minDepthFitness :: RBTree a -> Double
minDepthFitness = fromIntegral . Utils.minDepthRB

minDepthBFitness :: RBTree a -> Double
minDepthBFitness = fromIntegral . Utils.minDepthB

validityFitness :: Ord a => RBTree a -> Double
validityFitness = uncurry (/) . validityFitness'

validityFitness' :: Ord a => RBTree a -> (Double, Double)
validityFitness' E = (1, 1)
validityFitness' t =
  let
    nodeValid = if prop_RBValid t then 1 else 0
    (lValid, lNodes) = validityFitness' (Utils.left t)
    (rValid, rNodes) = validityFitness' (Utils.right t)
  in
    (nodeValid + lValid + rValid, 1 + lNodes + rNodes)

validityFitness2 :: Ord a => RBTree a -> Double
validityFitness2 = abs . uncurry (-) . validityFitness'

validityFitnessMax :: Ord a => RBTree a -> Double
validityFitnessMax E = 0
validityFitnessMax t = (if prop_RBValid t then 1 else 0) + validityFitnessMax (Utils.left t) + validityFitnessMax (Utils.right t)

validityFitnessMin :: Ord a => RBTree a -> Double
validityFitnessMin E = 0
validityFitnessMin t = (if prop_RBValid t then 0 else 1) + validityFitnessMin (Utils.left t) + validityFitnessMin (Utils.right t)

validityFitnessMaxRelative :: Ord a => RBTree a -> Double
validityFitnessMaxRelative tree = validityFitnessMax tree / max 1 (fromIntegral (Utils.countNodes tree))

validityFitnessMinRelative :: Ord a => RBTree a -> Double
validityFitnessMinRelative tree = validityFitnessMin tree / max 1 (fromIntegral (Utils.countNodes tree))

relativeRedRed :: RBTree a -> Double
relativeRedRed tree = noRedRedPairs / totalPairs
  where
    noRedRedPairs = fromIntegral (Utils.countPairs tree - Utils.countRR tree)
    totalPairs = fromIntegral (max 1 (Utils.countPairs tree))

relativeRedRedMax :: RBTree a -> Double
relativeRedRedMax = relativeRedRed

relativeRedRedMin :: RBTree a -> Double
relativeRedRedMin = (1 -) . relativeRedRedMax

relativeDepthMax, relativeDepthMin :: RBTree a -> Double
relativeDepthMax tree = fromIntegral (Utils.minDepthB tree) / max 1 (fromIntegral (Utils.maxDepthB tree))

relativeDepthMin = (1 -) . relativeDepthMax

-- fitness' :: (a -> a -> Bool) -> Double -> RBTree a -> Double
-- fitness' _ _ E = 0
-- fitness' f targetSize tree = sizePenalty * sizeModifier * (fSorted + fRR + fDepth) / 3
--   where
--     fSorted = sortedPairsFitness f (toAscList tree)
--     fRR = fromIntegral (Utils.countPairs tree - Utils.countRR tree) / fromIntegral (min 1 (Utils.countPairs tree))
--     fDepth = balanceFitness tree / min 1 (maxDepthBFitness tree)

--     size = fromIntegral (Utils.countNodes tree)
--     sizeModifier = min 1 (size / targetSize)
--     sizePenalty =
--       if size <= targetSize then 1                             -- Allow smaller (do not modify)
--       else max 0 (1 - (((size - targetSize) / targetSize)**2)) -- Penalty for bigger trees
