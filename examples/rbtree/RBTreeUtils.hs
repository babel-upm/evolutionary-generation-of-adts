{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
module RBTreeUtils
  (
    left,
    right,
    unsafeJoin,
    replaceLeft,
    replaceRight,

    balance,
    balanceBLeafs,
    countNodes,
    countNodesLeaves,
    countPairs,
    countB,
    countR,
    countRR,
    depthB,
    maxDepthB,
    maxDepthR,
    maxDepthRB,
    minDepthB,
    minDepthR,
    minDepthRB,

    treeStructure,
    pruneAtDepth,
    pruneAtDepthB,
    pruneAtDepthR,
    pruneAtDepthRB,

    NPI(),
    npi,
    npiLeafs,
    atNpi,
    removeNpi,
    replaceNodeAtNpi,
    replaceTreeAtNpi,

    lg
  ) where

import Base
import Test.QuickCheck

lg :: Int -> Int
lg n =
  let base = fromIntegral (2 :: Integer) :: Double
      arg = fromIntegral n :: Double
   in round (logBase base arg)

left :: RBTree a -> RBTree a
left E = E
left (T _ l _ _) = l

right :: RBTree a -> RBTree a
right E = E
right (T _ _ _ r) = r

unsafeJoin :: RBTree a -> a -> RBTree a -> RBTree a
unsafeJoin = T B

replaceLeft :: RBTree a -> RBTree a -> RBTree a
replaceLeft (T c _ v r) t = T c t v r
replaceLeft _ t = t

replaceRight :: RBTree a -> RBTree a -> RBTree a
replaceRight (T c l v _) t = T c l v t
replaceRight _ t = t


-- Does not assume balance
balance :: RBTree a -> (Int, Int)
balance t = (depthB (left t), depthB (right t))

balanceBLeafs :: RBTree a -> [Int]
balanceBLeafs (T c l _ r) =
  let leafsL = fmap (if c == B then fmap (1+) else id) balanceBLeafs l
      leafsR = fmap (if c == B then fmap (1+) else id) balanceBLeafs r
   in leafsL ++ leafsR
balanceBLeafs E = [1]
-- balanceBLeafs _ = [0]

countNodes :: RBTree a -> Int
countNodes (T _ l _ r) = 1 + countNodes l + countNodes r
countNodes _ = 0

countNodesLeaves :: RBTree a -> Int
countNodesLeaves (T _ l _ r) = 1 + countNodes l + countNodes r
countNodesLeaves E = 1


countR :: RBTree a -> Int
countR (T R l _ r) = 1 + countR l + countR r
countR (T B l _ r) = countR l + countR r
countR _ = 0

countB :: RBTree a -> Int
countB (T B l _ r) = 1 + countB l + countB r
countB (T R l _ r) = countB l + countB r
countB _ = 0

countRR :: RBTree a -> Int
countRR = countRR' B
  where
    countRR' :: Color -> RBTree a -> Int
    countRR' R (T R l _ r) = 1 + countRR' R l + countRR' R r
    countRR' _ (T c l _ r) = countRR' c l + countRR' c r
    countRR' _ _ = 0

countPairs :: RBTree a -> Int
countPairs (T _ l _ r) = 2 + countPairs l + countPairs r
countPairs _ = 0

depthB :: RBTree a -> Int
depthB E = 1
depthB (T B l _ _) = 1 + depthB l
depthB (T _ l _ _) = depthB l

maxDepthB :: RBTree a -> Int
maxDepthB E = 1
maxDepthB (T B l _ r) = 1 + Prelude.max (maxDepthB l) (maxDepthB r)
maxDepthB (T _ l _ r) = Prelude.max (maxDepthB l) (maxDepthB r)

maxDepthR :: RBTree a -> Int
maxDepthR E = 1
maxDepthR (T R l _ r) = 1 + Prelude.max (maxDepthR l) (maxDepthR r)
maxDepthR (T _ l _ r) = Prelude.max (maxDepthR l) (maxDepthR r)

maxDepthRB :: RBTree a -> Int
maxDepthRB E = 1
maxDepthRB (T _ l _ r) = 1 + Prelude.max (maxDepthRB l) (maxDepthRB r)

minDepthB :: RBTree a -> Int
minDepthB E = 0
minDepthB (T B l _ r) = 1 + Prelude.min (minDepthB l) (minDepthB r)
minDepthB (T _ l _ r) = Prelude.min (minDepthB l) (minDepthB r)

minDepthR :: RBTree a -> Int
minDepthR E = 0
minDepthR (T R l _ r) = 1 + Prelude.min (minDepthR l) (minDepthR r)
minDepthR (T _ l _ r) = Prelude.min (minDepthR l) (minDepthR r)

minDepthRB :: RBTree a -> Int
minDepthRB E = 1
minDepthRB (T _ l _ r) = 1 + Prelude.min (minDepthRB l) (minDepthRB r)



-- | Other operations


treeStructure :: RBTree a -> RBTree ()
treeStructure (T c l _ r) = T c (treeStructure l) () (treeStructure r)
treeStructure _ = E

pruneAtDepth :: Int -> RBTree a -> RBTree a
pruneAtDepth 0 _ = E
pruneAtDepth n (T c l v r) = T c (pruneAtDepth (n - 1) l) v (pruneAtDepth (n - 1) r)
pruneAtDepth _ _ = E

pruneAtDepthB :: Int -> RBTree a -> RBTree a
pruneAtDepthB 0 _ = E
pruneAtDepthB n (T B l v r) = T B (pruneAtDepthB (n - 1) l) v (pruneAtDepthB (n - 1) r)
pruneAtDepthB n (T R l v r) = T R (pruneAtDepthB n l) v (pruneAtDepthB n r)
pruneAtDepthB _ _ = E

pruneAtDepthR :: Int -> RBTree a -> RBTree a
pruneAtDepthR 0 _ = E
pruneAtDepthR n (T B l v r) = T B (pruneAtDepthR n l) v (pruneAtDepthR n r)
pruneAtDepthR n (T R l v r) = T R (pruneAtDepthR (n - 1) l) v (pruneAtDepthR (n - 1) r)
pruneAtDepthR _ _ = E

pruneAtDepthRB :: Int -> RBTree a -> RBTree a
pruneAtDepthRB = pruneAtDepth

-- | NPI: Node Posisition Index
--
-- We do have a way to encode the position of each node. We can either go Left, Right, or Stay.
-- Given the following tree
--           _______6_______
--          /               \
--      ___4___           ___8___
--     /       \         /       \
--    2       _5       7         9
--           /   \
--          1     3
--
-- To reach value 5 we will perform the following operations:
--
--  L -> R
--
-- Furtermore, they could be encoded with L=0 and R=1, beeing [0,1]. Or, in
-- binary, number 01. To disambiguate, all list should start with a 1. That is:
-- 101, or 5 in decimal.
--
-- The result for each node will be:
--           ______[]_______
--          /               \
--      __[L]__           __[R]__
--     /       \         /       \
--  [L,L]     [L,R]   [R,L]     [R,R]
--           /    \
--      [L,R,L]  [L,R,R]
--
data NPI = NPIL | NPIR deriving (Show, Eq)

npi :: RBTree a -> [[NPI]]
npi (T _ l _ r) =
  let npiL = (NPIL :) <$> npi l
      npiR = (NPIR :) <$> npi r
   in [[]] ++ npiL ++ npiR
npi _ = [[]]

-- | Returns only the NPI of the leafs
npiLeafs :: RBTree a -> [[NPI]]
npiLeafs (T _ l _ r) =
  let npiL = (NPIL :) <$> npiLeafs l
      npiR = (NPIR :) <$> npiLeafs r
   in npiL ++ npiR
npiLeafs _ = [[]]

atNpi :: [NPI] -> RBTree a -> RBTree a
atNpi (x:xs) (T _ l _ r)
  | x == NPIL = atNpi xs l
  | otherwise = atNpi xs r
atNpi _ t = t

removeNpi :: [NPI] -> RBTree a -> RBTree a
removeNpi [] _ = E
removeNpi (x:xs) (T c l v r) =
  case x of
    NPIL -> T c (removeNpi xs l) v r
    NPIR -> T c l v (removeNpi xs r)
removeNpi _ _ = E

replaceNodeAtNpi :: [NPI] -> Color -> a -> RBTree a -> RBTree a
replaceNodeAtNpi [] color a (T _ l _ r) = T color l a r
replaceNodeAtNpi (x:xs) color a (T c l v r) =
  case x of
    NPIL -> let l' = replaceNodeAtNpi xs color a l
             in T c l' v r
    NPIR -> let r' = replaceNodeAtNpi xs color a r
             in T c l v r'
replaceNodeAtNpi _ color a _ = T color E a E

replaceTreeAtNpi :: [NPI] -> RBTree a -> RBTree a -> RBTree a
replaceTreeAtNpi (x:xs) subT (T c l v r) =
  case x of
    NPIL -> let l' = replaceTreeAtNpi xs subT l
             in T c l' v r
    NPIR -> let r' = replaceTreeAtNpi xs subT r
             in T c l v r'
replaceTreeAtNpi _ subT _ = subT

instance Arbitrary NPI where
  arbitrary = elements [NPIL, NPIR]
