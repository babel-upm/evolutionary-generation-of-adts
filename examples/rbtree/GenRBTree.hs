module GenRBTree
  (
    unboundedTree,
    boundedTree,
    balTree,
    nrrTree,
    balnrrTree,
    ordbalnrrTree,
    rbTree,
    -- insertedTree
  )
where

import RBTreeUtils (lg)
import Base (RBTree(..), Color(..))

import Test.QuickCheck
import Control.Monad (liftM4)
import System.Random

-- Generate unbounded trees:
unboundedTree :: Arbitrary a => Gen (RBTree a)
unboundedTree =
  oneof
    [ return E,
      liftM4
        T
        (oneof [return R, return B])
        unboundedTree
        arbitrary
        unboundedTree
    ]

boundedTree :: Arbitrary a => Gen (RBTree a)
boundedTree = sized tree
  where
    tree :: Arbitrary a => Int -> Gen (RBTree a)
    tree n
      | n > 0 =
          let subtree = tree (n `div` 2)
              color = oneof [return R, return B]
           in oneof
                [ return E,
                  liftM4 T color subtree arbitrary subtree
                ]
      | otherwise = return E

-- Balanced trees
balTree :: Arbitrary a => Gen (RBTree a)
balTree = sized tree
  where
    tree :: Arbitrary a => Int -> Gen (RBTree a)
    tree n
      | n <= 1 = return E
      | otherwise =
          let subtree = tree (n `div` 2)
              color = oneof [return R, return B]
           in liftM4 T color subtree arbitrary subtree

-- Generate trees with no red-red violations:
nrrTree :: Arbitrary a => Gen (RBTree a)
nrrTree = sized (tree R)
  where
    tree :: Arbitrary a => Color -> Int -> Gen (RBTree a)

    -- Assuming black parent:
    tree B n
      | n > 0 =
          let subtree = tree B (n `div` 2)
              subtree' = tree R (n `div` 2)
           in oneof
                [ return E,
                  liftM4 T (return B) subtree arbitrary subtree,
                  liftM4 T (return R) subtree' arbitrary subtree'
                ]
    tree R n
      | n > 0 =
          let subtree = tree B (n `div` 2)
           in oneof
                [ return E,
                  liftM4 T (return B) subtree arbitrary subtree
                ]
    tree _ _ = return E

-- Generate black-balanced trees with no red-red violations:
balnrrTree :: Arbitrary a => Gen (RBTree a)
balnrrTree = sized (tree R . lg)
  where
    tree :: Arbitrary a => Color -> Int -> Gen (RBTree a)

    tree B 1 =
      oneof
        [ return E,
          liftM4 T (return R) (return E) arbitrary (return E)
        ]
    tree B n
      | n > 1 =
          oneof
            [ liftM4 T (return B) subtree arbitrary subtree,
              liftM4 T (return R) subtree' arbitrary subtree'
            ]
      where
        subtree = tree B (n - 1)
        subtree' = tree R n
    tree R n
      | n > 1 =
          oneof [liftM4 T (return B) subtree arbitrary subtree]
      where
        subtree = tree B (n - 1)
    tree _ _ = return E

-- Generate ordered, black-balanced trees with no red-red violations:
ordbalnrrTree ::
  ( Random a,
    Ord a,
    Num a
  ) =>
  Gen (RBTree a)
ordbalnrrTree =
  sized (tree 0 100000000000000 R . lg)
  where
    tree :: (Random a, Ord a, Num a) => a -> a -> Color -> Int -> Gen (RBTree a)
    tree min' max' _ _ | max' < min' = error "cannot generate ordbalnrrTree: max >= min"
    tree _min' _max' B 0 = return E
    tree min' max' B 1 =
      oneof
        [ -- return E,
          liftM4 T (return R) (return E) (choose (min', max')) (return E)
          -- Is it possible that returning a Black node here is also valid?
        ]
    tree min' max' B n | n > 0 =
      do
        key <- choose (min', max')
        let subtree1 = tree min' (key - 1) B (n - 1)
        let subtree2 = tree (key + 1) max' B (n - 1)
        let subtree1' = tree min' (key - 1) R n
        let subtree2' = tree (key + 1) max' R n
        oneof
          [ liftM4 T (return B) subtree1 (return key) subtree2,
            liftM4 T (return R) subtree1' (return key) subtree2'
          ]
    tree _min' _max' R 0 = return E
    -- tree _min' _max' R 1 = return E
    tree min' max' R n | n > 0 =
      do
        key <- choose (min', max')
        let subtree1 = tree min' (key - 1) B (n - 1)
        let subtree2 = tree (key + 1) max' B (n - 1)
        oneof [liftM4 T (return B) subtree1 (return key) subtree2]
    tree _ _ _ _ = return E

rbTree ::
  ( Random a,
    Ord a,
    Num a
  ) =>
  Gen (RBTree a)
rbTree = ordbalnrrTree

-- Generate trees from insertions:
-- insertedTree :: (Arbitrary a, Ord a) => Gen (RBTree a)
-- insertedTree = foldr insert empty <$> listOf arbitrary
