{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveGeneric #-}
module Base
  (
    RBTree (..),
    Color(..),
    toAscList,
    toAscListC
  )
where

import Control.DeepSeq (NFData)
import Data.Aeson
import Data.Data
import GHC.Generics

data Color = B | R deriving (Show, Eq, Ord, Typeable, Data, Generic)

instance NFData Color
instance ToJSON Color
instance FromJSON Color

data RBTree a = E | T Color (RBTree a) a (RBTree a) deriving (Show, Eq, Ord, Typeable, Data, Generic)

instance NFData a => NFData (RBTree a)
instance ToJSON a => ToJSON (RBTree a)
instance FromJSON a => FromJSON (RBTree a)

-- Used for Levenshtein distance
data V a = VC Color | V a deriving (Show, Eq, Ord, Typeable, Data, Generic)

instance Functor RBTree where
  fmap f (T c l v r) = T c (fmap f l) (f v) (fmap f r)
  fmap _ E = E

toAscList :: RBTree a -> [a]
toAscList E = []
toAscList (T _ l x r) = toAscList l ++ [x] ++ toAscList r

toAscListC :: RBTree a -> [V a]
toAscListC E = []
toAscListC (T c l x r) = toAscListC l ++ [VC c, V x] ++ toAscListC r
