{-# OPTIONS_GHC -fhpc #-}
module Properties
  (
    prop_BlackRoot,
    prop_NoRedRed,
    prop_BlackBalanced,
    prop_BlackBalanced',
    prop_Ordered,
    prop_RBValid,
    prop_RBValidBlackRoot
  ) where

import Base (RBTree(..), Color(..), toAscList)
import RBTreeUtils (minDepthB, maxDepthB)

-- Count the black depth of a red-black tree:
blackDepth :: RBTree a -> Maybe Int
blackDepth E = Just 1
blackDepth (T R l _ r) = case (blackDepth l, blackDepth r) of
  (Just n, Just m) -> if n == m then Just n else Nothing
  (_, _) -> Nothing
blackDepth (T B l _ r) = case (blackDepth l, blackDepth r) of
  (Just n, Just m) -> if n == m then Just (1 + n) else Nothing
  (_, _) -> Nothing
-- blackDepth _ = Nothing

prop_BlackRoot :: RBTree a -> Bool
prop_BlackRoot (T R _ _ _) = False
prop_BlackRoot _ = True

-- Check for red-red violations:
prop_NoRedRed :: RBTree a -> Bool
prop_NoRedRed E = True
prop_NoRedRed (T R (T R _ _ _) _ _) = False
prop_NoRedRed (T R _ _ (T R _ _ _)) = False
prop_NoRedRed (T _ l _x r) = prop_NoRedRed l && prop_NoRedRed r
-- prop_NoRedRed _ = False

-- Check for black-balanced violations:
prop_BlackBalanced :: RBTree a -> Bool
prop_BlackBalanced t =
  case blackDepth t of
    Just _ -> True
    Nothing -> False

prop_BlackBalanced' :: RBTree a -> Bool
prop_BlackBalanced' t = minDepthB t == maxDepthB t

-- Check for ordering violations:
prop_OrderedList :: Ord a => [a] -> Bool
prop_OrderedList [] = True
prop_OrderedList [_] = True
prop_OrderedList (x : y : tl) = (x <= y) && prop_OrderedList (y : tl)

prop_Ordered :: Ord a => RBTree a -> Bool
prop_Ordered t = prop_OrderedList (toAscList t)

-- Check for the validity of a red-black tree:
prop_RBValid :: Ord a => RBTree a -> Bool
prop_RBValid t = prop_NoRedRed t && prop_BlackBalanced t && prop_Ordered t

prop_RBValidBlackRoot :: Ord a => RBTree a -> Bool
prop_RBValidBlackRoot t = prop_BlackRoot t && prop_RBValid t
