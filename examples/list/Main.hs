{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -Wno-incomplete-uni-patterns #-}
{-# OPTIONS_GHC -Wno-missing-export-lists #-}
module Main where

-- import Debug.Trace

import G3.EA
import G3.Experiment (Experiment(..), runExperiment)
import G3.Experiment.Options (RunOptions(..), RuntimeOpt(..), parseMultipleOptions, runOptionsList)
import qualified G3.Experiment as BaseReport (BaseReport(..))
import G3.EA.Runtime (gaLoop, gpLoop, customLoop, nsga2Step)
import qualified Utils
-- import G3.TH (g3Data, NPI(..))

import Control.DeepSeq (NFData)
import Control.Monad (forM_)
import Data.Aeson
import Data.Map.Strict (Map)
import Data.Ord (Down(..))

import GHC.Generics
import Test.QuickCheck hiding (maxSize)

import qualified Data.List as List
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import qualified Data.Vector as V
import qualified Statistics.Sample as Statistics

--------------
-- Property --
--------------

mySort :: Ord a => a -> a -> Bool
mySort = (<=)

propSorted :: Ord a => [a] -> Bool
propSorted (x:y:ys) = x `mySort` y && propSorted (y:ys)
propSorted _ = True

---------------
-- Generator --
---------------

-- sortedList :: (Num a, Arbitrary a) => Gen [a]
-- sortedList = do
--   list <- arbitrary -- Generates an arbitrary list of a
--   case list of
--     [] -> return []
--     (x:xs) -> return (scanr (+) x xs)

-------------
-- Fitness --
-------------

fitnessSorted :: Ord a => [a] -> Int
fitnessSorted (x:y:ys) = (if x `mySort` y then 0 else 1) + fitnessSorted (y:ys)
fitnessSorted _ = 0

fitnessSortedMax :: Ord a => [a] -> Int
fitnessSortedMax (x:y:ys) = (if x `mySort` y then 1 else 0) + fitnessSortedMax (y:ys)
fitnessSortedMax _ = 0

fitnessSortedLength :: Ord a => [a] -> Double
fitnessSortedLength xs@(_:_:_) = fromIntegral (fitnessSorted xs) / fromIntegral (length xs)
fitnessSortedLength _ = 0

-- fitnessSortedPairs :: Ord a => [a] -> Int
-- fitnessSortedPairs (x:y:xs) = (if x < y then 0 else 1) + fitnessSortedPairs (y:xs)
-- fitnessSortedPairs _ = 0

bool01 :: Num n => (a -> Bool) -> a -> n
bool01 f a = if f a then 0 else 1

bool10 :: Num n => (a -> Bool) -> a -> n
bool10 f a = if f a then 1 else 0


fitnessStructural :: ([a] -> Bool) -> [a] -> Int
fitnessStructural f [] = bool01 f []
fitnessStructural f x@(_:xs) = bool01 f x + fitnessStructural f xs

fitnessStructuralMax :: ([a] -> Bool) -> [a] -> Int
fitnessStructuralMax f [] = bool10 f []
fitnessStructuralMax f x@(_:xs) = bool10 f x + fitnessStructuralMax f xs

fitnessSortedAllPairs :: Ord a => [a] -> Double
fitnessSortedAllPairs [] = 0
fitnessSortedAllPairs [_] = 1
fitnessSortedAllPairs xs =
  fromIntegral (length correctPairs) / fromIntegral (length totalPairs)
    where
      xs' = zip [(1::Int)..] xs
      totalPairs = [ (x,y) | (i, x) <- xs', (j, y) <- xs', i < j ]
      correctPairs = filter (uncurry mySort) totalPairs

fitnessSortedIncorrectPairs :: Ord a => [a] -> Double
fitnessSortedIncorrectPairs [] = 0
fitnessSortedIncorrectPairs [_] = 1
fitnessSortedIncorrectPairs xs =
  fromIntegral (length incorrectPairs) / fromIntegral (length totalPairs)
    where
      xs' = zip [(1::Int)..] xs
      totalPairs = [ (x,y) | (i, x) <- xs', (j, y) <- xs', i < j ]
      incorrectPairs = filter (not . uncurry mySort) totalPairs

fitness' :: Int -> ([a] -> Double) -> [a] -> Double
fitness' t f xs =
  let
    baseFitness = f xs
    size = fromIntegral (length xs)
    target = fromIntegral t
    sizeModifier = min 1 (size / target)
    sizePenalty = if size <= target then 1 else max 0 (1 - (((size - target) / target)**2))
  in
    baseFitness * sizeModifier * sizePenalty

penaltyMin' :: Int -> ([a] -> Double) -> [a] -> Double
penaltyMin' t f xs =
  let
    baseFitness = f xs
    size = fromIntegral (length xs)
    target = fromIntegral t
    sizeModifier = 1 / min 1 (size / target)
    sizePenalty = if size <= target then 1 else 1 / max 0 (1 - (((size - target) / target)**2))
  in
    baseFitness * sizeModifier * sizePenalty

-- fitnessMaxSortedSublist :: Ord a => [a] -> Int
-- fitnessMaxSortedSublist xs = go 0 0 xs
--   where
--     go :: Ord a => Int -> Int -> [a] -> Int
--     go accMax curr (x:y:ys) =
--       let curr' = if x <= y then curr + 1 else 0
--           accMax' = max accMax curr'
--        in go accMax' curr' (y:ys)
--     go accMax _curr _ = accMax


-- https://github.com/astanin/moo/blob/master/examples/gensort.hs
-- sortingFitness :: (Ord a) => [a] -> Double
-- sortingFitness [] = 1
-- sortingFitness [_] = 1
-- sortingFitness problem =
--       (fromIntegral . sortcount) problem
--       / (fromIntegral . twoOutOf . length) problem
--   where
--     sortcount :: (Ord a) => [a] -> Int
--     sortcount (x:xs) = (sum . map (sortcount' x)) xs + sortcount xs
--       where
--         sortcount' :: (Ord a) => a -> a -> Int
--         sortcount' x y
--           | x > y   = 0
--           | otherwise = 1
--         --sortcount' x y = (length . filter id . (\x -> [x])) (x <= y)
--     sortcount [] = 0

--     twoOutOf :: Int -> Int
--     twoOutOf n
--       | n >= 1 = n - 1 + twoOutOf (n-1)
--       | otherwise = 0


-- compareFitness :: [[a] -> Double] -> [[a]] -> [[Double]]
-- compareFitness fs xs = fmap (\x -> (fmap (\f -> f x) fs)) xs

-- applyFitness :: [[a] -> Double] -> [a] -> ([a], [Double])
-- applyFitness fs x = (x, (fmap (\f -> f x) fs))

----------------
-- Operations --
----------------

listCrossover :: [a] -> [a] -> Gen [[a]]
listCrossover x y = do
  pX <- chooseInt (0, length x)
  pY <- chooseInt (0, length x)
  let (xL, xR) = splitAt pX x
  let (yL, yR) = splitAt pY y
  return [xL ++ yR, yL ++ xR]

listCrossoverUniform :: [a] -> [a] -> Gen [[a]]
listCrossoverUniform [] ys = return [[], ys]
listCrossoverUniform xs [] = return [xs, []]
listCrossoverUniform (x:xs) (y:ys) = do
  b <- arbitrary :: Gen Bool
  let (x', y') = if b then (x, y) else (y, x)
  tail' <- listCrossoverUniform xs ys

  let [xs', ys'] = tail'
  return [x':xs', y':ys']


listMutInsert :: Arbitrary a => [a] -> Gen [a]
listMutInsert xs = do
  x <- arbitrary
  return (xs ++ [x])

listMutInsertAt :: Arbitrary a => [a] -> Gen [a]
listMutInsertAt xs = do
  i <- chooseInt (0, length xs)
  x <- arbitrary
  let (xL, xR) = splitAt i xs
  return (xL ++ (x:xR))

-- g3Data ''[]

-- listMutRemove :: [a] -> Gen [a]
-- listMutRemove [] = return []
-- listMutRemove xs = return (init xs)

listMutRemove :: [a] -> Gen [a]
listMutRemove xs = do
  i <- chooseInt (0, length xs)
  return (listRemoveAt i xs)

listMutValue :: Arbitrary a => [a] -> Gen [a]
listMutValue xs = do
  i <- chooseInt (0, length xs)
  x <- arbitrary
  return (listReplaceAt i x xs)

listMutTake :: [a] -> Gen [a]
listMutTake xs = do
  i <- chooseInt (0, length xs)
  return (take i xs)

listMutDrop :: [a] -> Gen [a]
listMutDrop xs = do
  i <- chooseInt (0, length xs)
  return (drop i xs)

listMutDropTake :: [a] -> Gen [a]
listMutDropTake xs = do
  i <- chooseInt (0, length xs)
  j <- chooseInt (i, length xs)
  return $ take j . drop i $ xs

listMutSwap :: [a] -> Gen [a]
listMutSwap [] = return []
listMutSwap xs = do
  i <- chooseInt (0, length xs - 1)
  j <- chooseInt (0, length xs - 1)
  let vi = xs !! i
  let vj = xs !! j
  return $ listReplaceAt j vi . listReplaceAt i vj $ xs

-----------
-- Utils --
-----------

listRemoveAt :: Int -> [a] -> [a]
listRemoveAt _ [] = []
listRemoveAt 0 (_:xs) = xs
listRemoveAt n (x:xs) = x:listRemoveAt (n-1) xs

listReplaceAt :: Int -> a -> [a] -> [a]
listReplaceAt _ _ [] = []
listReplaceAt 0 y (_:xs) = y:xs
listReplaceAt n y (x:xs) = x:listReplaceAt (n-1) y xs

sized' :: ([a] -> Int) -> ([a] -> Double)
sized' _ [] = 1
sized' f xs = fromIntegral (f xs) / fromIntegral (length xs)

sizeFitness :: [a] -> Double
sizeFitness xs = 1 / (1 + fromIntegral (length xs))

-- adjusted :: Double -> Double
-- adjusted n = 1 / (1 + n)

towards :: Num n => n -> (a -> n) -> a -> n
towards n f t = abs $ n - f t

aim :: (Ord n, Fractional n) => n -> (a -> n) -> a -> n
aim t f x =
  let
    n = f x
  in
    if n <= t
    then n/t
    else max 0 (1 - ((n - t)/t * 1.25))

----------
-- Main --
----------

-- main :: IO ()
-- main = do
--   steps <- generate (g3 search)
--   print (last steps)
--   where
--     search = GSearch {
--       gen = arbitrary :: Gen [Int],
--       fit = minimize "sorted list" . sized fitnessSorted,
--       popSize = 100,
--       maxSteps = 1000,
--       selectionType = return . selectionByElite 20,
--       mutationRatio = 4 % 10,
--       ops = [
--           CrossoverOp listCrossover,
--           MutationOp listMutValue,
--           MutationOp listMutInsert,
--           MutationOp listMutRemove
--         ],
--       extra = id
--       }


data ExtraStepReport' a = ExtraStepReport
  { popSummary :: [([Double], Int)] -- Represents the fitness values and the size, ordered by fitness
  , stepSizes :: Map Int Int
  , uniqueStepSizes :: Map Int Int
  , validStepSizes :: Map Int Int
  , best :: ([Double], a)
  , meanFitness :: Double
  , meanSize :: Double
  , stepDiversity :: Double
  } deriving (Show, Eq, Ord, Generic)

instance ToJSON a => ToJSON (ExtraStepReport' a)
instance FromJSON a => FromJSON (ExtraStepReport' a)

data ExtraFinalReport' a = ExtraFinalReport
  { maxSize :: Int
  , uniqueSizes :: Map Int Int
  , uniqueValidSizes :: Map Int Int
  , maxList :: Individual [a]
  , diversity :: Double
  , diversityBySize :: Map Int Double
  -- , totalUnique :: Int
  , totalUniqueValid :: Int
  -- , uValid :: Set [Int]
  , uniqueValidFitnessSummary :: [([Double], Int)] -- Represents the fitness values and the size, ordered by fitness
  , uniqueFitnessSummary :: [[Double]] -- Represents the fitness values and the size, ordered by fitness
  } deriving (Show, Generic)

instance ToJSON a => ToJSON (ExtraFinalReport' a)
instance FromJSON a => FromJSON (ExtraFinalReport' a)

type Experiment' = Experiment [Int'] (ExtraStepReport' [Int]) (ExtraFinalReport' Int')

newtype Int' = Int' { getInt' :: Int } deriving (Show, Eq, Ord, Generic)

instance ToJSON Int'
instance FromJSON Int'
instance NFData Int'

instance Arbitrary Int' where
  arbitrary = do
    let r = 100
    n <- chooseInt (-r, r)
    return (Int' n)

main :: IO ()
main = do
  multiOpts <- parseMultipleOptions

  -- Multipe options can be received. They are combined into a list of single
  -- options For example, mutliple generation options (-g 100 -g 1000) will
  -- execute two experiments, the first one with 100 generations and the second
  -- one with 1000
  forM_ (runOptionsList multiOpts) $ \opts -> do
    timePrefix <- Utils.dateTimePrefix
    let r = repeatN opts

    -- If the experiment is said to be repeated N time, we use that index to identify the iteration.
    Utils.replicateMWithIndex_ r $ \repeatIndex -> do
      let repeatPrefix = if r > 0 then show repeatIndex else ""
      let prefix = filePrefix opts <> (if  "" == filePrefix opts then "" else "_") <> timePrefix <> "_" <> repeatPrefix
      let suffix = "_" <>
            show (optGenerations opts) <> "g_" <>
            show (optPopulation opts) <> "p_" <>
            show (optMutation opts) <> "m_" <>
            show (optCrossoverP opts) <> "x_" <>
            show (optIndividualSize opts) <> "s_" <>
            show (runtimeStep opts)

      mapM_ runExperiment (
        filterExperiments (cases opts) (exclude opts).
        fmap (\ex -> ex { title = prefix <> "." <> title ex <> suffix}) $
        experimentList opts)

filterExperiments :: [String] -> [String] -> [Experiment'] -> [Experiment']
filterExperiments [] [] ecs = ecs
filterExperiments [] exclusions ecs =
  filter (\ec -> not $ any (\p -> p `List.isInfixOf` title ec) exclusions) ecs
filterExperiments patterns exclusions ecs =
  let included = filter (\ec -> any (\p -> p `List.isInfixOf` title ec) patterns) ecs
  in filterExperiments [] exclusions included

experimentList :: RunOptions -> [Experiment']
experimentList options =
  let
    -- nodes' = fromIntegral (optIndividualSize options)
    experiment = baseExperiment {
        searchPopulation = optPopulation options,
        searchMutation = optMutation options,
        searchCrossover = optCrossoverP options,
        searchGenerations = optGenerations options,
        checkDiversity = optCheckDiversity options,
        showStepReport = optShowStepReport options,
        showFinalReport = optShowFinalReport options,
        targetSize = optIndividualSize options,
        searchStep = case runtimeStep options of
            GALoop -> gaLoop
            GPLoop -> gpLoop
            NSGA2 -> nsga2Step
            CustomLoop -> customLoop
      }
  in
    [
      experiment { title = "random",
                   searchFitness = maximize "zero" . const 0,
                   searchOperators = [ MutationOp (const (searchGenerator baseExperiment)) ],
                   searchSelection = return,
                   searchMutation = 100
                 },
      experiment { title = "list_sorted_simple",
                   searchFitness = minimize "fitnessSorted" . fromIntegral . fitnessSorted
                 },
      -- Paper experiments
      experiment { title = "A",
                   searchFitness = maximize "zero" . const 0,
                   searchOperators = [ MutationOp (const (searchGenerator baseExperiment)) ],
                   searchSelection = return,
                   searchMutation = 100
                 },
      experiment { title = "B",
                   searchFitness = minimize "fitnessSorted" . fromIntegral . fitnessSorted
                 },
      experiment { title = "C",
                   searchFitness = minimize "fitnessSorted'" . fromIntegral . fitnessStructural propSorted
                 },
      experiment { title = "D",
                   searchFitness = minimize "sized fitnessSorted" . sized' fitnessSorted
                 },
      experiment { title = "E",
                   searchFitness = minimize "sized fitnessSorted'" . sized' (fitnessStructural propSorted)
                 },
      experiment { title = "F",
                   searchFitness = minimize "fitnessSorted''" . fitnessSortedIncorrectPairs
                 },
      experiment { title = "F_max",
                   searchFitness = maximize "fitnessSorted''" . fitnessSortedAllPairs
                 },
      experiment { title = "G",
                   searchFitness = maximize "target n fitnessSorted''" . penaltyMin' (targetSize experiment) (fromIntegral . fitnessSorted)
                 },
      experiment { title = "G_max",
                   searchFitness = maximize "target n fitnessSorted''" . fitness' (targetSize experiment) (fromIntegral . fitnessSortedMax)
                 },
      experiment { title = "H",
                   searchFitness = minimize "target n fitnessSorted''" . penaltyMin' (targetSize experiment) (fromIntegral . fitnessStructural propSorted)
                 },
      experiment { title = "H_max",
                   searchFitness = maximize "target n fitnessSorted''" . fitness' (targetSize experiment) (fromIntegral . fitnessStructuralMax propSorted)
                 },
      experiment { title = "I",
                   searchFitness = minimize "target n fitnessSorted''" . penaltyMin' (targetSize experiment) fitnessSortedAllPairs
                 },
      experiment { title = "I_max",
                   searchFitness = maximize "target n fitnessSorted''" . fitness' (targetSize experiment) fitnessSortedAllPairs
                 },
      -- Paper MOO experiment
      experiment { title = "MOO_B",
                   searchFitness = minimize "fitnessSorted" . fromIntegral . fitnessSorted <>
                                   maximize "length" . fromIntegral . length
                 },
      experiment { title = "MOO_C",
                   searchFitness = minimize "fitnessSorted'" . fromIntegral . fitnessStructural propSorted <>
                                   maximize "length" . fromIntegral . length
                 },
      experiment { title = "MOO_D",
                   searchFitness = minimize "fitnessSorted" . sized' fitnessSorted <>
                                   maximize "length" . fromIntegral . length
                 },
      experiment { title = "MOO_E",
                   searchFitness = minimize "sized fitnessSorted'" . sized' (fitnessStructural propSorted) <>
                                   maximize "length" . fromIntegral . length
                 },
      experiment { title = "MOO_F",
                   searchFitness = minimize "fitnessSorted''" . fitnessSortedIncorrectPairs <>
                                   maximize "length" . fromIntegral . length
                 },
      experiment { title = "MOO_G",
                   searchFitness = minimize "target n fitnessSorted''" . penaltyMin' (targetSize experiment) (fromIntegral . fitnessSortedMax) <>
                                   maximize "length" . fromIntegral . length
                 },
      experiment { title = "MOO_H",
                   searchFitness = minimize "target n fitnessSorted''" . penaltyMin' (targetSize experiment) (fromIntegral . fitnessStructuralMax propSorted) <>
                                   maximize "length" . fromIntegral . length
                 },
      experiment { title = "MOO_I",
                   searchFitness = minimize "target n fitnessSorted''" . penaltyMin' (targetSize experiment) fitnessSortedAllPairs <>
                                   maximize "length" . fromIntegral . length
                 },
      experiment { title = "MOO_F_max",
                   searchFitness = maximize "fitnessSorted''" . fitnessSortedAllPairs <>
                                   maximize "length" . fromIntegral . length
                 },
      experiment { title = "MOO_G_max",
                   searchFitness = maximize "target n fitnessSorted''" . fitness' (targetSize experiment) (fromIntegral . fitnessSortedMax) <>
                                   maximize "length" . fromIntegral . length
                 },
      experiment { title = "MOO_H_max",
                   searchFitness = maximize "target n fitnessSorted''" . fitness' (targetSize experiment) (fromIntegral . fitnessStructuralMax propSorted) <>
                                   maximize "length" . fromIntegral . length
                 },
      experiment { title = "MOO_I_max",
                   searchFitness = maximize "target n fitnessSorted''" . fitness' (targetSize experiment) fitnessSortedAllPairs <>
                                   maximize "length" . fromIntegral . length
                 },
      -- Single Objective - Maximization approaches
      -- 1. FitnesSortedMax
      experiment { title = "list_sorted_max",
                   searchFitness = maximize "fitnessSorted" . fromIntegral . fitnessSortedMax
                 },
      experiment { title = "list_sorted_sized_max",
                   searchFitness = maximize "fitnessSorted" . sized' fitnessSortedMax
                 },
      experiment { title = "list_sorted_size_penalty_max",
                   searchFitness = maximize "fitnessSorted" . fitness' (targetSize experiment) (fromIntegral . fitnessSortedMax)
                 },
      -- 2. Structural
      experiment { title = "list_sorted_structural",
                   searchFitness = maximize "fitnessSorted" . fromIntegral . fitnessStructuralMax propSorted
                 },
      experiment { title = "list_sorted_max_structural",
                   searchFitness = maximize "fitnessSorted" . fromIntegral . fitnessStructuralMax propSorted
                 },
      experiment { title = "list_sorted_sized_structural",
                   searchFitness = maximize "fitnessSorted" . sized' (fitnessStructuralMax propSorted)
                 },
      experiment { title = "list_sorted_penalty_max_structural",
                   searchFitness = maximize "fitnessSorted" . fitness' (targetSize experiment) (fromIntegral . fitnessStructuralMax propSorted)
                 },
      -- 3. All pairs
      experiment { title = "list_sorted_allpairs",
                   searchFitness = maximize "sorted" . fitnessSortedAllPairs
                 },
      experiment { title = "list_sorted_allpairs_size_penalty",
                   searchFitness = maximize "sorted" . fitness' (targetSize experiment) fitnessSortedAllPairs
                 },
      -- Multiple Objectives maximization
      experiment { title = "list_sorted_moo_simple",
                   searchFitness = minimize "fitnessSorted" . fromIntegral . fitnessSorted <>
                                   maximize "length" . (\n -> min 1 (fromIntegral n / fromIntegral (targetSize experiment))) . length
                 },
      experiment { title = "list_sorted_moo_sized_simple",
                   searchFitness = minimize "fitnessSorted" . sized' fitnessSorted <>
                                   maximize "length" . (\n -> min 1 (fromIntegral n / fromIntegral (targetSize experiment))) . length
                 },
      experiment { title = "list_sorted_moo_max_simple",
                   searchFitness = maximize "fitnessSorted" . fromIntegral . fitnessSortedMax <>
                                   maximize "length" . fitness' (targetSize experiment) (const 1)
                 },
      experiment { title = "list_sorted_moo_max_sized_simple",
                   searchFitness = maximize "fitnessSorted" . sized' fitnessSortedMax <>
                                   maximize "length" . fitness' (targetSize experiment) (const 1)
                 },
      experiment { title = "list_sorted_moo_structural",
                   searchFitness = minimize "fitnessSorted" . fromIntegral . fitnessStructural propSorted <>
                                   maximize "length" . (\n -> min 1 (fromIntegral n / fromIntegral (targetSize experiment))) . length
                 },
      experiment { title = "list_sorted_moo_max_structural",
                   searchFitness = maximize "fitnessSorted" . fromIntegral . fitnessStructuralMax propSorted <>
                                   maximize "length" . (\n -> min 1 (fromIntegral n / fromIntegral (targetSize experiment))) . length
                 },
      experiment { title = "list_sorted_moo_allpairs",
                   searchFitness = maximize "sorted" . fitnessSortedAllPairs <>
                                   maximize "length" . (\n -> min 1 (fromIntegral n / fromIntegral (targetSize experiment))) . length
                  }
      -- Other
      -- experiment { title = "list_sorted_simple_new",
      --              searchFitness = maximize "fitnessSorted" . fitness' (targetSize experiment) fitnessSortedLength
      --            },
      -- experiment { title = "list_sorted_sized",
      --              searchFitness = minimize "fitnessSorted" . sized' fitnessSorted
      --            },
      -- experiment { title = "list_sorted_structural_sized",
      --              searchFitness = minimize "fitnessStructural" . sized' (fitnessStructural propSorted)
      --            },
      -- experiment { title = "list_sorted_structural_max",
      --              searchFitness = maximize "fitnessStructural" . fromIntegral . fitnessStructuralMax propSorted
      --            },
      -- experiment { title = "list_sorted_and_size",
      --              searchFitness = minimize "propSorted" . fromIntegral . fitnessSorted <>
      --                              minimize "sizeFitness" . sizeFitness
      --            },
      -- experiment { title = "list_sorted_structural_and_size",
      --              searchFitness = minimize "fitnessStructural" . fromIntegral . fitnessStructural propSorted <>
      --                              minimize "length" . towards 100 fromIntegral . length
      --            }
    ]

-- Contains default values for options not configurable by CLI options
baseExperiment :: Experiment'
baseExperiment = E
  { title = ""
  -- Evolutionary
  , searchOperators = [
      CrossoverOp listCrossover,
      -- CrossoverOp listCrossoverUniform,
        MutationOp listMutValue,
        MutationOp listMutInsert,
        MutationOp listMutInsertAt,
        MutationOp listMutRemove,
        MutationOp listMutTake,
        MutationOp listMutDrop,
        MutationOp listMutDropTake,
        MutationOp listMutSwap
      ]
  -- , searchSelection = selectionByParetoElite 10
  , searchSelection = return . selectionByEliteOn fitnessMean 20
  , searchGenerator = arbitrary
  , searchStep = error "undefined runtime step"
  -- Fitness
  , searchFitness = error "undefined experiment fitness"
  -- Option params
  , searchGenerations = error "undefined experiment generators"
  , searchPopulation = error "undefined experiment population"
  , searchMutation = error "undefined experiment mutation ratio"
  , searchCrossover = error "undefined experiment crossover ratio"
  , targetSize = error "undefined target size"
  -- Reports
  , checkDiversity = False
  , showStepReport = False
  , stepReportExtra = \step ->
      let
        stepUnique = Set.fromList (pop step)

        stepUValid = Set.filter (propSorted . snd) stepUnique

        countBySize :: Foldable f => f (Individual [a]) -> Map Int Int
        countBySize = Utils.countBy (\(_,x) -> length x)

        -- TODO: Make it dependent of the optimization type of problem
        bestI = head $ List.sortOn (Down . sum . fitnessValues . fst) $ pop step
        best' = (fitnessValues (fst bestI), getInt' <$> snd bestI)
        meanFitness' = Statistics.mean $ V.fromList $ (sum . fitnessValues . fst) <$> pop step
        meanSize' = Statistics.mean $ V.fromList $ (fromIntegral . length . snd) <$> pop step
        stepDiversity' = Utils.calcDiversity (snd <$> (pop step))
       in ExtraStepReport
          { popSummary = (\(fi, x) -> (fitnessValues fi, length x)) <$> pop step -- [([Double], Int)] -- Represents the fitness values and the size, ordered by fitness
          , stepSizes = countBySize (pop step)
          , uniqueStepSizes = countBySize stepUnique
          , validStepSizes = countBySize stepUValid
          , best = best'
          , meanFitness = meanFitness'
          , meanSize = meanSize'
          , stepDiversity = stepDiversity'
          }
  , showFinalReport = True
  , validReport = propSorted . snd
  , selectionReport = \stepPopulation ->
      let valid' = filter (propSorted . snd) stepPopulation
      in if valid' == [] then [] else [Utils.maximumOn (length . snd) valid']
  , extraReportFinal = \_steps baseReport ->
    let
        unique = BaseReport._uniqueIndividuals baseReport
        uniqueValid' = BaseReport._validIndividuals baseReport
        uniqueValidLists' = Set.map snd (BaseReport._validIndividuals baseReport)
        uniqueSizes' = Utils.countBy (length . snd) unique
        uniqueValidSizes' = Utils.countBy (length . snd) uniqueValid'
        maxSize' = if null (Map.keys uniqueValidSizes') then -1 else maximum (Map.keys uniqueValidSizes')
        selected = Set.toList $ BaseReport._selectedIndividuals baseReport
        maxList' = if null selected then (SingleObjective (Maximize "not selected" 0), []) else Utils.maximumOn (length . snd) selected
        -- totalUnique' = Set.size unique
        totalUniqueValid' = Set.size uniqueValid'
        -- uValid' = (Set.map snd (uniqueValid'))
        diversity' = Utils.calcDiversity (Set.toList uniqueValidLists')
        -- diversity' = 0
        diversityBySize' = Utils.calcDiversityBySize uniqueValidLists'
        -- diversityBySize' = Map.empty
        uniqueValidFitnessSummary' = (\(fi, x) -> (fitnessValues fi, length x)) <$> Set.toList uniqueValid' -- [([Double], Int)] -- Represents the fitness values and the size, ordered by fitness
        uniqueFitnessSummary' = fitnessValues . fst <$> Set.toList unique
     in ExtraFinalReport
        { maxSize = maxSize'
        , uniqueSizes = uniqueSizes'
        , uniqueValidSizes = uniqueValidSizes'
        -- , totalUnique = totalUnique'
        , totalUniqueValid = totalUniqueValid'
        , maxList = maxList'
        , diversity = diversity'
        , diversityBySize = diversityBySize'
        -- , uValid = uValid'
        , uniqueValidFitnessSummary = uniqueValidFitnessSummary'
        , uniqueFitnessSummary = uniqueFitnessSummary'
        }
  }
