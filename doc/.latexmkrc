$pdf_mode = 1; # Generate PDF output

# Command to build PDF output
$pdflatex = 'pdflatex -synctex=1 -interaction=nonstopmode %O %S';

# Add dependencies for standalone TikZ files
# add_cus_dep('tikz', 'pdf', 0, 'pdflatex %O --shell-escape %B');
# add_input_dest('tikz', 'pdf', 0, 1);
# push @generated_exts, '.pdf';

# Customize error/warning messages
$warning_msg .= "\n\n##################################################\n\n";
$error_msg .= "\n\n##################################################\n\n";

# End of .latexmkrc
