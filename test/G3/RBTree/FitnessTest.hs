module G3.RBTree.FitnessTest (spec) where


import Test.Hspec
-- import Test.QuickCheck
-- import System.Random

-- import Control.Monad (liftM2)

-- import RBTree
-- import qualified Utils.RBTree as Utils

-- import G3.RBTree.Fitness
-- import G3.RBTree.Fitness.RecursiveProps


-- prop_fitness_NonEmptyTree :: (Show a, Eq a) => RBTreeFitness a -> Gen (RBTree a) -> Property
-- prop_fitness_NonEmptyTree f genTree = forAll genTree $ \t ->
--   let fE = fitness f E
--       fT = fitness f t
--       msg = "(" ++ show fE ++ ", E) < " ++ show (fT, t)
--    in counterexample msg (t /= E ==> fE < fT)

-- prop_fitness_RedRoot :: (Arbitrary a, Show a, Eq a) => RBTreeFitness a -> Gen (RBTree a) -> Property
-- prop_fitness_RedRoot  f genTree =
--   let arbitrary' = liftM2 (,) (suchThat genTree (/= E)) arbitrary
--    in forAll arbitrary' $ \(t, val) ->
--   let rt = Utils.insertNpi [] R val t
--       bt = Utils.insertNpi [] B val t
--       frt = fitness f rt
--       fbt = fitness f bt
--       msg = show (frt, rt) ++ " < " ++ show (fbt, bt)
--    in counterexample msg (frt < fbt)


-- prop_fitness_RBValid :: (Show a, Eq a) => RBTreeFitness a -> Gen (RBTree a) -> Gen (RBTree a) -> Property
-- prop_fitness_RBValid f valid invalid = forAll (sized arbitrary') $ \(v, i) -> v == i || fitness f v < fitness f i
--   where -- arbitrary' :: Int -> Gen (RBTree a, RBTree a)
--         arbitrary' n = do
--           v <- resize (n+1) valid
--           i <- resize (n+1) invalid
--           return (v, i)

-- prop_fitness_Contained :: (Show a, Arbitrary a) => RBTreeFitness a -> Gen (RBTree a) -> Property
-- prop_fitness_Contained f valid = forAll (liftM2 (,) valid arbitrary) $ \(t, val) -> fitness f t <= fitness f (T B E val t)

-- commonFitnessTest :: (Arbitrary a, Show a, Random a, Ord a, Num a) => RBTreeFitness a -> SpecWith ()
-- commonFitnessTest f = do
--   it "f E < f !E" $ prop_fitness_NonEmptyTree f boundedTree

--   it "f (T R E a E) < f (T B E a E)" $ prop_fitness_RedRoot f (arbitrary >>= \val -> return (T R E val E))

--   it "f (T R _ a _) < f (T B _ a _)" $ prop_fitness_RedRoot f boundedTree

--   it "t valid ==> f t < T B _ _ t || T B t _ _" $ prop_fitness_Contained f rbTree

--   it "depthB valid = depthB invalid AND valid /= invalid => f rbValid > f rbInvalid" pending -- $ prop_fitness_RBValid f rbTree boundedTree


spec :: SpecWith ()
spec = return ()
--   context "Fitness properties" $ do
--     context "recursive properties" $ do
--       commonFitnessTest (recursivePropsFitness :: RBTreeFitness Int)
