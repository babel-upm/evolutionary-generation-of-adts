{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}
module G3.EA.QualityIndicators.HypervolumeTest (test) where

import Test.QuickCheck

import G3.EA.QualityIndicators.HyperVolume

hvGen :: Gen (RefPoint, [Point])
hvGen =
  let
    dimension n = choose (1, max 1 n)
    point = choose(0.0, 1.0)
  in sized $ \n -> do
    d <- dimension n
    points <- listOf1 (vectorOf d point)
    return (RefPoint (replicate d 1.1), points)

hvShrink :: (RefPoint, [Point]) -> [(RefPoint, [Point])]
hvShrink (ref, points) = (ref,) <$> filter (/= []) (shrinkRemoveOne points)

-- shrinkDouble :: Double -> [Double]
-- shrinkDouble d = reverse [d + ((1 - d) / 2), d / 2]

-- shrinkPoint :: Point -> [Point]
-- shrinkPoint [] = [[]]
-- shrinkPoint (x:xs) = do
--   x' <- shrinkDouble x
--   xs' <- shrinkPoint xs
--   return (x':xs')

shrinkRemoveOne :: [Point] -> [[Point]]
shrinkRemoveOne [] = []
shrinkRemoveOne xs =
  let xs1 = [ removeAt n xs | n <- [0 .. (length xs - 1)]]
      xs2 = concatMap shrinkRemoveOne xs1
  in reverse $ xs1 ++ xs2

removeAt :: Int -> [a] -> [a]
removeAt _ [] = []
removeAt n (y:ys)
  | n <= 0 = ys
  | otherwise = y : removeAt (n-1) ys

test :: IO ()
test = do
  putStrLn "Running HV test..."
  verboseCheckWith (stdArgs {maxShrinks = 100}) $
    forAllShrink hvGen hvShrink $ \(ref, points) -> idempotentIOProperty $
    do
      py <- pyHV ref points
      let hs = wfg ref points
      let precision = 0.1 :: Double
      let diff = abs (py - hs)
      return $ counterexample (
        show py <> " =/= " <> show hs <> "\t" <>
          "(max diff " <> show precision <> ", got: " <> show diff <> ")"
        )
        (diff <= precision)
