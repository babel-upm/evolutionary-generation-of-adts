{-# LANGUAGE NumericUnderscores #-}
module Gen.RBTreeTest (spec) where

-- import RBTree
-- import qualified Utils.RBTree as Utils

import Test.QuickCheck
import Test.Hspec

-- prop_Size :: Show a => (RBTree a -> Int) -> Gen (RBTree a) -> Property
-- prop_Size size genTree = forAll (sized gen) $ \(s, t) -> counterexample (msg s t) $ s > 1 ==> size t === s
--   where
--     -- gen :: Int -> Gen (Int, RBTree a)
--     msg s t = "expected size " ++ show s ++ " for " ++ show t ++ " but (size t) == " ++ show (size t)
--     gen n' = do
--       let n = min n 10
--       t <- resize n genTree
--       return (n, t)

spec :: SpecWith ()
spec = return ()
  -- context "balTree" $ do
    -- it "size determines max depth of Black Nodes" $ property $ prop_Size (Utils.lg . Utils.maxDepthRB) (balTree :: Gen (RBTree ()))
