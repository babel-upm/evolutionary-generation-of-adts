{-
 Adapted from Matt Might: https://matt.might.net/articles/quick-quickcheck/

 He also sais that he learnt how to generate BST from Pierce:
 - http://www.seas.upenn.edu/~cis552/12fa/lectures/stub/BST.html
 Available at:fs
 - https://web.archive.org/web/20210728000614/http://www.seas.upenn.edu/~cis552/12fa/lectures/stub/BST.html
-}
{-# OPTIONS_GHC -Wno-missing-export-lists #-}
{-# OPTIONS_GHC -Wno-orphans #-}
module RBTreeTest (spec) where

import Test.Hspec
-- import Test.QuickCheck
-- import System.Random

-- import RBTree
-- import qualified Utils.RBTree as Utils

-- instance
--   ( Arbitrary a,
--     Random a,
--     Bounded a,
--     Ord a,
--     Num a
--   ) =>
--   Arbitrary (RBTree a)
--   where
--   arbitrary = oneof [ordbalnrrTree, insertedTree]

-- -- Insertion properties:
-- prop_Create5 :: Int -> Int -> Int -> Int -> Int -> Bool
-- prop_Create5 a b c d e =
--   foldr insert empty [a, b, c, d, e]
--     == foldr insert empty [b, c, d, e, a]

-- prop_InsertValid :: RBTree Int -> Int -> Bool
-- prop_InsertValid t x = prop_RBValid (insert x t)

-- prop_InsertMember :: RBTree Int -> Int -> Bool
-- prop_InsertMember t x = member x (insert x t)

-- prop_InsertSafe :: RBTree Int -> Int -> Int -> Property
-- prop_InsertSafe t x y = member x t ==> member x (insert y t)

-- prop_InsertSafe' :: RBTree Int -> Int -> Property
-- prop_InsertSafe' t y = forAll (arbitrary' t) $ \x -> member x t ==> member x (insert y t)
--   where
--     arbitrary' t'
--       | t' == empty = arbitrary
--       | otherwise = oneof (pure <$> toAscList t)

-- prop_NoInsertPhantom :: RBTree Int -> Int -> Int -> Property
-- prop_NoInsertPhantom t x y =
--   not (member x t) && x /= y ==> not (member x (insert y t))

-- -- Deletion properties:
-- prop_InsertDeleteValid :: RBTree Int -> Int -> Bool
-- prop_InsertDeleteValid t x = prop_RBValid (delete x (insert x t))

-- prop_MemberDeleteValid :: RBTree Int -> Int -> Property
-- prop_MemberDeleteValid t x = member x t ==> prop_RBValid (delete x t)

-- prop_MemberDeleteValid' :: RBTree Int -> Property
-- prop_MemberDeleteValid' t = forAll (arbitrary' t) $ \x -> member x t ==> prop_RBValid (delete x t)
--   where
--     arbitrary' t'
--       | t' == empty = arbitrary
--       | otherwise = oneof (pure <$> toAscList t)

-- prop_DeleteValid :: RBTree Int -> Int -> Bool
-- prop_DeleteValid t x = prop_RBValid (delete x t)

-- prop_MemberDelete :: RBTree Int -> Int -> Property
-- prop_MemberDelete t x = member x t ==> not (member x (delete x t))

-- prop_MemberDelete' :: RBTree Int -> Property
-- prop_MemberDelete' t = forAll (arbitrary' t) $ \x -> member x t ==> not (member x (delete x t))
--   where
--     arbitrary' t'
--       | t' == empty = arbitrary
--       | otherwise = oneof (pure <$> toAscList t)

-- prop_DeletePreserve :: RBTree Int -> Int -> Int -> Property
-- prop_DeletePreserve t x y = x /= y ==> member y t == member y (delete x t)

-- {- Main -}

spec :: SpecWith ()
spec = return ()
--   describe "Red Black Set Test" $ do
--     it "generates valid RB Trees" $ property (prop_RBValid :: RBTree Int -> Bool)

--     context "Insertion tests" $ do
--       it "prop_Create5" $ property prop_Create5
--       it "prop_InsertValid" $ property prop_InsertValid
--       it "prop_InsertSafe" $ property prop_InsertSafe'
--       it "prop_NoInsertPhantom" $ property prop_NoInsertPhantom
--       it "prop_InsertMember" $ property prop_InsertMember

--     context "Deletion tests" $ do
--       it "prop_InsertDeleteValid" $ property prop_InsertDeleteValid
--       it "prop_MemberDeleteValid" $ property prop_MemberDeleteValid'
--       it "prop_MemberDelete" $ property prop_MemberDelete'
--       it "prop_DeletePreserve" $ property prop_DeletePreserve
