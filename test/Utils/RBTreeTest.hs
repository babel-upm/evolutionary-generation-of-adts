{-# OPTIONS_GHC -Wno-missing-export-lists #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Utils.RBTreeTest (spec) where

import Test.Hspec
import Test.QuickCheck

-- import RBTree
-- import Utils.RBTree

-- npiInsertRemove :: Eq a => [NPI] -> Color -> a -> RBTree a -> Bool
-- npiInsertRemove i c a t = t == (removeNpi i . insertNpi i c a) t

-- prop_npi_insert_remove :: Property
-- prop_npi_insert_remove = forAll input $ \(i, c, a, t) -> npiInsertRemove i c a t
--   where
--     -- input :: Arbitrary a => Gen ([NPI], Color, a, RBTree a)
--     input = do
--       t <- boundedTree :: Gen (RBTree Int)
--       i <- elements (npiLeafs t)
--       c <- elements [R, B]
--       a <- arbitrary
--       return (i, c, a, t)


spec :: SpecWith ()
spec = return ()
--   describe "Utils on RBTree Test" $ do
--     context "pending" $ do
--       it "size" $ do pending
--       it "left" $ do pending
--       it "right" $ do pending
--       it "unsafeJoin" $ do pending
--       it "balance" $ do pending
--       it "depth" $ do pending
--       it "minDepth" $ do pending
--       it "npi" $ do pending
--       it "npiLeafs" $ do pending
--       it "removeNpi" $ do pending
--       it "insertNpi" $ do pending
--       it "lg" $ do pending

--     context "Node Position Index (NPI) on RBTree" $ do
--       it "equivalent to insert then remove with NPI" $ property prop_npi_insert_remove
