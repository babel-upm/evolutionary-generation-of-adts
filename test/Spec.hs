import Test.Hspec

import qualified RBTreeTest
import qualified Utils.RBTreeTest
import qualified Gen.RBTreeTest
import qualified G3.RBTree.FitnessTest
import qualified G3.EA.QualityIndicators.HypervolumeTest

main :: IO ()
main = do
  hspec RBTreeTest.spec
  hspec G3.RBTree.FitnessTest.spec
  G3.EA.QualityIndicators.HypervolumeTest.test
  -- hspec Gen.RBTreeTest.spec
  -- hspec Utils.RBTreeTest.spec
