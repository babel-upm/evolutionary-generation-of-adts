.PHONY: build

CONFIG_FILE := BenchmarkConfig.hs
STACK := stack

build:
	$(STACK) build

run: build $(CONFIG_FILE)
	$(STACK) run

run-timed: build $(CONFIG_FILE)
	d=$$(date +%s)\
	; $(STACK) run \
	&& echo "Build took $$(($$(date +%s)-d)) seconds"

run-clean: restore-benchmarks run

plot: run run-plot

run-plot:
	gnuplot -e "filename='max_fitness.csv'" plots/plot_all.gp
	gnuplot -e "filename='all_fitness.csv'" plots/plot_all.gp

benchmarks: build $(CONFIG_FILE)
	$(STACK) run

restore-benchmarks:
	rm -rf bench
